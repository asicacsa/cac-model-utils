package utiles;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 * Clase utilizada para convertir Identificadores que representen valores Enteros en base decimal a valores en otra base de representacion cuyo alfabeto de simbolos se representa por la constante <b>simbolos</b><br>La base de destino de la representacion viene determinada por laongitud del alfabeto de simbolos.<br>Por defecto, se utiliza un alfabeto de 36 simbolos con los valores [0-9][A-Z].
 * @author Angel Perez Izquierdo
 * @since Junio 2007
 *
 */
public class ElementoBaseN implements Comparable<ElementoBaseN> {
	
	/**Logger asociado a la clase FicheroReplica*/
	private static final Log log = LogFactory.getLog(ElementoBaseN.class);
	
	/**Valor en base 10 [0-9] del elemento representado.*/
	private long valorDecimal;
	
	/**Cadena de texto que representa el valor en la base N utilizando los simbolos del alafabeto de representacion.*/
	private String valorBaseN;
		
	/**Base en la que se va a representar el valor.*/
	private int base;
	
	/*Los 36 simbolos <b>[0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ]</b> utilizados para la representacion del elemento en base 36.<br>Se va a utilizar la posicion charAt() para el acceso directo a las posiciones de cada caracter.
	public static final String simbolos = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";*/
	/**Los 10 simbolos <b>[0123456789]</b> utilizados para la representacion del elemento en base 36.<br>Se va a utilizar la posicion charAt() para el acceso directo a las posiciones de cada caracter.*/
	public static final String simbolos = "0123456789";
	
	/**Numero de simbolos maximos a contener por el valor, nos da el rango maximo de representacion.*/
	private int longitudMax;
	

	/**
	 * Constructor minimo
	 * @param valorDecimal El valor que se desea expresar en la base destino
	 * @param longitudMax Numero de simbolos maximos a contener por el valor, nos da el rango maximo de representacion.
	 */
	public ElementoBaseN(long valorDecimal, int longitudMax ) {
		this.valorDecimal = valorDecimal;
		this.base = simbolos.length();
		this.longitudMax = longitudMax;
		generarValorBaseN();
	}
	
	/**
	 * Constructor minimo
	 * @param valorDecimal El valor que se desea expresar en la base destino
	 * @param valorDecimalModulador identificador del objeto del que depende el elemento actual para poder ser identificado de forma unica.
	 * @param longitudMax Numero de simbolos maximos a contener por el valorDecimal, nos da el rango maximo de representacion.
	 */
	public ElementoBaseN(long valorDecimal, long valorDecimalModulador, int longitudMax ) {
		this.valorDecimal = valorDecimal;
		this.base = simbolos.length();
		this.longitudMax = longitudMax;
		generarValorBaseNModulado( valorDecimalModulador );
	}
	
	/**
	 * Constructor minimo
	 * @param codlkx El valor en Base N que representa al entero decimal.
	 * @param longitudMax Numero de simbolos maximos a contener por el valor, nos da el rango maximo de representacion.
	 */
	public ElementoBaseN( String codlkx, int longitudMax ) {
		this.valorBaseN = codlkx;
		this.base = simbolos.length();
		this.longitudMax = longitudMax;
		generarValorDecimal();
	}
	
	/**
	 * Constructor minimo especifico para los elementos que dependen de la {@link Temporada} (CODPAR).
	 * 
	 * @param codlkx El valor en Base N que representa al entero decimal asociado a la entidad de la Caixa que dependa del {@link AforoTemporada} para su identificacion ( el codpar de {@link Partido} y el codpar de {@link Partido} ).
	 * @param codtem codigo de la {@link Temporada} que modula al codlkx para poder obtener el valor unico que identifica al {@link Partido} o {@link Precio}, pues el codpar y el tippre se repiten en los distintos {@link AforoTemporada}.
	 * @param longitud 
	 */
	public ElementoBaseN( String codlkx, long codtem ) {
		this.valorBaseN = codlkx;
		this.base = simbolos.length();
		this.longitudMax = Constantes.LONGITUDMAXCODAFO;
		long valorModulador = ( codtem - 1 ) * Constantes.MAXCODAFO;
		this.generarValorDecimalModulado( valorModulador );
	}

	
	/**
	 * Constructor minimo especifico para los elementos que dependen del {@link AforoTemporada} (CODPAR).
	 * 
	 * @param codlkx El valor que nos envia la Caixa que representa al entero decimal asociado a la entidad de la Caixa que dependa de la entidad identificada con el codigo facilitado (aplica a {@link Partido}, {@link Zonas} y {@link PrecioAforoTemporada} ).
	 * @param codpadre Codigo del {@link AforoTemporada} o {@link Partido} que modula al codlkx para poder obtener el valor unico que identifica al {@link Partido}, {@link Zonas} o {@link PrecioAforoTemporada}, pues el codpar, codzon y codblo y el tippre se repiten en los distintos {@link AforoTemporada}.
	 * @param longitud 
	 */
	public ElementoBaseN( String codlkx, long codpadre, int longitud ) {
		this.valorBaseN = codlkx;
		this.base = simbolos.length();
		this.longitudMax = longitud;
		long valorModulador = ( codpadre - 1 ) * ElementoBaseN.obtenerMaximoValorDecimalRepresentable( this.longitudMax );
		this.generarValorDecimalModulado( valorModulador );
	}
	
	
	/**
	 * @return Obtiene la base sobre la que se va a representar el elemento
	 */
	public int getBase() {
		return base;
	}

	/**
	 * @param base Valor de la base que se va a utilizar
	 */
	public void setBase(int base) {
		this.base = base;
	}

	/**
	 * @return Obtiene la longitud maxima que se puede utilizar para representar un elemento en la base especificada
	 */
	public int getLongitudMax() {
		return longitudMax;
	}

	/**
	 * @param longitudMax La longitud maxima que se puede utilizar para representar un elemento en la base especificada
	 */
	public void setLongitudMax(int longitudMax) {
		this.longitudMax = longitudMax;
	}
	
	/**
	 * Comprueba si ya se ha calculado el valor en base n y si no lo obtiene. 
	 * @return La representacion en base n del elemento actual formateado como un String
	 */
	public String getValorBaseN() {
		if ( this.valorBaseN == null || this.valorBaseN == Constantes.CADENA_VACIA ) {
			this.generarValorBaseN();
		}
		return this.valorBaseN;
	}	

	/**
	 * 
	 * @param valor Representacion en un String del valor codificado en base n
	 */
	public void setValorBaseN(String valorbasen) {
		this.valorBaseN = valorbasen;
	}

	/**
	 * @return Obtiene el Valor decimal al que representa el elemento codificado en la Base especificada.
	 */
	public long getValorDecimal() {
		if ( this.valorDecimal == 0 ) generarValorDecimal();
		return valorDecimal;
	}
	
	/**
	 * @param valorDecimal el Valor decimal al que representa el elemento codificado en la Base especificada.
	 */
	public void setValorDecimal( long valorDecimal ) {
		this.valorDecimal = valorDecimal;
	}
 
	
	/**
	 * funcion auxiliar para calcular potencias.
	 * @param base base que queremos elevar.
	 * @param exp exponente al que elevamos la base.
	 * @return el resultado de la base multiplicado exp veces por sigo misma.
	 */
	private static long potencia( int base , int exp ) {
		long pot = 1;
		for ( int i=0; i<exp; i++ ) pot = pot * base;
		return pot;
	}

	
	/**
	 * Metodo estatico que permite obtener el maximo valor entero en base 10 represntable por el objeto {@link ElementoBaseN}<br>Por defecto se usa base 36 [0-9][A-Z].
	 * @param posiciones longitud del campo para el que se va a generar un valor en base N
	 * @return
	 */
	public static long obtenerMaximoValorDecimalRepresentable(int posiciones) {
		return ElementoBaseN.potencia( ElementoBaseN.simbolos.length(), posiciones );
	}

	
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(ElementoBaseN ebn) {
		return new Long( ebn.valorDecimal ).compareTo( this.valorDecimal );
	}
	
	/**
	 * Metodo sobrecargado para asegurarnos que la persistencia a traves Hibernate funciona correctamente..
	 */
	public boolean equals(Object obj) {
		if ( obj.getClass() != this.getClass() ) return false;
		else {
			ElementoBaseN ebn = (ElementoBaseN)obj;
			return new Long( this.valorDecimal).equals(  new Long( ebn.getValorDecimal() )  );
		}
	}
	
	/**
	 * Metodo que es necesario sobrecargar para que Hibernate funcione como se espera, es imprescindible si esta clase va a ser incluido en algun Set.
	 */
	public int hashCode() {
		//return new Long( this.valorDecimal ).intValue();
		return new Long( this.valorDecimal ).hashCode();
	}

	
	/**
	 *
	 * @return representacion descriptiva del objeto en una cadena de texto que enumera el contenido de sus atributos.
	 */
	public String serializa( String valorBaseN ) {
		StringBuffer sb = new StringBuffer("Objeto de tipo ElementoBaseN cuyo valor Decimal es "); sb.append( this.valorDecimal );
		sb.append( " su valor representado en la base N es " );
		if ( valorBaseN == null ) { 
			sb.append( this.valorBaseN );
			sb.append( " que respeta su longitud maxima representable en base N fijada en " );
		}
		else {
			sb.append( valorBaseN ); 
			sb.append( " que EXCEDE su longitud maxima representable en base N fijada en " );
		}
		sb.append( this.longitudMax ); sb.append( " posiciones." );
		return sb.toString();

	}
	
	/**
	 * Calcula la representacion del valor decimal en base n del Elemento actual<br>Esta funcion sera invocada exclusivamente por el metodo getValorBaseN() cuando no tenga valor asignado
	 */
	private void generarValorBaseN() {
		StringBuffer val = new StringBuffer();
		long dividendo = this.valorDecimal;
		long resto = 0;

		while( dividendo >= this.base ) {
			resto = dividendo % this.base;
			dividendo = dividendo / this.base;
			val.insert(  0, ElementoBaseN.simbolos.charAt(  new Long( resto ).intValue()  )   );
		}
		
		int iDividendo = new Long( dividendo ).intValue();
		/**Siempre falta incluir el ultimo dividendo que ya no cumple la condicion pero es necesario para terminar la conversion.*/
		val.insert(  0, ElementoBaseN.simbolos.charAt( iDividendo )  );
		
		if ( val.length() > this.longitudMax )
			log.error(  this.serializa( val.toString() )  );
		
		if ( val.toString().length() > 0) 
			this.valorBaseN = val.toString();
		else 
			this.valorBaseN = String.valueOf( ElementoBaseN.simbolos.charAt( iDividendo ) )  ;

		//Padeamos por la izquierda con "0" para evitar tener que eliminarlos luego al leerlos. Incluso nos podemos evitar tener que padear al generar la salida. 
		this.valorBaseN = StringUtils.leftPad(this.valorBaseN, this.longitudMax, Constantes.CADENA_0 );
	}
	
	
	
	/**
	 * Calcula la representacion del valor decimal en base n del Elemento actual<br>Esta funcion sera invocada exclusivamente por el metodo getValorBaseN() cuando no tenga valor asignado   
	 * @param valorModulador
	 */
	private void generarValorBaseNModulado( long valorModulador ) {
		
		//long valorModulador = ( valorDecimalIdPadre -1 ) * valorMaxRepresentableHijo;
		long dividendo = this.valorDecimal;
		if ( valorModulador > 0 )
			dividendo = this.valorDecimal % valorModulador;
		
		StringBuffer val = new StringBuffer();
		long resto = 0;

		while( dividendo >= this.base ) {
			resto = dividendo % this.base;
			dividendo = dividendo / this.base;
			val.insert(  0, ElementoBaseN.simbolos.charAt(  new Long( resto ).intValue()  )   );
		}
		
		int iDividendo = new Long( dividendo ).intValue();
		/**Siempre falta incluir el ultimo dividendo que ya no cumple la condicion pero es necesario para terminar la conversion.*/
		val.insert(  0, ElementoBaseN.simbolos.charAt( iDividendo )  );
		
		if ( val.length() > this.longitudMax ) 
			log.error(  this.serializa( val.toString() )  );
		
		if ( val.toString().length() > 0) 
			this.valorBaseN = val.toString() ;
		else 
			this.valorBaseN = String.valueOf( ElementoBaseN.simbolos.charAt( iDividendo ) )  ;

		//Padeamos por la izquierda con "0" para evitar tener que eliminarlos luego al leerlos. Incluso nos podemos evitar tener que padear al generar la salida. 
		this.valorBaseN = StringUtils.leftPad(this.valorBaseN, this.longitudMax, Constantes.CADENA_0 );
	}
	
	
	/**
	 * Calcula la representacion del valor de la cadena alfanumerica en base decimal del Elemento actual<br>Esta funcion sera invocada exclusivamente por el metodo getValorDecimal() cuando no tenga valor asignado.
	 */	
	private void generarValorDecimal() {
		long decimal = 0;
		//Me aseguro que la cadena no excede la longitud maxima cogiendo los ultimos longitudMax-caracteres de la cadena.
		this.valorBaseN = StringUtils.upperCase(  StringUtils.substring( getValorBaseN(), -this.longitudMax )  );
		
		int longitud = this.valorBaseN.length();
		for( int i=0; i<longitud; i++ ) {
			//Con esto obtenenmos un numero [1-36]
			int coeficiente = ElementoBaseN.simbolos.indexOf(   String.valueOf(  this.valorBaseN.charAt( i )  )   );
			decimal = decimal + coeficiente * ElementoBaseN.potencia( this.base, longitud - i - 1 );
		}
		
		long max = ElementoBaseN.obtenerMaximoValorDecimalRepresentable( this.longitudMax );
		if(  decimal > max  ) {
			StringBuffer sb = new StringBuffer("Se ha generado un valor decimal "); sb.append(decimal); 
			sb.append(" que excedia el valor maximo representable por el elemento actual "); sb.append(max);
			log.warn( sb.toString() );
		}
		
		this.valorDecimal = decimal;
	}
	

	/**
	 * Calcula la representacion del valor de la cadena alfanumerica en base decimal del Elemento actual<br>Esta funcion sera invocada exclusivamente en el constructor de este objeto.
	 * @param valorModulador parte del codigo de la entidad de la que depende la actual necesaria para calcular un valor decimal unico, con el que identificar la entidad actual.
	 */	
	private void generarValorDecimalModulado( long valorModulador ) {
		
		long decimal = 0;
		//NO TIENE NINGUN SENTIDO, SI LLEGAMOS AQUI ES PORQUE HEMOS USADO EL CONSTRUCTOR CON EL VALOR EN BASEN YA MODULADO.
		//this.generarValorBaseNModulado( valorModulador );
		//Me aseguro que la cadena no excede la longitud maxima cogiendo los ultimos longitudMax-caracteres de la cadena.
		this.valorBaseN = StringUtils.upperCase(  
			StringUtils.substring( 
				this.getValorBaseN(),
				-this.longitudMax 
			)
		);
		int longitud = this.valorBaseN.length();
		for( int i=0; i<longitud; i++ ) {
			//Con esto obtenenmos un numero [1-36]
			int coeficiente = ElementoBaseN.simbolos.indexOf(  String.valueOf(  this.valorBaseN.charAt( i )  )   );
			decimal = decimal + coeficiente * ElementoBaseN.potencia( this.base, longitud - i - 1 );
		}
		long max = ElementoBaseN.obtenerMaximoValorDecimalRepresentable( this.longitudMax );
		if(  decimal > max  ) {
			StringBuffer sb = new StringBuffer("Se ha generado un valor decimal "); sb.append(decimal); 
			sb.append(" que excedia el valor maximo representable por el elemento actual "); sb.append(max);
			log.warn( sb.toString() );
		}
		this.valorDecimal = valorModulador + decimal;
	}

}
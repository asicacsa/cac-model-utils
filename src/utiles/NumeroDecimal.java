package utiles;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @author Angel Perez Izquierdo
 * @since Febrero 2008
 * 
 */
public class NumeroDecimal implements Comparable<NumeroDecimal> {

	private static final Log logger = LogFactory.getLog(NumeroDecimal.class);
	private BigDecimal valor;
	/**Objeto {@link PartesNumeroDecimal} que representa cada una de las magnitudes en valor absoluto que representan la parte que va antes del separador decimal (Parte Entera) y la que viene a continuación (Parte Decimal)*/
	private PartesNumeroDecimal partes;
	private boolean negativo;
	private boolean consigno;
	/**Longitud maxima para la parte entera.*/
	private int longParteEntera;
	/**Longitud maxima para la parte decimal.*/
	private int longParteDecimal;
	/**Separador decimal que utiliza el valor de entrada a pasar a los constructores.<br>Este valor puede ser vacio.*/
	private String marcaDecimalEntrada;
	/**Separador decimal para la representacion textual.<br>Este valor puede ser vacio.*/
	private String marcaDecimalSalida;

	/**
	 * Constructor por defecto.<br>
	 * Utiliza los siguientes valores por defecto:<br>
	 * valor = BigDecimal.ZERO<br>
	 * parteEntera = "0"<br>
	 * parteDecimal = "00"<br>
	 * longMaxParteEntera = 13<br>
	 * longMaxParteDecimal = 2<br>
	 * marcaDecimalEntrada = <b>.</b><br>
	 * marcaDecimalSalida = <b>,</b>
	 */
	public NumeroDecimal() {
		this.partes = new PartesNumeroDecimal( Constantes.CADENA_0, Constantes.CADENA_00 );
		this.valor = BigDecimal.ZERO;
		this.negativo = false;
		this.consigno = true;
		this.longParteDecimal = Constantes.POSICIONES_DECIMALES_POR_DEFECTO;
		this.longParteEntera = Constantes.POSICIONES_ENTERAS_POR_DEFECTO;
		this.marcaDecimalEntrada = Constantes.MARCA_DECIMAL_PUNTO;
		this.marcaDecimalSalida = Constantes.MARCA_DECIMAL_COMA;
	}

	/**
	 * Constructor minimo a partir de un objeto {@link BigDecimal}.<br>
	 * Permite instanciar a partir de un {@link BigDecimal} general un {@link NumeroDecimal}<br>
	 * Utiliza los siguientes valores por defecto:<br>
	 * longMaxParteEntera = 13<br>
	 * longMaxParteDecimal = 2<br>
	 * marcaDecimalEntrada = <b>.</b><br>
	 * marcaDecimalSalida = <b>,</b><br>
	 * magnitud con signo<br>
	 * @param importe {@link BigDecimal} con el valor decimal que deseamos asignar al objeto {@link NumeroDecimal} el resto de parametros se calculan en funcion de dicho valor.
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal( BigDecimal importe ) throws Exception {
		this(
			importe,
			Constantes.POSICIONES_ENTERAS_POR_DEFECTO,
			Constantes.POSICIONES_DECIMALES_POR_DEFECTO,
			Constantes.MARCA_DECIMAL_PUNTO,
			Constantes.MARCA_DECIMAL_COMA,
			true
		);
	}

	/**
	 * Constructor minimo a partir de un objeto {@link Integer}.<br>
	 * Permite instanciar a partir de un {@link Integer} general un {@link NumeroDecimal}<br>
	 * Utiliza los siguientes valores por defecto:<br>
	 * longMaxParteEntera = 13<br>
	 * longMaxParteDecimal = 2<br>
	 * marcaDecimalEntrada = <b>.</b><br>
	 * marcaDecimalSalida = <b>,</b>
	 * @param importe {@link BigDecimal} con el valor decimal que deseamos asignar al objeto {@link NumeroDecimal} el resto de parametros se calculan en funcion de dicho valor.
	 * Este constructor no puede llegar a lanzar la Exception "El importe a representar excede el espacio de representacion" pues con un entero no llegamos a más de 13 posiciones enteras.
	 */
	public NumeroDecimal( Integer importe ) throws Exception {
		this( new BigDecimal(importe) );
	}

	/**
	 * Constructor minimo a partir de un objeto {@link Long}.<br>
	 * Permite instanciar a partir de un {@link Long} general un {@link NumeroDecimal}<br>
	 * Utiliza los siguientes valores por defecto:<br>
	 * longMaxParteEntera = 13<br>
	 * longMaxParteDecimal = 2<br>
	 * marcaDecimalEntrada = <b>.</b><br>
	 * marcaDecimalSalida = <b>,</b>
	 * @param importe {@link BigDecimal} con el valor decimal que deseamos asignar al objeto {@link NumeroDecimal} el resto de parametros se calculan en funcion de dicho valor.
	 * Este constructor no puede llegar a lanzar la Exception "El importe a representar excede el espacio de representacion" pues con un entero no llegamos a más de 13 posiciones enteras.
	 */
	public NumeroDecimal( Long importe ) throws Exception {
		this( new BigDecimal(importe) );
	}

	/**
	 * Constructor minimo a partir de un objeto {@link Float}.<br>
	 * Permite instanciar a partir de un {@link Float} general un {@link NumeroDecimal}<br>
	 * Utiliza los siguientes valores por defecto:<br>
	 * longMaxParteEntera = 13<br>
	 * longMaxParteDecimal = 2<br>
	 * marcaDecimalEntrada = <b>.</b><br>
	 * marcaDecimalSalida = <b>,</b>
	 * contexto de redondeo (Precision 2 decimales y Metodo de redondeo del banquero HALF_EVEN)
	 * @param importe {@link BigDecimal} con el valor decimal que deseamos asignar al objeto {@link NumeroDecimal} el resto de parametros se calculan en funcion de dicho valor.
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal( Float importe ) throws Exception {
		this( importe.toString() );
	}

	/**
	 * Constructor minimo a partir de un objeto {@link Double}.<br>
	 * Permite instanciar a partir de un {@link Double} general un {@link NumeroDecimal}<br>
	 * Utiliza los siguientes valores por defecto:<br>
	 * longMaxParteEntera = 13<br>
	 * longMaxParteDecimal = 2<br>
	 * marcaDecimalEntrada = <b>.</b><br>
	 * marcaDecimalSalida = <b>,</b>
	 * contexto de redondeo (Precision 2 decimales y Metodo de redondeo del banquero HALF_EVEN)
	 * @param importe {@link BigDecimal} con el valor decimal que deseamos asignar al objeto {@link NumeroDecimal} el resto de parametros se calculan en funcion de dicho valor.
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal( Double importe ) throws Exception {
		this( importe.toString() );
	}

	/**
	 * Constructor parcial a partir de un objeto {@link BigDecimal}.
	 * @param importe {@link BigDecimal}a partir del cual se generaran la parteEntera y la parteDecimal.
	 * @param longPEntera valor entero que que representa la longitud maxima de la parte entera a representar por este {@link NumeroDecimal}
	 * @param longPDecimal valor entero que que representa la longitud maxima de la parte decimal a representar por este {@link NumeroDecimal}
	 * @param marcaEntrada {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en el valor que se le pasa al constructor {@link NumeroDecimal}
	 * @param marcaSalida {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en la representacion de este {@link NumeroDecimal}
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal( BigDecimal importe, int longPEntera, int longPDecimal, String marcaEntrada, String marcaSalida ) throws Exception {
		this(
			importe,
			longPEntera,
			longPDecimal,
			marcaEntrada,
			marcaSalida,
			true
		);
	}

	/**
	 * Constructor completo a partir de un objeto {@link BigDecimal}.<br>
	 * Este es el que se deberia de utilizar cuando queramos personalizar por completo el {@link NumeroDecimal}.
	 * @param importe {@link BigDecimal}a partir del cual se generaran la parteEntera y la parteDecimal. 
	 * @param longPEntera valor entero que que representa la longitud maxima de la parte entera a representar por este {@link NumeroDecimal}
	 * @param longPDecimal valor entero que que representa la longitud maxima de la parte decimal a representar por este {@link NumeroDecimal}
	 * @param marcaEntrada {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en el valor que se le pasa a este constructor {@link NumeroDecimal}
	 * @param marcaSalida {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en la representacion de este {@link NumeroDecimal}
	 * @param signed Valor lógico que va a indicar si tratamos una magnitud en la que hay que tener en cuenta el signo o no.
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal( BigDecimal importe, int longPEntera, int longPDecimal, String marcaEntrada, String marcaSalida, boolean signed ) throws Exception {
		this.longParteDecimal = longPDecimal;
		this.longParteEntera = longPEntera;
		this.marcaDecimalEntrada = marcaEntrada;
		this.marcaDecimalSalida = marcaSalida;
		this.valor = importe;
		this.consigno = signed;
		this.generaPartesEnteraYDecimal();
	}

	/**
	 * Constructor minimo que a partir de un importe en formato String<br>
	 * Utiliza los siguientes valores por defecto:<br><br>
	 * longMaxParteEntera = <b>13</b><br>
	 * longMaxParteDecimal = <b>2</b><br>
	 * Se asume que son valores con signo, con lo cual se va a tener en cuenta el mismo a la hora de serializar y de comparar.<br>
	 * @param importe {@link String} que representa un numero decimal, con <b>","</b> como separador decimal.
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal( String importe ) throws Exception {
		this(
			importe,
			( StringUtils.contains( importe, Constantes.MARCA_DECIMAL_COMA) ? Constantes.MARCA_DECIMAL_COMA : Constantes.MARCA_DECIMAL_PUNTO ),
			Constantes.MARCA_DECIMAL_COMA
		);
	}

	/**
	 * Constructor parcial que a partir de un importe en formato String, donde el separador decimal puede ser tanto la COMA como el PUNTO y un valor lógico que indica si es una magnitud en la que hay que tener en cuenta el signo o no.<br><br>
	 * Utiliza los siguientes valores por defecto:<br><br>
	 * longMaxParteEntera = <b>13</b><br>
	 * longMaxParteDecimal = <b>2</b><br>
	 * marcaDecimal de entrada y de salida es la coma <b>","</b>
	 * @param importe {@link String} que representa un numero decimal, con <b>","</b> como separador decimal.
	 * @param signed Valor lógico que nos indicará si hay que tener en cuenta el signo o no para esta magnitud.
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal( String importe, boolean signed ) throws Exception {
		this(
			( StringUtils.contains( importe, Constantes.MARCA_DECIMAL_COMA) ? 
				StringUtils.split( importe, Constantes.MARCA_DECIMAL_COMA )[0] :
				StringUtils.split( importe, Constantes.MARCA_DECIMAL_PUNTO )[0]
			),
			( StringUtils.contains( importe, Constantes.MARCA_DECIMAL_COMA) ? 
					StringUtils.split( importe, Constantes.MARCA_DECIMAL_COMA )[1] :
					StringUtils.split( importe, Constantes.MARCA_DECIMAL_PUNTO )[1]
				),
			( StringUtils.contains( importe, Constantes.MARCA_DECIMAL_COMA) ? Constantes.MARCA_DECIMAL_COMA : Constantes.MARCA_DECIMAL_PUNTO ),
			Constantes.MARCA_DECIMAL_COMA,
			Constantes.POSICIONES_ENTERAS_POR_DEFECTO,
			Constantes.POSICIONES_DECIMALES_POR_DEFECTO,
			signed
		);
	}

	/**
	 * Constructor que a partir de un importe en formato String, donde el separador decimal, se especifica como segundo parametro.<br>
	 * Utiliza los siguientes valores por defecto:<br>
	 * longMaxParteEntera = 13<br>
	 * longMaxParteDecimal = 2<br>
	 * se asume que son valores con signo, con lo cual se va a tener en cuenta el mismo a la hora de serializar y de comparar.
	 * @param importe {@link String} que representa un numero decimal, donde la marca decimal viene especificada por el segundo parametro.
	 * @param marcaEntrada {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en la representacion del valor que se facilita a este constructor de {@link NumeroDecimal}
	 * @param marcaSalida {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en la representacion de este {@link NumeroDecimal}
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal( String importe, String marcaEntrada, String marcaSalida ) throws Exception {
		this(
			( StringUtils.containsAny( importe, Constantes.MARCAS_DECIMALES ) ?
				StringUtils.split( importe, marcaEntrada )[0] :
				importe
			), 
			( StringUtils.containsAny( importe, Constantes.MARCAS_DECIMALES ) ?
				StringUtils.split( importe, marcaEntrada )[1] :
				Constantes.CADENA_00
			), 
			marcaEntrada,
			marcaSalida
		);
	}

	/**
	 * Constructor parcial que utiliza los siguientes valores por defecto:<br>
	 * longMaxParteEntera = 13<br>
	 * longMaxParteDecimal = 2<br>
	 * se asume que son valores con signo, con lo cual se va a tener en cuenta el mismo a la hora de serializar y de comparar.
	 * @param parteEntera {@link String} que representa el valor de la parte entera del {@link NumeroDecimal}.
	 * @param parteDecimal {@link String} que representa el valor de la parte decimal del {@link NumeroDecimal}.
	 * @param marcaEntrada {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en la representacion del valor que se facilita a este constructor de {@link NumeroDecimal}
	 * @param marcaSalida {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en la representacion de este {@link NumeroDecimal}
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal( String parteEntera, String parteDecimal, String marcaEntrada, String marcaSalida ) throws Exception {
		this(
			parteEntera,
			parteDecimal,
			marcaEntrada,
			marcaSalida,
			Constantes.POSICIONES_ENTERAS_POR_DEFECTO,
			Constantes.POSICIONES_DECIMALES_POR_DEFECTO
		);
	}

	/**
	 * Constructor parcial en el que se puede personalizar todos los campos del {@link NumeroDecimal} tomando como origen una cadena de texto. Se asume que el numero puede ser con signo.<br>
	 * Se asume que son valores con signo, con lo cual se va a tener en cuenta el mismo a la hora de serializar y de comparar.
	 * @param parteEntera {@link String} que representa el valor de la parte entera del {@link NumeroDecimal}.
	 * @param parteDecimal {@link String} que representa el valor de la parte decimal del {@link NumeroDecimal}.
	 * @param marcaEntrada {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en la representacion del valor que se facilita a este constructor de {@link NumeroDecimal}
	 * @param marcaSalida {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en la representacion de este {@link NumeroDecimal}
	 * @param longMaxParteEntera valor entero que que representa la longitud maxima de la parte entera a representar por este {@link NumeroDecimal}
	 * @param longMaxParteDecimal valor entero que que representa la longitud maxima de la parte decimal a representar por este {@link NumeroDecimal}
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal( String parteEntera, String parteDecimal, String marcaEntrada, String marcaSalida, int longMaxParteEntera, int longMaxParteDecimal ) throws Exception {
		this(
			parteEntera,
			parteDecimal,
			marcaEntrada,
			marcaSalida,
			longMaxParteEntera,
			longMaxParteDecimal,
			true
		);
	}

	/**
	 * Constructor completo en el que se puede personalizar todos los campos del {@link NumeroDecimal} tomando como origen una cadena de texto. 
	 * @param parteEntera {@link String} que representa el valor de la parte entera del {@link NumeroDecimal}.
	 * @param parteDecimal {@link String} que representa el valor de la parte decimal del {@link NumeroDecimal}.
	 * @param marcaEntrada {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en la representacion del valor que se facilita a este constructor de {@link NumeroDecimal}
	 * @param marcaSalida {@link String} que representa el simbolo que va a separar la parte entera de la decimal, en la representacion de este {@link NumeroDecimal}
	 * @param longMaxParteEntera valor entero que que representa la longitud maxima de la parte entera a representar por este {@link NumeroDecimal}
	 * @param longMaxParteDecimal valor entero que que representa la longitud maxima de la parte decimal a representar por este {@link NumeroDecimal}
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal( String parteEntera, String parteDecimal, String marcaEntrada, String marcaSalida, int longMaxParteEntera, int longMaxParteDecimal, boolean signed ) throws Exception {
		this.negativo = StringUtils.contains( parteEntera, Constantes.SIGNOMENOS );
		this.longParteDecimal = longMaxParteDecimal;
		this.longParteEntera = longMaxParteEntera;
		this.marcaDecimalEntrada = marcaEntrada;
		this.marcaDecimalSalida = marcaSalida;
		this.consigno = signed;
		this.partes = new PartesNumeroDecimal(  StringUtils.remove( new Long( parteEntera ).toString(), Constantes.SIGNOMENOS ), parteDecimal  );
		this.calcularValor();
		if ( !validarImporte() )
			throw new Exception("El importe a representar excede el espacio de representacion.");
	}



	public void setParteDecimal(String parteDecimal) throws Exception {
		this.partes.setDecimal( parteDecimal );
		this.calcularValor();
	}

	public void setParteEntera(String parteEntera) throws Exception {
		/**API 25 Julio 2011 con la siguiente instruccion eliminamos los posibles ceros por la izaquierda del String que representa la parte entera.*/
		this.partes.setEntera(  new Long( parteEntera ).toString()  );
		this.negativo = StringUtils.contains( parteEntera, Constantes.SIGNOMENOS );
		this.calcularValor();
		if ( !validarImporte() ) 
			throw new Exception("El importe a representar excede el espacio de representacion.");
		
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	/**
	 * Asigna el valor {@link BigDecimal} al {@link NumeroDecimal} y calcula los atributos parteEntera y la parteDecimal correspondientes a dicho BigDecimal.
	 * @param valor the valor to set
	 */
	public void setValor(BigDecimal valor) throws Exception {
		this.valor = valor;
		this.generaPartesEnteraYDecimal();
	}

	/**
	 * Asigna el valor {@link BigDecimal} al {@link NumeroDecimal} a partir de un Float y calcula los atributos parteEntera y la parteDecimal correspondientes a dicho BigDecimal.
	 * @param valor the valor to set
	 */
	public void setValor(Float valor) throws Exception {
		this.setValor(new BigDecimal( valor.toString() ) );
		this.generaPartesEnteraYDecimal();
	}

	/**
	 * Asigna el valor {@link Integer} al {@link NumeroDecimal} a partir de un Integer y calcula los atributos parteEntera y la parteDecimal correspondientes a dicho BigDecimal.
	 * @param valor the valor to set
	 */
	public void setValor(Integer valor) throws Exception {
		this.setValor(new BigDecimal(valor) );
		this.generaPartesEnteraYDecimal();
	}

	/**
	 * Asigna el valor {@link Long} al {@link NumeroDecimal} a partir de un Integer y calcula los atributos parteEntera y la parteDecimal correspondientes a dicho BigDecimal.
	 * @param valor the valor to set
	 */
	public void setValor(Long valor) throws Exception {
		this.setValor(new BigDecimal(valor) );
		this.generaPartesEnteraYDecimal();
	}

	public String getParteDecimal() {
		return this.partes.getDecimal();
	}

	public String getParteEntera() {
		return this.partes.getEntera();
	}

	public int getLongParteDecimal() {
		return longParteDecimal;
	}

	public void setLongParteDecimal(int longParteDecimal) {
		this.longParteDecimal = longParteDecimal;
	}

	public int getLongParteEntera() {
		return longParteEntera;
	}

	public void setLongParteEntera(int longParteEntera) {
		this.longParteEntera = longParteEntera;
	}

	public String getMarcaDecimalEntrada() {
		return marcaDecimalEntrada;
	}

	public void setMarcaDecimalEntrada(String marcaDecimalEntrada) {
		this.marcaDecimalEntrada = marcaDecimalEntrada;
	}

	public String getMarcaDecimalSalida() {
		return marcaDecimalSalida;
	}

	public void setMarcaDecimalSalida(String marcaDecimalSalida) {
		this.marcaDecimalSalida = marcaDecimalSalida;
	}

	public boolean isNegativo() {
		return negativo;
	}

	/**
	 * Marca como negativo en {@link NumeroDecimal}.<br>Ojo, esto no es más que un setter, para actualizar la propiedad.<b>Para negar el objeto entero de forma consistente, utilizar el método {@link NumeroDecimal}.niega()</b> 
	 * @param negativo the negativo to set
	 */
	public void setNegativo(boolean negativo) {
		this.negativo = negativo;
	}

	public boolean isConsigno() {
		return consigno;
	}

	public void setConsigno(boolean consigno) {
		this.consigno = consigno;
	}


	/**
	 * Sobrecargamos el hashCode() para que se gestione correctamente los criterios de igualdad. Pasamos a utilizar el hashCode() de las {@link PartesNumeroDecimal} del {@link NumeroDecimal} como identidad.
	 */
	public int hashCode() {
		return (  this.isNegativo() ? -this.aplicaRedondeoYTroceaEnPartes().hashCode() : this.aplicaRedondeoYTroceaEnPartes().hashCode()  );
	}


	/**
	 * Sobrecargado el equals(Object obj) para que se gestione correctamente los criterios de igualdad.
	 * @return <b>true</b> si los valores de la partes entera y decimal coinciden una vez redondeados, al criterio de 2 Decimales por HALF_EVEN, no coincidelos 2 objetos que deben ser de la clase {@link NumeroDecimal}.
	 * <br><b>false</b> si el {@link Object} obj con el que se desea igualar no es instancia de la clase {@link NumeroDecimal} o bien si las partes tras redondear al criterio de 2 Decimales por HALF_EVEN no coincide.
	 */
	public boolean equals( Object obj ){
		if ( obj.getClass() != this.getClass() )
			return false;
		NumeroDecimal nd = (NumeroDecimal)obj;
		if ( this.consigno != nd.isConsigno() )
			throw new ArithmeticException("No se pueden comparar NumerosDecimales sin signo con NumerosDecimales con signo.");
		return this.aplicaRedondeoYTroceaEnPartes().equals( nd.aplicaRedondeoYTroceaEnPartes()  ) && this.isNegativo() == nd.isNegativo();
	}


	/**
	 * En este caso la comparación entre 2 {@link NumeroDecimal} va en función de la comparación de cada una de las partes de los 2 {@link NumeroDecimal} implicados.
	 * @return Se evalua el signo de los 2 {@link NumeroDecimal} implicados y se devuelve:<br><b>-1</b> si el primero es negativo y el segundo no.<br><b>1</b> si el primero es positivo y el segundo negativo.<br>Si los signos coinciden se devuelve el resultado de la comparación de las partes. El resultado se niega si los 2 son negativos.
	 */
	public int compareTo(NumeroDecimal nd) {
		if ( this.consigno != nd.isConsigno() )
			throw new ArithmeticException("No se pueden comparar NumerosDecimales sin signo con NumerosDecimales con signo.");
		if ( !this.negativo && nd.isNegativo()  )
			return 1;
		else if (  this.negativo && !nd.isNegativo()  )
			return -1;
		int comparacion = this.aplicaRedondeoYTroceaEnPartes().compareTo( nd.aplicaRedondeoYTroceaEnPartes() );
		return (  this.negativo && nd.isNegativo() ? -comparacion : comparacion  );
	}



	/**
	 * Multiplicacion entre el {@link NumeroDecimal} actual y el {@link NumeroDecimal} facilitado como argumento.<br>
	 * @param nd El {@link NumeroDecimal} por el que se quiere multiplicar el actual
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal multiplica( NumeroDecimal nd ) throws Exception {
		return new NumeroDecimal(
			this.getValor().multiply( nd.getValor() ),
			this.longParteEntera,
			this.longParteDecimal,
			this.marcaDecimalEntrada,
			this.marcaDecimalSalida
		);
	}

	/**
	 * Multiplicacion entre el {@link NumeroDecimal} actual y el {@link Integer} facilitado como argumento.<br>
	 * @param i El {@link Integer} por el que se quiere multiplicar el actual
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal multiplica( Integer i ) throws Exception {
		return multiplica( new NumeroDecimal( i ) );
	}

	/**
	 * Multiplicacion entre el {@link NumeroDecimal} actual y el {@link Float} facilitado como argumento.<br>
	 * @param f El {@link Float} por el que se quiere multiplicar el actual
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal multiplica( Float f ) throws Exception {
		return multiplica( new NumeroDecimal( f ) );
	}

	/**
	 * Multiplicacion entre el {@link NumeroDecimal} actual y el {@link BigDecimal} facilitado como argumento.<br>
	 * @param bd El {@link BigDecimal} por el que se quiere multiplicar el actual
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal multiplica( BigDecimal bd ) throws Exception {
		return multiplica( new NumeroDecimal( bd ) );
	}

	/**
	 * Multiplicacion entre el {@link NumeroDecimal} actual y el {@link String} facilitado como argumento.<br>
	 * @param s El {@link String} por el que se quiere multiplicar el actual
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal multiplica( String s ) throws Exception {
		return multiplica( new NumeroDecimal( s ) );
	}



	/**
	 * Division de el {@link NumeroDecimal} actual por el {@link NumeroDecimal} facilitado como argumento.<br>
	 * @param nd El {@link NumeroDecimal} por el que se quiere dividir el actual
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal divide( NumeroDecimal nd ) throws Exception {
		NumeroDecimal div;
		try {
			div = new NumeroDecimal(
				this.getValor().divide( nd.getValor() ),
				this.longParteEntera,
				this.longParteDecimal,
				this.marcaDecimalEntrada,
				this.marcaDecimalSalida
			);
		} catch (ArithmeticException ae) {
			/**ArithmeticException if the exact quotient does not have a terminating decimal expansion (Capturamos esta excepción extendiendo la Precisión a 32 que es un potencia de 2 con holgura más que suficiente para evitar desviaciones con 2 o 3 decimales significativos.)*/
			div = new NumeroDecimal(
				this.getValor().divide( nd.getValor(), new MathContext( Constantes.PRECISION_REDONDEO_POR_TRUNCAMIENTO_EN_DIVISION_BigDecimal, RoundingMode.DOWN) ),
				this.longParteEntera,
				this.longParteDecimal,
				this.marcaDecimalEntrada,
				this.marcaDecimalSalida
			);
		}
		return div;
	}

	/**
	 * Division de el {@link NumeroDecimal} actual por el {@link Integer} facilitado como argumento.<br>
	 * @param i El {@link Integer} por el que se quiere dividir el actual
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal divide( Integer i ) throws Exception {
		return divide(  new NumeroDecimal( i )  );
	}

	/**
	 * Division de el {@link NumeroDecimal} actual por el {@link Float} facilitado como argumento.<br>
	 * @param f El {@link Float} por el que se quiere dividir el actual
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal divide( Float f ) throws Exception {
		return divide(  new NumeroDecimal( f )  );
	}

	/**
	 * Division de el {@link NumeroDecimal} actual por el {@link BigDecimal} facilitado como argumento.<br>
	 * @param bd El {@link BigDecimal} por el que se quiere dividir el actual
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal divide( BigDecimal bd ) throws Exception {
		return divide(  new NumeroDecimal( bd )  );
	}

	/**
	 * Division de el {@link NumeroDecimal} actual por el {@link String} facilitado como argumento.<br>
	 * @param s El {@link String} por el que se quiere dividir el actual
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal divide( String s ) throws Exception {
		return divide(  new NumeroDecimal( s )  );
	}



	/**
	 * Suma entre el {@link NumeroDecimal} actual y el facilitado como argumento.<br>
	 * @param nd El {@link NumeroDecimal} que se quiere sumar con el actual.
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal suma( NumeroDecimal nd ) throws Exception {
		return new NumeroDecimal(
			this.getValor().add( nd.getValor() ),
			this.longParteEntera,
			this.longParteDecimal,
			this.marcaDecimalEntrada,
			this.marcaDecimalSalida
		);
	}

	/**
	 * Suma entre el {@link NumeroDecimal} actual y el valor facilitado como argumento.<br>
	 * @param i El {@link Integer} que se quiere sumar con el actual.
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal suma( Integer i ) throws Exception {
		return suma(  new NumeroDecimal(i)  );
	}

	/**
	 * Suma entre el {@link NumeroDecimal} actual y el valor facilitado como argumento.<br>
	 * @param f El {@link Float} que se quiere sumar con el actual.
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal suma( Float f ) throws Exception {
		return suma(  new NumeroDecimal(f)  );
	}

	/**
	 * Suma entre el {@link NumeroDecimal} actual y el valor facilitado como argumento.<br>
	 * @param bd El {@link BigDecimal} que se quiere sumar con el actual.
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal suma( BigDecimal bd ) throws Exception {
		return suma(  new NumeroDecimal( bd )  );
	}

	/**
	 * Suma entre el {@link NumeroDecimal} actual y el valor facilitado como argumento.<br>
	 * @param s El {@link String} que se quiere sumar con el actual.
	 * @return
	 * @throws Exception Los 2 objetos NumeroDecimal no son compatibles entre si, ambos deben de compartir la misma marca decimal, longitudParteEntera, longitudParteDecimal y Contexto.
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal suma( String s ) throws Exception {
		return suma(  new NumeroDecimal( s )  );
	}



	/**
	 * Resta entre el {@link NumeroDecimal} actual y el {@link NumeroDecimal} facilitado como argumento.<br>
	 * @param nd El {@link NumeroDecimal} que se quiere restar al actual.
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal resta( NumeroDecimal nd ) throws Exception {
		return new NumeroDecimal(
			this.getValor().subtract( nd.getValor() ),
			this.longParteEntera,
			this.longParteDecimal,
			this.marcaDecimalEntrada,
			this.marcaDecimalSalida
		);
	}

	/**
	 * Resta entre el {@link NumeroDecimal} actual y el {@link Integer} facilitado como argumento.<br>
	 * @param i El {@link Integer} que se quiere restar al actual.
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal resta( Integer i ) throws Exception {
		return resta(  new NumeroDecimal( i )  );
	}

	/**
	 * Resta entre el {@link NumeroDecimal} actual y el {@link Float} facilitado como argumento.<br>
	 * @param f El {@link Float} que se quiere restar al actual.
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal resta( Float f ) throws Exception {
		return resta(  new NumeroDecimal( f )  );
	}

	/**
	 * Resta entre el {@link NumeroDecimal} actual y el {@link BigDecimal} facilitado como argumento.<br>
	 * @param nd El {@link BigDecimal} que se quiere restar al actual.
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal resta( BigDecimal bd ) throws Exception {
		return resta(  new NumeroDecimal( bd )  );
	}

	/**
	 * Resta entre el {@link NumeroDecimal} actual y el {@link String} facilitado como argumento.<br>
	 * @param nd El {@link String} que se quiere restar al actual.
	 * @return
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	public NumeroDecimal resta( String s ) throws Exception {
		return resta(  new NumeroDecimal( s )  );
	}



	/**
	 * Se aplica la operación de negación al propio {@link NumeroDecimal}<br>
	 * No devuelve ningun valor para evitar caer en el error de utilizar el valor negado de un Objeto sin caer en la cuenta que estamos negando el objeto subyacente, de forma efectiva, desvirtuando posibles utilizaciones de la citada instancia subyacente. 
	 */
	public void niega() {
		this.valor = this.valor.negate();
		this.negativo = (this.valor.compareTo( BigDecimal.ZERO ) < 0 );
	}



	/**
	 * Se aplica la operación de negación a un nuevo {@link NumeroDecimal} sin modificar la instancia sobre la que se aplica.<br>
	 * @return Un nuevo {@link NumeroDecimal} con la mismas caracteristicas que al que se aplica el método pero con el valor negado.
	 * @throws Exception
	 */
	public NumeroDecimal negado() throws Exception {
		NumeroDecimal ndnegado = new NumeroDecimal( this.valor );
		ndnegado.setValor( ndnegado.getValor().negate() );
		return ndnegado;
	}



	/**
	 * @author API
	 * @since 14 Julio 2011
	 * Este nuevo método de validación permite serializar a un número de decimales distinto del por defecto.
	 * @param posicionesDecimales Con este valor podemos utilizar una cantidad de decimales diferente a la definida para la instancia.
	 * @return
	 * @deprecated API 26 Julio 2011 Pese a que es completamente funcional y hasta la fecha obtiene el resultado esperado en los casos de prueba, Se realizan algunos procesos un poco artesanales que han podido ser evitados en la nueva versión aplicaRedondeoYFormatea.
	 */
	private PartesNumeroDecimal formateaPartes( Integer posicionesDecimales ) {
		long entrada = 0l;
		if ( logger.isDebugEnabled() ) entrada = new Date().getTime();
		if ( posicionesDecimales == null ) posicionesDecimales = this.longParteDecimal;
		MathContext mc = new MathContext( posicionesDecimales, RoundingMode.HALF_EVEN );
		String pDecimal = this.partes.getDecimal();
		String pEntera = this.partes.getEntera();
		if ( pDecimal.length() > posicionesDecimales ) {
			if (  pDecimal.startsWith( Constantes.CADENA_0 )  ) {
				pDecimal = StringUtils.replaceOnce( pDecimal, Constantes.CADENA_0, Constantes.CADENA_1 );
				pDecimal = new BigDecimal( pDecimal ).round( mc ).toPlainString();
				/**API 14 Julio 2011 Problematico el 1 se puede convertir en 2, mejor restar 1 a la primera posición decimal que no volver a sustituir.*/
				pDecimal = new StringBuffer()
					.append(  Integer.parseInt(  pDecimal.substring( 0, 1 )  ) - 1   )
					.append(  pDecimal.substring( 1, posicionesDecimales )  )
					.toString();
			} else /**API 26 Julio 2011 Detectamos la posibilidad de que el redondeo afecte a la parte entera *,99X** o *,999X*** */
				if (   pDecimal.startsWith(  StringUtils.rightPad( Constantes.CADENA_VACIA, posicionesDecimales, Constantes.CADENA_9 )  )   ) {
					if ( pDecimal.substring( posicionesDecimales, posicionesDecimales+1 ).compareTo( Constantes.CADENA_5 ) >= 0 ) {
						pEntera = String.valueOf(  Long.parseLong( pEntera ) + 1  );
						pDecimal = StringUtils.rightPad( Constantes.CADENA_VACIA, posicionesDecimales, Constantes.CADENA_0 );
					} else //El tercer decimal es menor que 5 redondeamos hacia abajo y no tocamos la parte entera.
						pDecimal = StringUtils.rightPad( Constantes.CADENA_VACIA, posicionesDecimales, Constantes.CADENA_9 );
				} else
					pDecimal = new BigDecimal( pDecimal ).round( mc ).toPlainString();
		}
		pDecimal = StringUtils.substring( pDecimal, 0, posicionesDecimales );
		pDecimal = StringUtils.rightPad( pDecimal, posicionesDecimales, Constantes.CADENA_0 );
		PartesNumeroDecimal pnd = new PartesNumeroDecimal(
			StringUtils.leftPad( pEntera, this.longParteEntera, Constantes.CADENA_0 ),
			StringUtils.left( pDecimal, posicionesDecimales )
		);
		if ( logger.isDebugEnabled() )
			logger.debug(  new StringBuffer("El método formateaPartes ha estado en ejecución ").append( new Date().getTime() - entrada ).append( " ms" )  );
		return pnd;
	}



	/**
	 * @author API
	 * @since 21 Octubre 2011
	 * Este nuevo método se lo utilizan tanto el compareTo, como el equals. Es el criterio de igualdad maestro.
	 * Comparación con el valor redondeado a 2 posiciones decimales y la parte entera sin padear.
	 * @return
	 */
	private PartesNumeroDecimal aplicaRedondeoYTroceaEnPartes() {
		return aplicaRedondeoYTroceaEnPartes( null, false );
	}



	/**
	 * @author API
	 * @since 22 Julio 2011
	 * @modified 28 Mayo 2012
	 * Este metodo genera las Partes Entera y Decimal del {@link NumeroDecimal} subyacente, aplicando el redondeo con la precisión especificada.<br>Además permite especificar si queremos extender con 0's por la izquierda la parte entera, hasta la longitud especificada incialmente en el constructor, que por defecto, lo ponemos a 13.
	 * @param posicionesDecimales Con este valor podemos indicar la cantidad de cifras significativas a utilizar para redondear el valor del {@link NumeroDecimal}.
	 * @param padeaParteEntera Con este flag indicamos si queremos que el resultado final se obtenga con la parte entera extendida con 0's por la izquierda hasta completar la longitud especificada en el atributo longParteEntera o no.
	 * @return
	 */
	private PartesNumeroDecimal aplicaRedondeoYTroceaEnPartes( Integer posicionesDecimales, boolean padeaParteEntera ) {
		long entrada = 0l;
		if ( logger.isDebugEnabled() ) entrada = new Date().getTime();
		boolean reemplazar = false;
		if ( posicionesDecimales == null ) posicionesDecimales = this.longParteDecimal;
		/**API 25 Julio 2011 Ya nos hemos asegurado que la parte entera se ha guardado sin ceros por la izquierda, por tanto, no es necesario limpiarla de nuevo.
		 * API 25 Mayo 2012 Pero ojo, que tenemos que tomar el valor absoluto, para evitar cuando el número sea negativo no encontrarnos con serializaciones del tipo -0000000-26,93*/
		BigDecimal valorARedondear = this.getValor().abs();
		String pDecimal = this.partes.getDecimal();
		String pEntera = this.partes.getEntera();
		int precision = posicionesDecimales + pEntera.length();
		if ( pDecimal.length() > posicionesDecimales ) { /**Solo es necesario redondear si tenemos una precision superior a las posiciones decimales deseadas.*/
			if ( pEntera.equals( Constantes.CADENA_0 )  ) {
				pEntera = Constantes.CADENA_1;
				valorARedondear = generaBigDecimal( pEntera, pDecimal );
				reemplazar = true;
			}
			String valorformateado = valorARedondear.round(  new MathContext( precision, RoundingMode.HALF_EVEN )  ).toPlainString();
			String[] partes = StringUtils.split( valorformateado, Constantes.MARCA_DECIMAL_PUNTO );
			pEntera = partes[0];
			if ( partes.length > 1 ) pDecimal = partes[1];
			else pDecimal = Constantes.CADENA_00;
		}
		if ( reemplazar ) pEntera = new Long( Long.parseLong( pEntera ) - 1 ).toString();
		pDecimal = StringUtils.substring( pDecimal, 0, posicionesDecimales );
		pDecimal = StringUtils.rightPad( pDecimal, posicionesDecimales, Constantes.CADENA_0 );
		PartesNumeroDecimal pnd = new PartesNumeroDecimal(
			( padeaParteEntera ? StringUtils.leftPad( pEntera, this.longParteEntera, Constantes.CADENA_0 ) : pEntera ),
			StringUtils.left( pDecimal, posicionesDecimales )
		);
		if ( logger.isDebugEnabled() )
			logger.debug(  new StringBuffer("El método aplicaRedondeoYFormatea ha estado en ejecución ").append( new Date().getTime() - entrada ).append( " ms." )  );
		return pnd;
	}



	/**
	 * Se encarga de generar las partes entera y decimal una vez asignado el valor {@link BigDecimal}
	 * @throws Exception El importe a representar excede el espacio de representacion.
	 */
	private void generaPartesEnteraYDecimal() throws Exception {
		if ( this.partes == null ) this.partes = new PartesNumeroDecimal();
		String[] sImporte = StringUtils.split( this.valor.toPlainString(), Constantes.MARCA_DECIMAL_PUNTO );
		this.negativo = StringUtils.contains( sImporte[0], Constantes.SIGNOMENOS );
		if ( this.negativo )
			this.partes.setEntera(  StringUtils.remove( sImporte[0], Constantes.SIGNOMENOS )  );
		else
			this.partes.setEntera( sImporte[0] );
		if ( sImporte.length > 1 ) {
			this.partes.setDecimal( sImporte[1] );
		} else
			this.partes.setDecimal( Constantes.CADENA_00 );
		if ( !validarImporte() )
			throw new Exception("El importe a representar excede el espacio de representacion.");
	}



	/**
	 * Se encarga de calcular el valor {@link BigDecimal} al que representa este {@link NumeroDecimal}.
	 * Contempla correctamente los nuemros negativos.
	 * @author API
	 * @since 25 Julio 2011
	 */
	private void calcularValor() {
		StringBuffer sValor = new StringBuffer();
		if ( this.consigno && this.negativo && !StringUtils.contains( this.partes.getEntera(), Constantes.SIGNOMENOS ) ) sValor.append( Constantes.SIGNOMENOS );
		sValor.append( this.partes.getEntera() ).append( Constantes.MARCA_DECIMAL_PUNTO ).append( this.partes.getDecimal() );
		this.valor = new BigDecimal( sValor.toString() );
		this.partes.setEntera(  StringUtils.remove( this.partes.getEntera(), Constantes.SIGNOMENOS )  );
	}



	/**
	 * @author API
	 * @since 26 Julio 2011
	 * Funcion auxiliar que construye un BigDecimal a partir de los parametros facilitados.
	 * @param pEntera
	 * @param pDecimal
	 * @return
	 */
	private BigDecimal generaBigDecimal( String pEntera, String pDecimal ) {
		return new BigDecimal(  new StringBuffer( pEntera ).append( Constantes.MARCA_DECIMAL_PUNTO ).append( pDecimal ).toString()  );
	}



	/**
	 * comprueba que la longitud de la parte entera no exceda el tamaño maximo asignado al NumeroDecimal que por defecto es 13
	 * @return 
	 */
	public boolean validarImporte() {
		return ( this.partes.getEntera().length() > this.longParteEntera ? false : true );
	}



	/**
	 * @return la respresentacion en un String formateado de acuerdo a los atributos especificados en la instancia actual.
	 */
	public String serializa() {
		return serializa( null );
	}


	/**
	 * Se incluye el signo en la serializacion en el caso de que el atributo consigno sea cierto.
	 * @param posicionesDecimales El numero de posiciones decimales a las que queremos redondear.
	 * @return la respresentacion en un String formateado de acuerdo a los atributos especificados en la instancia actual.
	 */
	public String serializa( Integer posicionesDecimales ) {
		return serializa( posicionesDecimales, true );
	}



	/**
	 * Se incluye el signo en la serializacion en el caso de que el atributo consigno sea cierto.
	 * @param posicionesDecimales El numero de posiciones decimales a las que queremos redondear.
	 * @param padeado Indica si extendemos con 0's por la izquierda la parte entera o no.
	 * @return la respresentacion en un String formateado de acuerdo a los atributos especificados en la instancia actual.
	 */
	public String serializa( Integer posicionesDecimales, boolean padeado ) {
		long entrada = 0l;
		if ( logger.isDebugEnabled() ) entrada = new Date().getTime();
		if ( posicionesDecimales == null ) posicionesDecimales = this.longParteDecimal;
		PartesNumeroDecimal pnd = aplicaRedondeoYTroceaEnPartes( posicionesDecimales, padeado );
		StringBuffer importe = new StringBuffer();
		if ( this.consigno && this.negativo )
			importe.append( Constantes.SIGNOMENOS );
		importe.append( pnd.getEntera() ).append( this.marcaDecimalSalida ).append( pnd.getDecimal() );
		if ( logger.isDebugEnabled() )
			logger.debug(  new StringBuffer("El método serializa ha estado en ejecucion ").append( new Date().getTime() - entrada ).append( " ms" )  );
		return importe.toString();
	}



	/**
	 * Funcion para depurar la Clase y su funcionamiento, utilizado para mostrar por pantalla el valor y serialización de una instancia {@link NumeroDecimal}.
	 * @param descripcion
	 */
	public void muestra( String descripcion ) {
		muestra( descripcion, null  );
	}



	/**
	 * Funcion para depurar la Clase y su funcionamiento, utilizado para mostrar por pantalla el valor y serialización de una instancia {@link NumeroDecimal}.
	 * @param descripcion
	 * @param posicionesDecimales
	 */
	public void muestra( String descripcion, Integer posicionesDecimales ) {
		if ( descripcion == null ) descripcion = Constantes.CADENA_VACIA;
		if ( posicionesDecimales == null ) posicionesDecimales = this.longParteDecimal;
		System.out.println(  new StringBuffer( descripcion ).append( " utilizando ").append( posicionesDecimales )
			.append( " decimales" ).append( " >>> valor [").append( this.getValor() ).append( "] serializacion '")
			.append( this.serializa(posicionesDecimales) ).append( Constantes.COMILLA )
		);
	}



	/**
	 * Funcion para depurar la Clase y su funcionamiento, utilizado para almacenar en un log el valor y serialización de una instancia {@link NumeroDecimal}.
	 * @param descripcion 
	 */
	public void loga( String descripcion ) {
		loga( descripcion, null);
	}



	/**
	 * Funcion para depurar la Clase y su funcionamiento, utilizado para almacenar en un log el valor y serialización de una instancia {@link NumeroDecimal}.
	 * @param descripcion 
	 */
	public void loga( String descripcion, Integer posicionesDecimales ) {
		if ( descripcion == null ) descripcion = Constantes.CADENA_VACIA;
		if ( posicionesDecimales == null ) posicionesDecimales = this.longParteDecimal;
		if ( logger.isTraceEnabled() )
			logger.trace(  new StringBuffer( descripcion ).append( " >>> valor [").append( this.getValor() ).append( "] serializacion '").append( this.serializa( posicionesDecimales ) ).append( Constantes.COMILLA )  );
		else if ( logger.isDebugEnabled() )
			logger.debug(  new StringBuffer( descripcion ).append( " >>> valor [").append( this.getValor() ).append( "] serializacion '").append( this.serializa( posicionesDecimales ) ).append( Constantes.COMILLA )  );
		else if ( logger.isInfoEnabled() )
			logger.info(  new StringBuffer( descripcion ).append( " >>> valor [").append( this.getValor() ).append( "] serializacion '").append( this.serializa( posicionesDecimales ) ).append( Constantes.COMILLA )  );
		else if ( logger.isWarnEnabled() )
			logger.warn(  new StringBuffer( descripcion ).append( " >>> valor [").append( this.getValor() ).append( "] serializacion '").append( this.serializa( posicionesDecimales ) ).append( Constantes.COMILLA )  );
		else if ( logger.isErrorEnabled() )
			logger.error(  new StringBuffer( descripcion ).append( " >>> valor [").append( this.getValor() ).append( "] serializacion '").append( this.serializa( posicionesDecimales ) ).append( Constantes.COMILLA )  );
		else if ( logger.isFatalEnabled() )
			logger.fatal(  new StringBuffer( descripcion ).append( " >>> valor [").append( this.getValor() ).append( "] serializacion '").append( this.serializa( posicionesDecimales ) ).append( Constantes.COMILLA )  );
	}

}
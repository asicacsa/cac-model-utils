package utiles;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import javax.mail.MessagingException;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.UrlResource;
import utiles.auxiliar.Correo;
import es.di.framework.acegisecurity.context.httpinvoker.CustomCommonsHttpInvokerRequestExecutor;
import es.di.framework.acegisecurity.utils.UserSessionCache;
import es.di.framework.service.controller.ISOAServiceController;



/**
 * @author Angel Parez Izquierdo
 * @since 1 Junio 2007
 * @modified 15 Octubre 2010
 * @version 4.0 Se han unificado los distintos metodos de autenticacion en uno solo, al que se puede invocar con distintos parámetros.<hr>Se ha intentado capturar mas informacion ante un fallo de autenticacion y se han revisado todos los posibles mensajes de error de realizaLlamadaServicio.<hr>Se va a dar soporte a las llamadas y Autenticación por SOA, que se usaran de forma predeterminada y si no es posible se recurrirá a los .invoke.
 *
 */
public class InvocadorServiciosHttp {
	
	/**Logger asociado a la clase InvocadorServiciosHttp
	private static final Logger log = Logger.getLogger( InvocadorServiciosHttp.class );*/
	private static final Log log = LogFactory.getLog( InvocadorServiciosHttp.class );
	
	/*Contexto de la aplicación ya configurado con Spring.<br>Este es el atributo más importante de la Clase, gracias a el nos llevaremos detrás toda la configuración preparada.*
	private ApplicationContext context;*/

	/**Direccion IP sobre la que se levanta la capa de servicios.*/
	private String services_IP;
	/**Direccion IP sobre la que se levanta la capa de servicios.*/
	private int services_port;
	/**Manejador que permite realizar las llamadas a la capa de servicios por SOA.*/
	private ISOAServiceController manejadorServicos;
	/**Manejador que permite realizar la autenticación ante la capa de servicios por SOA.*/
	private ISOAServiceController manejadorAutenticacion;
	/**Invocador que acabará realizando las llamadas a través de invoke en ultima instancia.*/
	private CustomCommonsHttpInvokerRequestExecutor invoker;
	
	
	
	/**
	 * Constructor por defecto.
	 * Se encarga de construir el Cliente Http configurado conforme lo montamos desde el aplication via Spring<br>aunque en nuestro caso lo configuramos en el código directamente.<br>Atención con este constructor, no tenemos ni el Contexto de la aplicación, ni podemos realizar llamadas por SOA.<br>Se desaconseja utilizar este constructor a menos que no haya más remedio. 
	 * 
	 * @param IPservicios {@link String} que representa la dirección IP de la capa de servicios.
	 * @param PuertoServicios Valor entero que representa el puerto en el que se espera escuche la capa de servicios.
	 */
	public InvocadorServiciosHttp( String IPservicios, int PuertoServicios ) {
		
		this.services_IP = IPservicios;
		this.services_port = PuertoServicios;
		
		MultiThreadedHttpConnectionManager mtcm = new MultiThreadedHttpConnectionManager();
		mtcm.setMaxConnectionsPerHost( Constantes.NUM_MAX_CONEXIONES_POR_HUESPED );
		mtcm.setMaxTotalConnections( Constantes.NUM_MAX_TOTAL_CONEXIONES );
		HttpClient clienteHttp = new HttpClient();
		clienteHttp.setHttpConnectionManager( mtcm );
		
		EhCacheManagerFactoryBean cacheManager = new EhCacheManagerFactoryBean();
		try {
			cacheManager.setConfigLocation(  new UrlResource( Constantes.URL_FICHERO_CONFIGURACION_CACHE_USUARIO )  );
		} catch (MalformedURLException mue) {
			log.error("La ruta por defecto para cargar la configuracion de la cache de usuarios no existe");
		}
		
		UserSessionCache userCacheSesion = new UserSessionCache();
		userCacheSesion.setUserCacheName( Constantes.NOMBRE_CACHE_SESION_USUARIO );
		
		CustomCommonsHttpInvokerRequestExecutor clienteHIRE = new CustomCommonsHttpInvokerRequestExecutor();
		clienteHIRE.setUserCache( userCacheSesion );
		clienteHIRE.setHttpClient(clienteHttp);
		
		this.invoker = clienteHIRE;
		this.manejadorServicos = null;
		this.manejadorAutenticacion = null;
		
		/*this.manejadorServicos = (ISOAServiceController)context.getBean( Constantes.NOMBRE_BEAN_httpServiceSOA );
		this.invoker = (CustomCommonsHttpInvokerRequestExecutor)context.getBean( Constantes.NOMBRE_BEAN_authenticationInvokerHttp );
		this.manejadorAutenticacion = (ISOAServiceController)context.getBean( Constantes.NOMBRE_BEAN_serviceSOA );*/
	}
	
	
	
	/**
	 * Constructor completo, para ser utilizado desde dentro del application para integrar tecnologias distintas a Laszlo.
	 * El cliente http en este caso ya nos llega configurado correctamente, y con su {@link MultiThreadedHttpConnectionManager}
	 * 
	 * @deprecated Se mantiene por compatibilidad con las primeras versiones.<hr>En nuevos desarollos no utilizar, no esta garantizada la transaccionalidad en llamadas desde una aplicación remota realizando funcionalidades que requieran interacción con más de un servicio.
	 *  
	 * @param IPservicios {@link String} que representa la dirección IP de la capa de servicios.
	 * @param PuertoServicios Valor entero que representa el puerto en el que se espera escuche la capa de servicios.
	 * @param clienteConfigurado Cliente Http parcialmente configurado, pues necesita un manejador de la Cache de Usuarios, para convertirse en un {@link CustomCommonsHttpInvokerRequestExecutor}
	 * 
	 */
	public InvocadorServiciosHttp( String IPservicios, int PuertoServicios, HttpClient clienteConfigurado ) {
		
		this.services_IP = IPservicios;
		this.services_port = PuertoServicios;
		
		EhCacheManagerFactoryBean cacheManager = new EhCacheManagerFactoryBean();
		try {
			cacheManager.setConfigLocation(  new UrlResource( Constantes.URL_FICHERO_CONFIGURACION_CACHE_USUARIO )  );
		} catch (MalformedURLException mue) {
			log.error("La ruta por defecto para cargar la configuracion de la cache de usuarios no existe");
		}
		UserSessionCache userCacheSesion = new UserSessionCache();
		userCacheSesion.setUserCacheName( Constantes.NOMBRE_CACHE_SESION_USUARIO );
		
		this.invoker.setUserCache( userCacheSesion );
		/**Este cliente ya viene previamente configurado desde el application, con lo cual no es necesario añadirle el el conection Manager.*/
		this.invoker.setHttpClient( clienteConfigurado );
		this.manejadorServicos = null;
		this.manejadorAutenticacion = null;
	}
	
	
	
	/**
	 * Constructor completo, para ser utilizado desde dentro del application para integrar tecnologias distintas a Laszlo.
	 * El cliente http en este caso ya nos llega configurado correctamente, y con su {@link MultiThreadedHttpConnectionManager}
	 * 
	 * @param IPservicios {@link String} que representa la dirección IP de la capa de servicios.
	 * @param PuertoServicios Valor entero que representa el puerto en el que se espera escuche la capa de servicios.
	 * @param ac Contexto de la aplicación application que ya ha sido previamente configurada via Spring inicializando todos los beans.
	 */
	public InvocadorServiciosHttp( String IPservicios, int PuertoServicios, ApplicationContext ac ) {
		this.services_IP = IPservicios;
		this.services_port = PuertoServicios;
		this.manejadorAutenticacion = (ISOAServiceController)ac.getBean( Constantes.NOMBRE_BEAN_serviceSOA );
		this.manejadorServicos = (ISOAServiceController)ac.getBean( Constantes.NOMBRE_BEAN_httpServiceSOA );
		this.invoker = (CustomCommonsHttpInvokerRequestExecutor)ac.getBean( Constantes.NOMBRE_BEAN_authenticationInvokerHttp );
	}
	
	
	
//	/**
//	 * Realiza el Login ante la capa de Servicios de la aplicacian de Ticketing.
//	 * 
//	 * @param credenciales datos de autenticacion facilitados.
//	 * @param inetAddress direccion IP del cliente prefijada para que la sesion que se nos asigne se asigne al cliente en lugar de al servidor. 
//	 * @return {@link String} conteniendo el idUsuario asignado a las credenciales facilitadas.<br><br>En caso de error, devuelve una estructura xml con 2 atributos:<br><span style="padding-left:10px"><b>1. </b>El código resultante de realizar la peticion Http.</span><br><span style="padding-left:10px"><b>2. </b>Un mensaje descriptivo del mismo.</span>
//	 */
//	private String autenticarseAnteServicesObteniendoUsuarioInvoke( String credenciales, String inetAddress ) {
//		
//		/**Se encarga de recoger el posible mensaje de la excepción devuelta por el servicio.*/
//		String mExcServicio = Constantes.CADENA_VACIA;
//		
//		byte[] codificado = Base64.encodeBase64( credenciales.getBytes() );
//		StringBuffer sbbasic = new StringBuffer( Constantes.MARCA_AUTENTICACION_BASICA )
//			.append( Constantes.ESPACIO_BLANCO )
//			.append( new String( codificado ) );
//		Header cabecera = new Header( Constantes.NOMBRE_CABECERA_AUTENTICACION, sbbasic.toString() );
//		
//		StringBuffer xmllogin = new StringBuffer(Constantes.MARCA_INICIO_PARAMETRO_XML_AUTENTICACION);
//		if ( inetAddress == null ) {
//			try {
//				xmllogin.append( InetAddress.getLocalHost().getHostAddress() );
//			}
//			catch ( UnknownHostException uhe ) {
//				//if ( log.isEnabledFor( Level.WARN ) ) {
//				if ( log.isWarnEnabled() ) {
//					StringBuffer warning = new StringBuffer( Constantes.MSG_ERROR_OBTENIENDO_IP_CLIENTE_LOGIN );
//					warning.append(this.services_IP);
//					log.warn( warning.toString() );
//				}
//				xmllogin.append( this.services_IP );
//			}
//		}
//		else {
//			xmllogin.append( inetAddress );
//		}
//		xmllogin.append( Constantes.MARCA_CIERRE_PARAMETRO_XML_AUTENTICACION );
//		
//		StringBuffer url = new StringBuffer( Constantes.MARCA_PROTOCOLO_HTTP_EN_URL );
//		url.append( this.services_IP );
//		url.append( Constantes.INDICADOR_PUERTO_EN_URL );
//		url.append( this.services_port );
//		url.append( Constantes.SEPARADOR_URLs );
//		url.append( Constantes.NOMBRE_CAPA_SERVICIOS );
//		url.append( Constantes.SEPARADOR_URLs );
//		url.append( Constantes.NOMBRE_SERVICIO_LOGIN );
//		url.append( Constantes.TIPO_PUBLICACION_SERVICIOS_INVOKE );
//		
//		PostMethod mlogin = new PostMethod( url.toString() );
//		mlogin.setRequestHeader( cabecera );
//		mlogin.addParameter( Constantes.NOMBRE_PARAMETRO_xml, xmllogin.toString() );
//		mlogin.addParameter( Constantes.NOMBRE_PARAMETRO_exe, Constantes.CADENA_1 );
//		
//		int codigoMetodo = 0;
//		try {
//			//codigoMetodo = this.clienteHttp.executeMethod( mlogin );
//			codigoMetodo = this.invoker.getHttpClient().executeMethod( mlogin );
//		} catch ( HttpException e1 ) {
//			//Status 400: The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications.
//			e1.printStackTrace();
////			codigoMetodo = HttpStatus.SC_BAD_REQUEST;
//			mExcServicio = e1.getMessage();
//		} catch ( IOException e2 ) {
//			e2.printStackTrace();
////			codigoMetodo = HttpStatus.SC_INTERNAL_SERVER_ERROR;
//			mExcServicio = e2.getMessage();
//		}
//		
//		if ( codigoMetodo != HttpStatus.SC_OK ) {
//			StringBuffer menerror = new StringBuffer( Constantes.NODO_XML_error_APERTURA );
//			menerror.append( Constantes.NODO_XML_codigo_APERTURA );
//			menerror.append( codigoMetodo );
//			menerror.append( Constantes.NODO_XML_codigo_CIERRE );
////			menerror.append( Constantes.NODO_XML_mensaje_APERTURA );
//			if (codigoMetodo == HttpStatus.SC_UNAUTHORIZED) 
//				menerror.append(Constantes.MSG_ERROR_AUTENTICACION); 
//			else
//				menerror.append(Constantes.MSG_ERROR_LLAMADA_AUTENTICACION);
//			menerror.append( Constantes.MSG_ERROR_LLAMADA_AUTENTICACION );
//			menerror.append( Constantes.SALTO_LINEA );
//			menerror.append( url );
//			menerror.append( Constantes.SALTO_LINEA );
//			menerror.append( xmllogin );
//			menerror.append( Constantes.SALTO_LINEA );
//			menerror.append( mExcServicio );
//			menerror.append( Constantes.NODO_XML_mensaje_CIERRE );
//			menerror.append( Constantes.NODO_XML_error_CIERRE );
//			//if (  log.isEnabledFor( Level.ERROR )  )
//			if (  log.isErrorEnabled()  )
//				log.error(menerror);
//			/**
//			 * API 5 Octubre 2010
//			 * Se incluye para liberar conexiones contra la capa de servicios. Estudiar comportamiento.
//			 */
//			mlogin.releaseConnection();
//			return menerror.toString(); 
//		}
//		else {
//			String resp = StringUtils.substringAfter( mlogin.getResponseBodyAsString(), Constantes.ETIQUETA_RESPUESTA_LOGIN_USUARIO );
//			resp = StringUtils.substringBetween( resp, Constantes.NODO_XML_value_APERTURA, Constantes.NODO_XML_value_CIERRE );
//			
//			// JMH (05.04.2017): Integración ExperTicket
//			if (log.isInfoEnabled()) log.info("InvocadorServicios.java (autenticarseAnteServicesObteniendoUsuarioInvoke): resp = " + resp);
//			// JMH (05.04.2017): Integración ExperTicket
//			
//			/**
//			 * API 5 Octubre 2010
//			 * Se incluye para liberar conexiones contra la capa de servicios. Estudiar comportamiento.
//			 */
//			mlogin.releaseConnection();
//			return StringUtils.trim( resp );
//		}
//	}
	
	
	/**
	 * Realiza el Login ante la capa de Servicios de la aplicacian de Ticketing.
	 * 
	 * @param credenciales datos de autenticacion facilitados.
	 * @param inetAddress direccion IP del cliente prefijada para que la sesion que se nos asigne se asigne al cliente en lugar de al servidor. 
	 * @return {@link String} conteniendo el idUsuario asignado a las credenciales facilitadas.<br><br>En caso de error, devuelve una estructura xml con 2 atributos:<br><span style="padding-left:10px"><b>1. </b>El código resultante de realizar la peticion Http.</span><br><span style="padding-left:10px"><b>2. </b>Un mensaje descriptivo del mismo.</span>
	 */
	private String autenticarseAnteServicesObteniendoUsuarioInvoke( String credenciales, String inetAddress ) throws Exception {
		
		/**Se encarga de recoger el posible mensaje de la excepción devuelta por el servicio.*/
		String mExcServicio = Constantes.CADENA_VACIA;
		
		byte[] codificado = Base64.encodeBase64( credenciales.getBytes() );
		StringBuffer sbbasic = new StringBuffer( Constantes.MARCA_AUTENTICACION_BASICA )
			.append( Constantes.ESPACIO_BLANCO )
			.append( new String( codificado ) );
		Header cabecera = new Header( Constantes.NOMBRE_CABECERA_AUTENTICACION, sbbasic.toString() );
		
		StringBuffer xmllogin = new StringBuffer(Constantes.MARCA_INICIO_PARAMETRO_XML_AUTENTICACION);
		if ( inetAddress == null ) {
			try {
				xmllogin.append( InetAddress.getLocalHost().getHostAddress() );
			}
			catch ( UnknownHostException uhe ) {
				//if ( log.isEnabledFor( Level.WARN ) ) {
				if ( log.isWarnEnabled() ) {
					StringBuffer warning = new StringBuffer( Constantes.MSG_ERROR_OBTENIENDO_IP_CLIENTE_LOGIN );
					warning.append(this.services_IP);
					log.warn( warning.toString() );
				}
				xmllogin.append( this.services_IP );
			}
		}
		else {
			xmllogin.append( inetAddress );
		}
		xmllogin.append( Constantes.MARCA_CIERRE_PARAMETRO_XML_AUTENTICACION );
		
		StringBuffer url = new StringBuffer( Constantes.MARCA_PROTOCOLO_HTTP_EN_URL );
		url.append( this.services_IP );
		url.append( Constantes.INDICADOR_PUERTO_EN_URL );
		url.append( this.services_port );
		url.append( Constantes.SEPARADOR_URLs );
		url.append( Constantes.NOMBRE_CAPA_SERVICIOS );
		url.append( Constantes.SEPARADOR_URLs );
		url.append( Constantes.NOMBRE_SERVICIO_LOGIN );
		url.append( Constantes.TIPO_PUBLICACION_SERVICIOS_INVOKE );
		
		PostMethod mlogin = new PostMethod( url.toString() );
		mlogin.setRequestHeader( cabecera );
		mlogin.addParameter( Constantes.NOMBRE_PARAMETRO_xml, xmllogin.toString() );
		mlogin.addParameter( Constantes.NOMBRE_PARAMETRO_exe, Constantes.CADENA_1 );
		
		int codigoMetodo = 0;
		try {
			//codigoMetodo = this.clienteHttp.executeMethod( mlogin );
			codigoMetodo = this.invoker.getHttpClient().executeMethod( mlogin );
		} catch ( HttpException e1 ) {
			//Status 400: The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications.
			e1.printStackTrace();
//			codigoMetodo = HttpStatus.SC_BAD_REQUEST;
			mExcServicio = e1.getMessage();
		} catch ( IOException e2 ) {
			e2.printStackTrace();
//			codigoMetodo = HttpStatus.SC_INTERNAL_SERVER_ERROR;
			mExcServicio = e2.getMessage();
		}
		
		if ( codigoMetodo != HttpStatus.SC_OK ) {
			StringBuffer menerror = new StringBuffer( Constantes.NODO_XML_error_APERTURA );
			menerror.append( Constantes.NODO_XML_codigo_APERTURA );
			menerror.append( codigoMetodo );
			menerror.append( Constantes.NODO_XML_codigo_CIERRE );
			menerror.append( Constantes.NODO_XML_mensaje_APERTURA );
			if (codigoMetodo == HttpStatus.SC_UNAUTHORIZED) 				
				menerror.append(Constantes.MSG_ERROR_AUTENTICACION);
			else
				menerror.append(Constantes.MSG_ERROR_LLAMADA_AUTENTICACION);
			
			menerror.append( Constantes.SALTO_LINEA );
			menerror.append( url );
			menerror.append( Constantes.SALTO_LINEA );
			menerror.append( xmllogin );
			menerror.append( Constantes.SALTO_LINEA );
			menerror.append( mExcServicio );
			menerror.append( Constantes.NODO_XML_mensaje_CIERRE );
			menerror.append( Constantes.NODO_XML_error_CIERRE );
			
			//if (  log.isEnabledFor( Level.ERROR )  )
			if (log.isErrorEnabled()) log.error(menerror);
			
			/**
			 * API 5 Octubre 2010
			 * Se incluye para liberar conexiones contra la capa de servicios. Estudiar comportamiento.
			 */
			mlogin.releaseConnection();
			return menerror.toString(); 
		}
		else {
			String resp = StringUtils.substringAfter( mlogin.getResponseBodyAsString(), Constantes.ETIQUETA_RESPUESTA_LOGIN_USUARIO );
			resp = StringUtils.substringBetween( resp, Constantes.NODO_XML_value_APERTURA, Constantes.NODO_XML_value_CIERRE );
			
			// JMH (05.04.2017): Integración ExperTicket
			if (log.isInfoEnabled()) log.info("InvocadorServicios.java (autenticarseAnteServicesObteniendoUsuarioInvoke): resp = " + resp);
			// JMH (05.04.2017): Integración ExperTicket
			
			/**
			 * API 5 Octubre 2010
			 * Se incluye para liberar conexiones contra la capa de servicios. Estudiar comportamiento.
			 */
			mlogin.releaseConnection();
			return StringUtils.trim( resp );
		}
	}
	
	/**
	 * Realiza el Login ante la capa de Servicios de la aplicacian de Ticketing.
	 * 
	 * @param credenciales datos de autenticacion facilitados.
	 * @param inetAddress direccion IP del cliente prefijada para que la sesion que se nos asigne se asigne al cliente en lugar de al servidor. 
	 * @return {@link String} conteniendo el idUsuario asignado a las credenciales facilitadas.<br><br>En caso de error, devuelve una estructura xml con 2 atributos:<br><span style="padding-left:10px"><b>1. </b>El código resultante de realizar la peticion Http.</span><br><span style="padding-left:10px"><b>2. </b>Un mensaje descriptivo del mismo.</span>
	 * @throws Exception
	 */
	private String autenticarseAnteServicesObteniendoUsuarioSOA( String credenciales, String inetAddress ) throws Exception {
		
		StringBuffer valorXml = new StringBuffer( Constantes.MARCA_INICIO_PARAMETRO_XML );
		if (inetAddress != null) valorXml.append( inetAddress );
		valorXml.append( Constantes.MARCA_CIERRE_PARAMETRO_XML );
		
		String respuesta = this.manejadorAutenticacion.process( Constantes.NOMBRE_SERVICIO_LOGIN, valorXml.toString() );
		
		if ( log.isInfoEnabled() )
			log.info( new StringBuffer( Constantes.NOMBRE_SERVICIO_LOGIN ).append( Constantes.CONECTOR_VALOR_DEVUELTO_POR_SERVICIO ).append( respuesta ) );
		return respuesta;
	}
	
	
	
	/**
	 * Realiza el Login ante la capa de Servicios de la aplicacian de Ticketing.
	 * @param credenciales datos de autenticacion facilitados.
	 * @return {@link String} conteniendo el idUsuario asignado a las credenciales facilitadas.<br><br>En caso de error, devuelve una estructura xml con 2 atributos:<br><span style="padding-left:10px"><b>1. </b>El código resultante de realizar la peticion Http.</span><br><span style="padding-left:10px"><b>2. </b>Un mensaje descriptivo del mismo.</span>
	 * @throws Exception
	 */
	public String autenticarseAnteServicesObteniendoUsuario( String credenciales, String inetAddress ) throws Exception {
		if ( this.manejadorServicos == null )
			return autenticarseAnteServicesObteniendoUsuarioInvoke(credenciales, null );
		else
			return autenticarseAnteServicesObteniendoUsuarioSOA( credenciales, null );
	}
	
	
	/**
	 * Realiza el Login ante la capa de Servicios de la aplicacian de Ticketing.
	 * @param credenciales datos de autenticacion facilitados.
	 * @return {@link String} conteniendo el idUsuario asignado a las credenciales facilitadas.<br><br>En caso de error, devuelve una estructura xml con 2 atributos:<br><span style="padding-left:10px"><b>1. </b>El código resultante de realizar la peticion Http.</span><br><span style="padding-left:10px"><b>2. </b>Un mensaje descriptivo del mismo.</span>
	 * @throws Exception
	 */
//	public String autenticarseAnteServicesObteniendoUsuario( String credenciales ) throws Exception {
//		return autenticarseAnteServicesObteniendoUsuario( credenciales, null );
//	}

	
	/**
	 * Realiza el Login ante la capa de Servicios de la aplicacian de Ticketing.
	 * @param credenciales datos de autenticacion facilitados.
	 * @return {@link String} conteniendo el idUsuario asignado a las credenciales facilitadas.<br><br>En caso de error, devuelve una estructura xml con 2 atributos:<br><span style="padding-left:10px"><b>1. </b>El código resultante de realizar la peticion Http.</span><br><span style="padding-left:10px"><b>2. </b>Un mensaje descriptivo del mismo.</span>
	 * @throws Exception
	 */
	public String autenticarseAnteServicesObteniendoUsuario( String credenciales ) throws Exception {
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String mensajeFijo = "ERROR en " + this.getClass().getName() + "." + methodName + "() con los parámetros ";
		String mensajeVariable = null ; //"idcanal: " + idcanal.toString() + " fecha: " + fecha.toString();
		String mensaje = mensajeFijo +  mensajeVariable;
		
//		if (credenciales == null || credenciales.length() == 0) {
//			msg = "ERROR en " + this.getClass().getName() + ".autenticarseAnteServicesObteniendoUsuario(): las credenciales son null.";
//		}
		
		try {
			return autenticarseAnteServicesObteniendoUsuario( credenciales, null );
		}
		catch (Exception e) {
			try {
				String remitente, destinatario, asunto = null;
				
				// Obtención de valores de la tabla CONFIGURACION
				remitente = Constantes.INTEGRACION_COLOSSUS_EXPERTICKET_REMITENTE_EMAIL;
				destinatario = Constantes.INTEGRACION_COLOSSUS_EXPERTICKET_DESTINATARIO_EMAIL;
				asunto = Constantes.INTEGRACION_COLOSSUS_EXPERTICKET_ASUNTO_EMAIL;
				//if (msg == null) msg = ConfiguracionUtil.getStringConfiguracionPorNombre(getSession(), "internet.correo.error.Experticket.mensaje");
			
				Correo correo = new Correo();
				correo.setRemitente(remitente);
				Validate.notNull(destinatario, "El destinatario del email es null.");
				correo.setDestinatario(destinatario);
				correo.setAsunto(asunto);
				correo.setContenidoHtml(false);
				correo.setContenido(mensaje);
				correo.enviarCorreo(false);
				return mensaje;
			} catch (MessagingException me) {
				mensaje = mensaje + " Excepción: " + me.getMessage();
				return mensaje;
			} catch (UnsupportedEncodingException uee) {
				mensaje = mensaje + " Excepción: " + uee.getMessage();
				return mensaje;
			} catch (IllegalArgumentException iae) {
				mensaje = mensaje + " Excepción: " + iae.getMessage();
				return mensaje;				
			}
		}
		finally {
			if (log.isErrorEnabled()) log.error(mensaje);
		}		
	}
	
	
	/**
	 * Realiza el Login ante la capa de Servicios de la aplicacian de Ticketing. Este metodo se incluyó a partir de la incorporación del
	 * gestor de pases de Club, ya que el código del invocador se ejecuta realmente en servidor y no en cliente, por lo que precisa un
	 * nuevo parametro que simule la autenticacion desde la maquina cliente. Esto es importante para la impresion de recibos, en los que
	 * la taquilla desde la que se autentique es determinante.
	 * @return El valor logico cierto si la autenticacian ha sido posible o el valor logico falso si se ha producido algun error.
	 * @throws Exception 
	 */
	public boolean autenticarseAnteServices( String credenciales, String inetAddress ) throws Exception {
		if ( this.manejadorAutenticacion == null )
			return ( !this.autenticarseAnteServicesObteniendoUsuarioInvoke( credenciales, inetAddress ).startsWith( Constantes.NODO_XML_error_APERTURA ) );
		else
			return ( !this.autenticarseAnteServicesObteniendoUsuarioSOA( credenciales, inetAddress ).startsWith( Constantes.NODO_XML_error_APERTURA ) );
	}
	
	
	/**
	 * Realiza el Login ante la capa de Servicios de la aplicacian de Ticketing.
	 * @return El valor logico cierto si la autenticacian ha sido posible o el valor logico falso si se ha producido algun error.
	 * @throws Exception 
	 */
	public boolean autenticarseAnteServices( String credenciales ) throws Exception {
		return autenticarseAnteServices( credenciales, null );
	}
	
	
	
	/**
	 * Hace la llamada a un servicio asociandole los parametros especificados en el xml.
	 * @param nombresServicio Nombre del servicio al que se desea llamar.
	 * @param xml Cadena de texto que contiene los parametros necesarios para realizar la llamada al servicio especificado.
	 * @return Un String que contiene una estructura xml filtrada a partir de la respuesta que proporciona el servicio invocado mediante un Petician Http .invoke
	 */
	
	/**
	 * 
		 200 OK (HTTP/1.0 - RFC 1945)  SC_OK = 200 --> OK
		 500 Server Error (HTTP/1.0 - RFC 1945)  SC_INTERNAL_SERVER_ERROR = 500 --> MSG_ERROR_LLAMADA_SERVICIO = "Se ha producido un error durante la llamada al servicio ";
		 400 Bad Request (HTTP/1.1 - RFC 2616)  SC_BAD_REQUEST = 400 --> MSG_ERROR_URL_MAL_FORMADA = "La llamada al servicio no esta bien formada, verifique la sintaxis del nombre de servicio y de los parametros: ";
		 401 Unauthorized (HTTP/1.0 - RFC 1945)  SC_UNAUTHORIZED = 401 --> MSG_ERROR_SERVICIO_NO_AUTORIZADO = "No tiene permisos suficientes para utilizar el servicio ";
		 404 Not Found (HTTP/1.0 - RFC 1945)  SC_NOT_FOUND = 404 --> MSG_ERROR_SERVICIO_NO_ENCONTRADO = "Servicio no publicado o inexistente: ";	 
	 */
	private String realizaLlamadaServicio( String nombreServicio, String xml ) throws Exception {
		
		String respuesta;
		/**Se encarga de recoger el posible mensaje de la excepción devuelta por el servicio.*/
		String mExcServicio = Constantes.CADENA_VACIA;
		StringBuffer menerror = new StringBuffer();
		
		StringBuffer url = new StringBuffer( Constantes.MARCA_PROTOCOLO_HTTP_EN_URL );
		url.append( this.services_IP ); 
		url.append( Constantes.INDICADOR_PUERTO_EN_URL );
		url.append( this.services_port );
		url.append( Constantes.SEPARADOR_URLs ); 
		url.append( Constantes.NOMBRE_CAPA_SERVICIOS );
		url.append( Constantes.SEPARADOR_URLs ); 
		url.append( nombreServicio ); 
		url.append( Constantes.TIPO_PUBLICACION_SERVICIOS_INVOKE );
		
		PostMethod metodo = new PostMethod( url.toString() );
		StringBuffer valorXml = new StringBuffer( Constantes.MARCA_INICIO_PARAMETRO_XML );
		if (xml != null) valorXml.append( xml );
		valorXml.append( Constantes.MARCA_CIERRE_PARAMETRO_XML );
		metodo.addParameter( Constantes.NOMBRE_PARAMETRO_xml, valorXml.toString() );
		metodo.addParameter( Constantes.NOMBRE_PARAMETRO_exe, Constantes.CADENA_1);
		
		int codigoMetodo = 0;
		// JMH (02.03.2017): EXPERTICKET
		String responseBody = null;
		// JMH (02.03.2017): EXPERTICKET
		try {
			if (log.isInfoEnabled()) log.info("InvocadorServiciosHttp.java (realizaLlamadaServicio): Antes de HttpClient.executeMethod("+ nombreServicio + ") con parámetro xml = " + xml);
			
			codigoMetodo = this.invoker.getHttpClient().executeMethod(metodo);
			if (log.isInfoEnabled()) log.info("InvocadorServiciosHttp.java: Despues de HttpClient.executeMethod("+ nombreServicio + ")...");

			// JMH (02.03.2017): EXPERTICKET
			responseBody = metodo.getStatusText(); // .getResponseBodyAsString();			
			if (codigoMetodo != HttpStatus.SC_OK) {
				log.error("Código devuelto: " + codigoMetodo + " Respuesta: " + responseBody + "StatusLine: " + metodo.getStatusLine());
			}
		} catch (HttpException e3) {
			e3.printStackTrace();
			//Status 400: The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications.
//			codigoMetodo = HttpStatus.SC_BAD_REQUEST;
			if ( e3.getMessage() != null )
				mExcServicio = e3.getMessage();
			log.error( mExcServicio );
		} catch (IOException e4) {
			e4.printStackTrace();
//			codigoMetodo = HttpStatus.SC_INTERNAL_SERVER_ERROR;
			if ( e4.getMessage() != null )
//				mExcServicio = e4.getMessage();
				mExcServicio = new StringBuffer().append(e4.getMessage()).append(metodo.getStatusLine()).toString();
			log.error( mExcServicio );
		} catch (Exception e5) {
			e5.printStackTrace();
			//Status 500: Error interno del Servidor, no especificado.
//			codigoMetodo = HttpStatus.SC_INTERNAL_SERVER_ERROR;
			if ( e5.getMessage() != null ) 
				mExcServicio = e5.getMessage();
//				mExcServicio = new StringBuffer().append(e5.getMessage()).append(metodo.getStatusText()).toString();
				
			log.error( mExcServicio );
			throw e5;
		} 
		
		
		StringBuffer sbComun = new StringBuffer( nombreServicio ).append( Constantes.SALTO_LINEA ).append( valorXml );
		StringBuffer sbExtension = new StringBuffer( Constantes.SALTO_LINEA ).append( mExcServicio );
		StringBuffer sbComunExtendido = new StringBuffer( sbComun ).append( sbExtension );
		
		menerror.append( Constantes.NODOS_XML_error_codigo_APERTURA ).append( codigoMetodo ).append( Constantes.NODOS_XML_codigocierre_mensajeapertura );
		
		if ( codigoMetodo == HttpStatus.SC_BAD_REQUEST ) {
			menerror.append( Constantes.MSG_ERROR_URL_MAL_FORMADA ).append( url ).append( Constantes.SALTO_LINEA ).append( valorXml ).append( Constantes.NODOS_XML_mensaje_error_CIERRE );
			log.error( menerror.toString() );
			return menerror.toString();
		}
		
		if (codigoMetodo == HttpStatus.SC_UNAUTHORIZED ) {
			menerror.append( "(NO AUTORIZADO) " + Constantes.MSG_ERROR_SERVICIO_NO_AUTORIZADO ).append( sbComun ).append( Constantes.NODOS_XML_mensaje_error_CIERRE );
			log.error( HttpStatus.SC_UNAUTHORIZED + "(UNAUTHORIZED): " + menerror.toString() );
			return menerror.toString();
		}
		
		if ( codigoMetodo == HttpStatus.SC_NOT_FOUND ) {
			menerror.append( Constantes.MSG_ERROR_SERVICIO_NO_ENCONTRADO ).append( sbComun ).append( Constantes.NODOS_XML_mensaje_error_CIERRE );
			log.error( menerror.toString() );
			return menerror.toString();
		}
		
		if ( codigoMetodo == HttpStatus.SC_INTERNAL_SERVER_ERROR ) {
			menerror.append( "HttpStatus.SC_INTERNAL_SERVER_ERROR: " + Constantes.MSG_ERROR_LLAMADA_SERVICIO ).append( sbComunExtendido ).append( Constantes.NODOS_XML_mensaje_error_CIERRE );
			log.error( menerror.toString() );
			return menerror.toString();
		}
		
		if ( codigoMetodo == HttpStatus.SC_OK ) {
			String resp = StringUtils.trim(
				StringUtils.substringBeforeLast(  metodo.getResponseBodyAsString(), Constantes.ETIQUETA_CIERRE_RESPUESTA_INVOKES  )
			);
			if (  resp.endsWith( Constantes.MARCA_CIERRE_NODO_XML_VACIO )  ) {
				respuesta = Constantes.ADVERTENCIA_LISTA_VACIA;
				if ( log.isInfoEnabled() )
					log.info( new StringBuffer( nombreServicio ).append( Constantes.CONECTOR_VALOR_DEVUELTO_POR_SERVICIO ).append( respuesta ) );
			}
			else {
				StringBuffer tagRespuesta = new StringBuffer( Constantes.MARCA_INICIO_ETIQUETA_XML );
				tagRespuesta.append(  StringUtils.substringAfterLast( resp, Constantes.MARCA_INICIO_CIERRE_ETIQUETA_XML )  );
				respuesta = tagRespuesta.toString() + StringUtils.substringAfterLast( resp, tagRespuesta.toString() );
			}
		}
		else { //cualquier codigo de error distinto de 200,400,401,404,500
			menerror.append( Constantes.MSG_ERROR_NO_CONTROLADO_LLAMADA_SERVICIO ).append( sbComunExtendido ).append( Constantes.NODOS_XML_mensaje_error_CIERRE );
			log.error(menerror);
			respuesta = menerror.toString();
		}
		
		if ( log.isInfoEnabled() )
			log.info( new StringBuffer( nombreServicio ).append( Constantes.CONECTOR_VALOR_DEVUELTO_POR_SERVICIO ).append( respuesta ) );
		
		/**
		 * API 5 Octubre 2010
		 * Se incluye para liberar conexiones contra la capa de servicios. Estudiar comportamiento.
		 */
		metodo.releaseConnection();
		
		return respuesta;
	}
	
	
	
	/**
	 * Hace la llamada a un servicio asociandole los parametros especificados en el xml.
	 * Utilizando SOA en lugar de .invoke.
	 * @param nombresServicio Nombre del servicio al que se desea llamar.
	 * @param xml Cadena de texto que contiene los parametros necesarios para realizar la llamada al servicio especificado.
	 * @return Un String que contiene una estructura xml filtrada a partir de la respuesta que proporciona el servicio invocado mediante un Petician Http .invoke
	 * @throws Exception
	 */
	private String realizaLlamadaServicioSOA( String nombreServicio, String xml ) throws Exception {
		
		String servicio = new StringBuffer( nombreServicio ).append( Constantes.SUFIJO_MAPEADOR_SERVICIO_SOA ).toString();
		
		StringBuffer valorXml = new StringBuffer( Constantes.MARCA_INICIO_PARAMETRO_XML );
		if (xml != null) valorXml.append( xml );
		valorXml.append( Constantes.MARCA_CIERRE_PARAMETRO_XML );
		String respuesta = null;
		
		try {
			respuesta = this.manejadorServicos.process( servicio, valorXml.toString() );
			
			if ( log.isInfoEnabled() )
				log.info( new StringBuffer( nombreServicio ).append( Constantes.CONECTOR_VALOR_DEVUELTO_POR_SERVICIO ).append( respuesta ) );
			return respuesta;
		}
		catch (Exception e) {
			log.error("InvocadorServiciosHttp.java (realizaLlamadaServicioSOA):" + e.getMessage());
			throw e;
		}
	}
	
	
	
	/**
	 * Realiza el login a ante la capa de servicios de forma automatica con el usuario especificado.<br>Lleva a cabo la llamadas al servicio especificado y obtiene la respuesta correspondiente.
	 * @param credenciales Cadena de texto en formato "login:password" en plano.
	 * @param nombreServicio Nombre del servicio al que se va a llamar sin incluir .invoke. Parametro REQUERIDO.
	 * @param xml Cadena de texto que representa un xml que contendra los parametros que espera el servicio. No incluir las etiquetas servicio ni parametro. En los servicios que no requieran parametros se DEBE pasar a null.
	 * @return Devuelve en caso de que no se haya producido ningun error un String que continene una pagina HTML que a su vez contiene un xml con la respuesta generada por el nombreServicio solicitado. 
	 * @throws Exception 
	 */
	public String invocaServicio( String credenciales, String nombreServicio, String xml ) throws Exception {
		if ( ! this.autenticarseAnteServices( credenciales ) ) 
			return Constantes.MSG_ERROR_AUTENTICACION;
		if ( this.manejadorServicos == null ) {
			if (log.isInfoEnabled()) log.info("InvocadorServiciosHTTP.java (invocaServicio): Llamando a realizaLlamadaServicio()");
		
			return this.realizaLlamadaServicio(nombreServicio, xml);
		}
		else {
			if (log.isInfoEnabled()) log.info("InvocadorServiciosHTTP.java (invocaServicio): Llamando a realizaLlamadaServicioSOA()");
			return this.realizaLlamadaServicioSOA( nombreServicio, xml );
		}
	}
	
	
	
	/**
	 * Lleva a cabo la llamadas al servicio especificado y obtiene la respuesta correspondiente.<br>Asume que estamos autenticados.
	 * @param nombreServicio Nombre del servicio al que se va a llamar sin incluir .invoke. Parametro REQUERIDO 
	 * @param xml Cadena de texto que representa un xml que contendra los parametros que espera el servicio. No incluir las etiquetas servicio ni parametro. En los servicios que no requieran parametros se DEBE pasar a null.
	 * @return Devuelve en caso de que no se haya producido ningun error un String que continene una pagina HTML que a su vez contiene un xml con la respuesta generada por el nombreServicio solicitado. 
	 * @throws Exception 
	 */
	public String invocaServicio( String nombreServicio, String xml ) throws Exception {
		if ( this.manejadorServicos == null ) 
			return this.realizaLlamadaServicio(nombreServicio, xml);
		else
			return this.realizaLlamadaServicioSOA(nombreServicio, xml);
	}
	
	
	
	/**
	 * Lleva a cabo la llamadas al servicio especificado y obtiene la respuesta correspondiente.<br>Asume que estamos autenticados.<br>UTILIZAR PARA SERVICIOS QUE DEVUELVEN OBJETOS BASICOS o SIMPLES como Integer, String, Float, ...
	 * @param nombreServicio Nombre del servicio al que se va a llamar sin incluir Constantes.TIPO_PUBLICACION_SERVICIOS. Parametro REQUERIDO 
	 * @param xml Cadena de texto que representa un xml que contendra los parametros que espera el servicio. No incluir las etiquetas servicio ni parametro. En los servicios que no requieran parametros se DEBE pasar a null.
	 * @return Devuelve en caso de que no se haya producido ningun error un String que continene una pagina HTML que a su vez contiene un xml con la respuesta generada por el nombreServicio solicitado.
	 * @throws Exception 
	 * @deprecated Ante un error en el servicio devuelve una respuesta indeterminada<br><b>NO UTILIZAR</b> 
	 */	
	public String invocaServicioRespuestaSimple( String nombreServicio, String xml ) throws Exception {
		if ( this.manejadorServicos == null ) 
			return StringUtils.substringBetween( this.realizaLlamadaServicio(nombreServicio, xml), Constantes.MARCA_FIN_ETIQUETA_XML, Constantes.MARCA_INICIO_ETIQUETA_XML);
		else
			return StringUtils.substringBetween( this.realizaLlamadaServicioSOA(nombreServicio, xml), Constantes.MARCA_FIN_ETIQUETA_XML, Constantes.MARCA_INICIO_ETIQUETA_XML);
	}
	
	
	
	/**
	 * Realiza el login ante la capa de servicios con las credenciales facilitadas.<br>Lleva a cabo las llamadas a los servicio especificados y obtiene las respuestas correspondientes en el mismo orden de petician.
	 * @param credenciales Cadena de texto en formato "login:password" en plano.
	 * @param nombresServicio Array de Nombres de los servicios a los que se va a llamar sin incluir Constantes.TIPO_PUBLICACION_SERVICIOS. Parametro REQUERIDO 
	 * @param xml Array de cadenas de texto que representan un xml que contendran los parametros que espera cada uno de los servicios. No incluir las etiquetas servicio ni parametro. En los servicios que no requieran parametros se puede pasar a null.
	 * @return Devuelve en caso de que no se haya producido ningun error un Array de Strings que continenen cada uno una pagina HTML que contiene un xml con la respuesta generada por cada servicio del array de nombresServicio solicitados. 
	 * @throws Exception 
	 */
	public String[] invocaServicio( String credenciales, String[] nombresServicio, String[] xml ) throws Exception {
		String[] respuesta = new String[nombresServicio.length];
		if ( ! this.autenticarseAnteServices(credenciales) ) { 
			respuesta[0] = "<error>Se ha producido un error al autenticarse ante la capa de servicios. Revise los logs.</error>";
			return respuesta;
		}
		for(int i=0; i<nombresServicio.length; i++)
			respuesta[i] = this.realizaLlamadaServicio( nombresServicio[i], xml[i] );
		return respuesta;
	}
	
	
	
	/**
	 * Realiza el login a ante la capa de servicios de forma automatica con el usuario adminangel.<br>Lleva a cabo las llamadas a los servicio especificados y obtiene las respuestas correspondientes en el mismo orden de petician.
	 * @param nombresServicio Array de Nombres de los servicios a los que se va a llamar sin incluir <b>.invoke</b>. Parametro REQUERIDO 
	 * @param xml Array de cadenas de texto que representan un xml que contendran los parametros que espera cada uno de los servicios. No incluir las etiquetas servicio ni parametro. En los servicios que no requieran parametros se puede pasar a null.
	 * @return Devuelve en caso de que no se haya producido ningun error un Array de Strings que continenen cada uno una pagina HTML que contiene un xml con la respuesta generada por cada servicio del array de nombresServicio solicitados. 
	 */
	public String[] invocaServicio( String[] nombresServicio, String[] xml ) throws Exception {
		String[] respuesta = new String[nombresServicio.length];
		for(int i=0; i<nombresServicio.length; i++)
			respuesta[i] = this.realizaLlamadaServicio( nombresServicio[i], xml[i] );
		return respuesta;
	}
	
	
	
	/**
	 * @deprecated El metodo estatico main solo se debe de usar para probar la funcionalidad.<br>Hay que crear un objeto del tipo de la clase InvocadorServiciosHttp y llamar a los métodos que necesitemos, pasando siempre por la autenticacion ante services, directa o indirectamente.
	 * @param args Argumentos que acepta la clase InvocadorServiciosHttp de forma estatica. 
	 */
	public static void main(String[] args) {
		
		String IP = "172.29.40.9";
		int puerto = 8080; 
		InvocadorServiciosHttp invocador = new InvocadorServiciosHttp( IP, puerto );
		
		String credenciales = "lkx01:lkx01";
		try {
			invocador.autenticarseAnteServices(credenciales);
		} catch (Exception e1) {
			e1.printStackTrace();
			log.error(  new StringBuffer( e1.getMessage() )  );
		}
		
		String servicio = "obtenerZonasPorRecinto";
		String xml = "<java.lang.Integer>1000</java.lang.Integer>";
		String respuesta;
		
		try {
			respuesta = invocador.invocaServicio( credenciales, servicio, xml );
			System.out.println( respuesta );
			System.out.println( "TERMINADA PRUEBA OK" );
			log.info( respuesta );
		} catch (Exception e) {
			StringBuffer sbe = new StringBuffer("Se ha producido un error en la llamada al servicio ").append( servicio ).append( Constantes.SALTO_LINEA ).append( e.getMessage() );
			System.out.println( sbe.toString() );
			log.error( sbe.toString() );
			e.printStackTrace();
		}
	}
	
}
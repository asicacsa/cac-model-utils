package utiles;

import org.apache.commons.lang.StringUtils;

/**
 * Va a representar un NumeroDecimal por sus representaciones textuales de los valores que encontramos antes y después de la marca decimal.
 * @author API
 * @since 4 Julio 2011
 *
 */
public class PartesNumeroDecimal implements Comparable<PartesNumeroDecimal> {
	
	private String entera;
	private String decimal;
	
	public PartesNumeroDecimal() {
		super();
	};
	
	public PartesNumeroDecimal(String entera, String decimal) {
		super();
		this.entera = entera;
		this.decimal = decimal;
	}

	public String getEntera() {
		return entera;
	}

	public void setEntera(String entera) {
		this.entera = entera;
	}

	public String getDecimal() {
		return decimal;
	}

	public void setDecimal(String decimal) {
		this.decimal = decimal;
	}


	/**
	 * De este modo nos abstraemos de la marca decimal para el criterio de unicidad/igualdad con lo que 30,25 sera igual a 30.25.
	 * Del mismo modo 00030,25 sera igual a 30.25 y 00030,25 igual a 030.250.
	 */
	public int hashCode() {
		return (  new Long( this.entera ).toString() + StringUtils.stripEnd( this.decimal, Constantes.CADENA_0 )  ).hashCode();
	}


	/**
	 * De este modo nos abstraemos de la marca decimal para el criterio de unicidad/igualdad con lo que 30,25 sera igual a 30.25.
	 * Del mismo modo 00030,25 sera igual a 30.25 y 00030,25 igual a 030.250.
	 * No utilizamos la opcion obvia de convertir a Long las 2 partes y sumarlas, porque hay mas colisiones que de este modo.
	 */
	public int compareTo(PartesNumeroDecimal o) {
		int clentera = new Long( this.entera ).compareTo(  new Long( o.getEntera() )  );
		if ( clentera == 0 )
			return StringUtils.stripEnd( this.decimal, Constantes.CADENA_0 ).compareTo(  StringUtils.stripEnd( o.getDecimal(), Constantes.CADENA_0 )  );
		else
			return clentera;
	}


	/**
	 * De este modo nos abstraemos de la marca decimal para el criterio de unicidad/igualdad con lo que 30,25 sera igual a 30.25.
	 * Del mismo modo 00030,25 sera igual a 30.25 y 00030,25 igual a 030.250.
	 * No utilizamos la opcion obvia de convertir a Long las 2 partes y sumarlas, porque hay mas colisiones que de este modo.
	 */
	public boolean equals( Object o ) {
		if ( o.getClass() != this.getClass() )
			return false;
		else {
			PartesNumeroDecimal pnd = (PartesNumeroDecimal)o;
			if (   new Long( pnd.getEntera() ).equals(  new Long( this.entera )  )   )
				return StringUtils.stripEnd( pnd.getDecimal(), Constantes.CADENA_0 ).equals(  StringUtils.stripEnd( this.decimal, Constantes.CADENA_0 )  );
			else
				return false;
		}
	}
}
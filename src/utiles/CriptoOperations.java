package utiles;

import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
//import java.security.interfaces.RSAKey;
import java.util.Enumeration;
import java.util.Iterator;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
//import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
//import org.bouncycastle.asn1.ASN1Sequence;
//import org.bouncycastle.asn1.pkcs.RSAPublicKey;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.jcajce.JcePGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyKeyEncryptionMethodGenerator;
import es.di.framework.exceptions.BusinessRuleException;
import utiles.auxiliar.Firma;

/**
 * Clase para el manejo de operaciones criptográficas.<br>
 * No requiere instanciación, pues es un compendio de métodos estáticos.
 * @author API
 * @since 2 Abril 2012
 *
 */
public class CriptoOperations {

	/**Nombre del Provider por defecto que facilita la clase {@link BouncyCastleProvider}*/
	public static final String BC = BouncyCastleProvider.PROVIDER_NAME;
	/**Logger asociado a la clase SrvInformeICAA*/
	protected static final Log LOG = LogFactory.getLog( CriptoOperations.class );
	/**Array de bytes utilizado como clave.<br>No podemos exceder 128 bits gracias a los protectores de la paz mundial, porque tienen que asegurarse que la pueden reventar cuando les venga en gana.*/
	private static final byte[] keyBytes = "ViVaYankeelandia".getBytes();



	/**
	 * Obtiene la cadena resultante de aplicar el algoritmo de resumen SHA-1 a una cadena de texto.<br>
	 * Esta función se aplica normalmente para obtener firmas, funciones hash o resumen.
	 * @author API
	 * @since 2 Abril 2012
	 * @param cadena
	 * @return
	 * @throws Exception
	 */
	public static String calculaHashSHA_1 ( String cadena ) throws Exception {
		MessageDigest Digester = null;
		try {
			Digester = MessageDigest.getInstance( Constantes.ALGORITMO_RESUMEN_SHA_1 );
		} catch ( NoSuchAlgorithmException e ) {
			e.printStackTrace();
		}
		byte[] resumen = Digester.digest( cadena.getBytes() );
		String result = new String(  Hex.encodeHex( resumen )  );
		return result;
	}


	//public static String descifrarRSA ( byte[] cadena, BufferedInputStream f ) throws Exception {
	public static String descifrarRSA ( byte[] cadena, Reader clavePublica ) throws Exception {
		
		 // Anadir provider JCE (provider por defecto no soporta RSA)
	     // Security.addProvider(new BouncyCastleProvider());  // Cargar el provider BC
		
		 // PASO 1: Crear e inicializar el par de claves RSA DE 512 bits
	     // KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", "BC"); // Hace uso del provider BC
		
		//File aFile = new File(nombreFichero);
		//declared here only to make visible to finally clause
		BufferedReader input = null;		
		//use buffering, reading one line at a time
		//FileReader always assumes default encoding is OK!
		input = new BufferedReader(clavePublica);
		String line = null; //not declared within while loop
		
		while ((line = input.readLine()) != null){
				System.out.println(line.toString());
		}
		
		 System.out.print("2. Introducir Texto Plano (max. 64 caracteres): ");
	     byte[] bufferCifrado = cadena;
	     byte[] bufferPlano = null;
	     // PASO 2: Crear cifrador RSA
	     Cipher cifrador = Cipher.getInstance("RSA", "BC"); // Hace uso del provider BC
		try {
			// La SecretKey es para clave simetrica Key clavePublica = new SecretKeySpec( keyBytes, algoritmoCifrado );
			//RSAPublicKey, RSAKey, Key???????????????Cual utilizamos? Qué contiene el fichero que nos pasan .kyp????
			
			//Key clavePublica = (Key) new RSAPublicKey( file );
			// PASO 3b: Poner cifrador en modo DESCIFRADO 
			cifrador.init(Cipher.DECRYPT_MODE, (Key) clavePublica); // Descrifra con la clave publica
	    
			System.out.println("4b. Descifrar con clave publica");
			bufferPlano = cifrador.doFinal(bufferCifrado);
			System.out.println("TEXTO DESCIFRADO");
			mostrarBytes(bufferPlano);
			System.out.println("\n-------------------------------");
			
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	 return bufferPlano.toString();
	}
	public static void mostrarBytes(byte [] buffer) {
		System.out.write(buffer, 0, buffer.length);
   } 
	/**
	 * @author API
	 * @since 29 Marzo 2012
	 * Este metodo cifra, utilizando el algoritmo de cifrado simetrico facilitado, el contenido del OutputStreamSalida y lo deposita en el fichero nombreAbsolutoFicheroCifrado.
	 * @param ruta Cadena de texto que representa la ubicación del fichero que contiene el informe a cifrar.
	 * @param nombreFicheroOrigen Cadena de texto que representa el nombre del fichero que contiene el informe a cifrar.
	 * @param charsetDestino Cadena de texto que representa el nombre del juego de caracteres en el que se espera obtener desencriptado el fichero a cifrar mediante PGP.
	 * @param epilogoFicheroRecodificado parte del nombre del fichero que indica que ha sido recodificado.
	 * @param streamSalida {@link FileOutputStream} que apunta a la ruta completa del fichero cifrado, en la que se depositara el contenido cifrado del informe original referenciado por el parémetro nombreAbsolutoFicheroOrigen.
	 * @param clavePublicaCifrado Clave de cifrado extraida a partir de un Conjunto de PKR's (PublicKeyRing's), con la que cifraremos el fichero especificado por los parametros ruta y nombreFicheroOrigen.
	 * @param algoritmoCifradoSimetrico Algoritmo de cifrado simetrico para 
	 * @param compatibilidadPGP26x Parametro que indica si el encriptador debe utilizar la compatibilidad con el PGP 2.6.x y anteriores o no. 
	 * @throws Exception
	 */
	private static void cifrarPGP(
		String ruta, String nombreFicheroOrigen, String charsetDestino, String epilogoFicheroRecodificado, OutputStream streamSalida,
		PGPPublicKey clavePublicaCifrado, Integer algoritmoCifradoSimetrico, boolean compatibilidadPGP26x, 
		boolean compresion, Integer tipoCompresion
	) throws Exception, IOException, NoSuchProviderException, PGPException {
		streamSalida = new ArmoredOutputStream( streamSalida );
		ByteArrayOutputStream arrayBytesSalida = new ByteArrayOutputStream();
		PGPCompressedDataGenerator compresorDatos = null;
		if ( compresion ) {
			//compresorDatos = new PGPCompressedDataGenerator( PGPCompressedDataGenerator.ZIP );
			compresorDatos = new PGPCompressedDataGenerator( tipoCompresion );
			arrayBytesSalida = (ByteArrayOutputStream)compresorDatos.open( arrayBytesSalida );
			compresorDatos.close();
		}
		if ( charsetDestino != null ) {
			Validate.notNull( epilogoFicheroRecodificado, "La extensión del fichero recodificado es requerida" );
			
			//JMH (15.06.2018): ERROR en la codificación del fichero enviado. Debe ser windows-1252
//			PGPUtil.writeFileToLiteralData(  arrayBytesSalida, PGPLiteralData.BINARY, IOOperations.recodificaFichero( ruta, nombreFicheroOrigen, epilogoFicheroRecodificado, charsetDestino )  );
			File ficherorecodificado = IOOperations.recodificaFichero(ruta, nombreFicheroOrigen, epilogoFicheroRecodificado, charsetDestino);
			PGPUtil.writeFileToLiteralData(arrayBytesSalida, PGPLiteralData.BINARY, ficherorecodificado);
		} else
			PGPUtil.writeFileToLiteralData(  arrayBytesSalida, PGPLiteralData.BINARY, new File( ruta + nombreFicheroOrigen )  );
		PGPEncryptedDataGenerator generadorDatosEncriptadosPGP = new PGPEncryptedDataGenerator(
			new JcePGPDataEncryptorBuilder( algoritmoCifradoSimetrico )
				.setSecureRandom( new SecureRandom() )
				.setProvider( BC ),
			compatibilidadPGP26x //Indica si queremos compatibilidad con formato antiguo
		);
		if ( LOG.isInfoEnabled() ) {
			StringBuilder sb = new StringBuilder("La Clave publica de cifrado que se nos facilita contiene los siguientes campos:");
			sb.append( "\ngetAlgorithm() ").append( Constantes.PIPE ).append( clavePublicaCifrado.getAlgorithm() ).append( Constantes.PIPE )
				.append( "\ngetBitStrength() ").append( Constantes.PIPE ).append( clavePublicaCifrado.getBitStrength() ).append( Constantes.PIPE )
				.append( "\ngetKeyID() ").append( Constantes.PIPE ).append( clavePublicaCifrado.getKeyID() ).append( Constantes.PIPE )
				.append( "\ngetValidDays() ").append( Constantes.PIPE ).append( clavePublicaCifrado.getValidDays() ).append( Constantes.PIPE )
				.append( "\ngetValidSeconds() ").append( Constantes.PIPE ).append( clavePublicaCifrado.getValidSeconds() ).append( Constantes.PIPE )
				.append( "\ngetVersion() ").append( Constantes.PIPE ).append( clavePublicaCifrado.getVersion() ).append( Constantes.PIPE )
				.append( "\ngetCreationTime() ").append( Constantes.PIPE ).append( clavePublicaCifrado.getCreationTime() ).append( Constantes.PIPE )
				.append( "\ngetEncoded() ").append( Constantes.PIPE ).append( clavePublicaCifrado.getEncoded() ).append( Constantes.PIPE )
				.append( "\ngetFingerprint() ").append( Constantes.PIPE ).append( clavePublicaCifrado.getFingerprint() ).append( Constantes.PIPE )
				.append( "\n.getPublicKeyPacket().getKey().getFormat() ").append( Constantes.PIPE ).append( clavePublicaCifrado.getPublicKeyPacket().getKey().getFormat() ).append( Constantes.PIPE )
				.append( "\ngetTrustData() ").append( Constantes.PIPE ).append( clavePublicaCifrado.getTrustData() ).append( Constantes.PIPE );
			LOG.info( sb );
		}
		generadorDatosEncriptadosPGP.addMethod(  new JcePublicKeyKeyEncryptionMethodGenerator( clavePublicaCifrado ).setProvider( BC )  );
		byte[] bytes = arrayBytesSalida.toByteArray();
		if ( LOG.isInfoEnabled() ) {
			StringBuilder sb = new StringBuilder( "\nEl generadorDatosEncriptadosPGP serializado es " ).append( Constantes.PIPE ).append( generadorDatosEncriptadosPGP.toString() ).append( Constantes.PIPE );
			sb.append( "\nEl arrayBytesSalida a encriptar es [" );
			for ( int j = 0; j<bytes.length-1; j++ )
				sb.append( bytes[j] ).append( Constantes.COMA ).append( Constantes.ESPACIO_BLANCO );
			sb.append( bytes[bytes.length-1] ).append( Constantes.ESPACIO_BLANCO ).append( Constantes.CIERRE_CORCHETE )
				.append("\nEl streamSalida contiene ").append( Constantes.PIPE ).append( streamSalida ).append( Constantes.PIPE );
			LOG.info( sb );
		}
		OutputStream contenidoCifrado = generadorDatosEncriptadosPGP.open( streamSalida, bytes.length );
		contenidoCifrado.write( bytes );
		generadorDatosEncriptadosPGP.close();
		streamSalida.close();
	}



	/**
	 * @author API
	 * @since 2 Abril 2012
	 * @param ruta
	 * @param nombreFichero
	 * @param nombreAbsolutoFicheroCifrado
	 * @param charsetDestino
	 * @param epilogoFicheroRecodificado
	 * @param ubicacionFicheroClavePublicaCifradoInformeICAA
	 * @param idClavePublicaCifrado
	 * @param algoritmoCifradoSimetrico
	 * @param modoCompatibilidadPGP26x
	 * @param compresion
	 * @param tipoCompresion
	 * @return
	 */
	public static String cifrarPGPYGuardarEnDisco(
		String ruta, String nombreFichero, String nombreAbsolutoFicheroCifrado, String charsetDestino, String epilogoFicheroRecodificado,
		String ubicacionFicheroClavePublicaCifradoInformeICAA, Long idClavePublicaCifrado, Integer algoritmoCifradoSimetrico, Integer modoCompatibilidadPGP26x,
		boolean compresion, Integer tipoCompresion
	) {
		try {
			Security.addProvider( new BouncyCastleProvider() );
			PGPPublicKeyRingCollection coleccionAnillosDeClavesPublicasPGP = new PGPPublicKeyRingCollection(  new FileInputStream( ubicacionFicheroClavePublicaCifradoInformeICAA )  );
			/**API 17 Enero 2012 Vamos a reconvertir esto, tenemos que extraer la clave de cifrado del ICAA, no la de Cibernos, pues esperan que llegue cifrado con la del ICAA. Finalmente la buscaremos por su ID.*/
			boolean nocifrado = true;
			long marca = 0;
			for ( Iterator<PGPPublicKeyRing> itPKRs = coleccionAnillosDeClavesPublicasPGP.getKeyRings(); itPKRs.hasNext() && nocifrado; ) {
				PGPPublicKeyRing PGPpkr = itPKRs.next();
				for( Iterator<PGPPublicKey> itPKs = PGPpkr.getPublicKeys(); itPKs.hasNext(); ) {
					PGPPublicKey pubKey = itPKs.next();
					if ( pubKey.isEncryptionKey() && pubKey.getKeyID() == idClavePublicaCifrado ) {
						if ( LOG.isInfoEnabled() ) {
							StringBuilder sb = new StringBuilder( "Llamando al metodo cifrarPGP con los siguientes parametros:" );
							sb.append( "\nruta " ).append( Constantes.PIPE ).append( ruta ).append( Constantes.PIPE )
								.append( "\nnombreFichero " ).append( Constantes.PIPE ).append( nombreFichero ).append( Constantes.PIPE )
								.append( "\ncharsetDestino " ).append( Constantes.PIPE ).append( charsetDestino ).append( Constantes.PIPE )
								.append( "\nepilogoFicheroRecodificado " ).append( Constantes.PIPE ).append( epilogoFicheroRecodificado ).append( Constantes.PIPE )
								.append( "\nnombreAbsolutoFicheroCifrado " ).append( Constantes.PIPE ).append( nombreAbsolutoFicheroCifrado ).append( Constantes.PIPE )
								.append( "\npubkey " ).append( Constantes.PIPE ).append( pubKey.toString() ).append( Constantes.PIPE )
								.append( "\nalgoritmoCifradoSimetrico " ).append( Constantes.PIPE ).append( algoritmoCifradoSimetrico ).append( Constantes.PIPE )
								.append( "\nmodoCompatibilidadPGP26x " ).append( Constantes.PIPE ).append( modoCompatibilidadPGP26x ).append( Constantes.PIPE )
								.append( "\ncompresion " ).append( Constantes.PIPE ).append( compresion ).append( Constantes.PIPE )
								.append( "\ntipoCompresion " ).append( Constantes.PIPE ).append( tipoCompresion ).append( Constantes.PIPE );
							LOG.info( sb );
						}
						cifrarPGP(
							ruta,
							nombreFichero,
							charsetDestino,
							epilogoFicheroRecodificado,
							new FileOutputStream( nombreAbsolutoFicheroCifrado ),
							pubKey,
							algoritmoCifradoSimetrico,
							modoCompatibilidadPGP26x.compareTo( Constantes.UNO ) == 0 ? true : false,
							compresion,
							tipoCompresion
						);
						nocifrado = false;
						marca = pubKey.getKeyID();
					}
				}
			}
			return new StringBuilder( "Informe cifrado con la clave cuyo id es ").append( marca ).append( " y guardado correctamente en " ).append( nombreAbsolutoFicheroCifrado ).toString();
		} catch ( Exception e ) {
			StringBuilder mensaje = new StringBuilder( e.getMessage() );
			if ( e.getCause() != null )
				mensaje.append( Constantes.PUNTO ).append( Constantes.ESPACIO_BLANCO ).append( e.getCause().getMessage() ).append( Constantes.SALTO_LINEA );
			StackTraceElement[] stes = e.getStackTrace();
			for( int i = 0; i< stes.length; i++ )
				mensaje.append( stes[i].toString() ).append( Constantes.SALTO_LINEA );
			LOG.fatal( mensaje );
			return mensaje.toString();
		}
	}



	/**
	 * @author API
	 * @since 20 Febrero 2012
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws BusinessRuleException 
	 * @throws DecoderException 
	 */
	private static char[] obtenerPassArrayDescifrado( String passwordParejaClavesFirmaCAC, String algoritmoCifrado ) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, BusinessRuleException, DecoderException {
		Validate.notNull( passwordParejaClavesFirmaCAC, "El password para obtener la firma digital no ha sido dado de alta todavía. Por favor, de de alta el password realizando una llamada al servicio: 'darDeAltaOModificarPasswordParejaClavesFirmaAutorizadaICAA( String password )' " );
		//Cipher descifrador = Cipher.getInstance( "AES" );
		Cipher descifrador = Cipher.getInstance( algoritmoCifrado );
		Key clave = new SecretKeySpec( keyBytes, algoritmoCifrado );
		descifrador.init( Cipher.DECRYPT_MODE, clave );
		byte[] hexDecodedPassword = Hex.decodeHex( passwordParejaClavesFirmaCAC.toCharArray() );
		byte[] arrayBytesDescifrados = descifrador.doFinal( hexDecodedPassword );
		String passwordDescifrado = new String( arrayBytesDescifrados );
		if ( LOG.isDebugEnabled() ) {
			StringBuilder db = new StringBuilder( "El passwordParejaClavesFirmaCAC encriptado es <" ).append( passwordParejaClavesFirmaCAC )
				.append( "> el arrayBytesDescifrados contiene [").append( arrayBytesDescifrados )
				.append("] y el String construido a partir del array de bytes es '").append( passwordDescifrado ).append( Constantes.COMILLA );
			LOG.debug( db );
		}
		return passwordDescifrado.toCharArray();
	}



	/**
	 * @author API
	 * @since 2 Abril 2012
	 * @return
	 * @throws DecoderException 
	 * @throws BusinessRuleException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchProviderException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws KeyStoreException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws CertificateException 
	 * @throws UnrecoverableKeyException 
	 */
	public static Firma obtenerFirma( String ubicacionFicheroFirma, String tipoKeyStoreFirma, String passwordParejaClavesFirma, String algoritmoCifradoDescifrado ) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, BusinessRuleException, DecoderException, KeyStoreException, CertificateException, FileNotFoundException, IOException, UnrecoverableKeyException {
		Firma firma = new Firma();
		char[] passArray = obtenerPassArrayDescifrado( passwordParejaClavesFirma, algoritmoCifradoDescifrado );
		KeyStore ks = KeyStore.getInstance( tipoKeyStoreFirma, BC );
		ks.load( new FileInputStream( ubicacionFicheroFirma ), passArray );
		Enumeration<String> aliasEnum = ks.aliases();
		Key key = null;
		Certificate certificado = null;
		while( aliasEnum.hasMoreElements() ) {
			String nombreClave = (String)aliasEnum.nextElement();
			key = ks.getKey( nombreClave, passArray );
			certificado = ks.getCertificate( nombreClave );
			
			// JMH (31.05.2017 18:26): El bucle es ineficiente e incorrecto,
			// ya que recorre todos los items del Keystore y ha llegado a darse el caso de finalizar con un certificado cuya key == null
			if (nombreClave.equalsIgnoreCase("FIRMA") && key != null && certificado != null) break;
			// JMH (31.05.2017 18:26): El bucle es ineficiente e incorrecto,
		}
		firma.setCertificado(certificado);
		firma.setKey(key);
		return firma;
	}



	/**
	 * @author API
	 * @since 2 Abril 2012
	 * @param cadena
	 * @param algoritmoSimetrico
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static String cifradoSimetrico( String cadena, String algoritmoSimetrico ) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cifrador = Cipher.getInstance( algoritmoSimetrico );
		SecretKeySpec clave = new SecretKeySpec( CriptoOperations.keyBytes, algoritmoSimetrico );
		cifrador.init( Cipher.ENCRYPT_MODE, clave );
		byte[] encryptedByteArray = cifrador.doFinal( cadena.getBytes() );
		String cadenaCifrada = String.copyValueOf(  Hex.encodeHex( encryptedByteArray )  );
		if ( LOG.isDebugEnabled() ) {
			StringBuilder db = new StringBuilder( " La cadena facilitada en plano es '" ).append( cadena )
				.append( "' el array de byte encriptado es <" ).append( encryptedByteArray )
				.append( "> y el valor guardado o cadenaCifrada |").append( cadenaCifrada ).append( Constantes.PIPE );
			LOG.debug( db );
		}
		return cadenaCifrada;
	}



	/**
	 * En caso de no facilitar el algoritmo se no se utiliza cifrado.
	 * @author API
	 * @since 3 Abril 2012
	 * @param descripcionAlgoritmo
	 * @return El valor entero correspondiente a la constante entera de la clase {@link PGPEncryptedData} que representa cada uno de los distintos algoritmos de cifrado simetricos soportados en la implementacion del Open PGP the The Legion of Bouncy Castle v1.47.
	 */
	public static Integer obtenerIdentificadorAlgoritmoPGPEncryptedData( String descripcionAlgoritmo ) {
		if (  descripcionAlgoritmo == null || descripcionAlgoritmo.compareTo( Constantes.CADENA_VACIA ) == 0 || descripcionAlgoritmo.compareTo( Constantes.CADENA_null ) == 0 || descripcionAlgoritmo.compareTo( Constantes.CADENA_NULL ) == 0 )
			return PGPEncryptedData.NULL;
		else if (  descripcionAlgoritmo.compareTo( Constantes.ALGORITMO_AES_128 ) == 0  )
			return PGPEncryptedData.AES_128;
		else if (  descripcionAlgoritmo.compareTo( Constantes.ALGORITMO_AES_192 ) == 0  )
			return PGPEncryptedData.AES_192;
		else if (  descripcionAlgoritmo.compareTo( Constantes.ALGORITMO_AES_256 ) == 0  )
			return PGPEncryptedData.AES_256;
		else if (  descripcionAlgoritmo.compareTo( Constantes.ALGORITMO_BLOWFISH ) == 0  )
			return PGPEncryptedData.BLOWFISH;
		else if (  descripcionAlgoritmo.compareTo( Constantes.ALGORITMO_CAST5 ) == 0  )
			return PGPEncryptedData.CAST5;
		else if (  descripcionAlgoritmo.compareTo( Constantes.ALGORITMO_DES ) == 0  )
			return PGPEncryptedData.DES;
		else if (  descripcionAlgoritmo.compareTo( Constantes.ALGORITMO_IDEA ) == 0  )
			return PGPEncryptedData.IDEA;
		else if (  descripcionAlgoritmo.compareTo( Constantes.ALGORITMO_SAFER ) == 0  )
			return PGPEncryptedData.SAFER;
		else if (  descripcionAlgoritmo.compareTo( Constantes.ALGORITMO_TRIPLE_DES ) == 0  )
			return PGPEncryptedData.TRIPLE_DES;
		else if (  descripcionAlgoritmo.compareTo( Constantes.ALGORITMO_TWOFISH ) == 0  )
			return PGPEncryptedData.TWOFISH;
		else
			return PGPEncryptedData.NULL;
	}

}
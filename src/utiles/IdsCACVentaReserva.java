/**
 * 
 */
package utiles;

import org.apache.commons.lang.StringUtils;

import utiles.Constantes;

/**
 * @author Angel Perez Izquierdo
 * @since 26 Diciembre 2007
 *
 */
public class IdsCACVentaReserva implements Comparable<IdsCACVentaReserva> {

	/**Solo tomara valor en el caso de productos combinados. Para los casos de producto simple estara a null*/
	private Integer idProducto;
	private Integer idPerfilvisitante;
	private Integer idZonasesion;
	private Integer idZona;
	private Integer idSesion;
	private Float importe;

	
	/**
	 * Constructor Por Defecto, vacio.
	 *
	 */
	public IdsCACVentaReserva() {}
	
	
	/**
	 * @param idProducto
	 * @param idContenido
	 * @param idSesion
	 * @param idPerfil
	 * @param idTarifa
	 * @param idZona
	 */
	public IdsCACVentaReserva(Integer idProducto, Integer idPerfil, Integer idZonasesion, Integer idZona, Integer idSesion, Float importe) {
		if (idProducto == null) this.idProducto = null;
		else this.idProducto = idProducto;
		this.idPerfilvisitante = idPerfil;
		this.idZonasesion = idZonasesion;
		this.idZona = idZona;
		this.idSesion = idSesion;
		this.importe = importe;
	}


	
	/**
	 * @return the idProducto
	 */
	public Integer getIdProducto() {
		return this.idProducto;
	}



	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}


	/**
	 * @return the idPerfilvisitante
	 */
	public Integer getIdPerfilvisitante() {
		return this.idPerfilvisitante;
	}



	/**
	 * @param idPerfilvisitante the idPerfilvisitante to set
	 */
	public void setIdPerfilvisitante(Integer idPerfilvisitante) {
		this.idPerfilvisitante = idPerfilvisitante;
	}



	/**
	 * @return the idZonasesion
	 */
	public Integer getIdZonasesion() {
		return this.idZonasesion;
	}

	
	
	/**
	 * @param idZonasesion the idZonasesion to set
	 */
	public void setIdZonasesion(Integer idZonasesion) {
		this.idZonasesion = idZonasesion;
	}
	
	
	
	

	/**
	 * @return the idSesion
	 */
	public Integer getIdSesion() {
		return idSesion;
	}

	/**
	 * @param idSesion the idSesion to set
	 */
	public void setIdSesion(Integer idSesion) {
		this.idSesion = idSesion;
	}

	/**
	 * @return the idZona
	 */
	public Integer getIdZona() {
		return idZona;
	}

	/**
	 * @param idZona the idZona to set
	 */
	public void setIdZona(Integer idZona) {
		this.idZona = idZona;
	}

	/**
	 * @return the importe
	 */
	public Float getImporte() {
		return this.importe;
	}



	/**
	 * @param importe the importe to set
	 */
	public void setImporte(Float importe) {
		this.importe = importe;
	}



	/** (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo( IdsCACVentaReserva o ) {

		if ( this.idZonasesion.equals( o.getIdZonasesion() ) ) return 0;
		else return this.idPerfilvisitante.compareTo( o.getIdPerfilvisitante() );
		
	}	


	/**
	 * 
	 * @param respuesta
	 * @return
	 * @throws NumberFormatException
	 */
	public static IdsCACVentaReserva parseFromInvokerResponse( String respuesta ) throws NumberFormatException {
		
		IdsCACVentaReserva ids =  new IdsCACVentaReserva();
		
		String rformat = StringUtils.remove( respuesta, "<IdsCACVentaReserva>" );
		rformat = StringUtils.remove( rformat, "</IdsCACVentaReserva>" );
		rformat = StringUtils.substringAfter( rformat, "<idProducto>" );
		ids.setIdProducto(   Integer.parseInt(  StringUtils.substringBefore( rformat, Constantes.MARCA_INICIO_CIERRE_ETIQUETA_XML )  )   );
		rformat = StringUtils.substringAfter( rformat, "<idPerfilvisitante>" );
		ids.setIdPerfilvisitante(   Integer.parseInt(  StringUtils.substringBefore( rformat, Constantes.MARCA_INICIO_CIERRE_ETIQUETA_XML )  )   );
		rformat = StringUtils.substringAfter( rformat, "<idZonasesion>" );
		
		if ( !rformat.equals( Constantes.CADENA_VACIA) ) { //Es producto simple. 
			ids.setIdZonasesion(   Integer.parseInt(  StringUtils.substringBefore( rformat, Constantes.MARCA_INICIO_CIERRE_ETIQUETA_XML )  )   );
			rformat = StringUtils.substringAfter( rformat, "<idZona>" );
			ids.setIdZona(   Integer.parseInt(  StringUtils.substringBefore( rformat, Constantes.MARCA_INICIO_CIERRE_ETIQUETA_XML )  )   );
			rformat = StringUtils.substringAfter( rformat, "<idSesion>" );
			ids.setIdSesion(   Integer.parseInt(  StringUtils.substringBefore( rformat, Constantes.MARCA_INICIO_CIERRE_ETIQUETA_XML )  )   );
			rformat = StringUtils.substringAfter( rformat, Constantes.NODO_XML_importe_APERTURA );
			ids.setImporte(   Float.parseFloat(  StringUtils.substringBefore( rformat, Constantes.MARCA_INICIO_CIERRE_ETIQUETA_XML )  )   );
		}
		else {//Es producto combinado
			ids.setIdZonasesion( null );
			ids.setIdZona( null );
			ids.setIdSesion( null );
			rformat = StringUtils.substringAfter( respuesta, Constantes.NODO_XML_importe_APERTURA );
			ids.setImporte(   Float.parseFloat(  StringUtils.substringBefore( rformat, Constantes.MARCA_INICIO_CIERRE_ETIQUETA_XML )  )   );
		}
		return ids;
	}

}
package utiles.auxiliar;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

//import es.di.cac.ticketing.services.util.CacUtils;
//import es.di.cac.ticketing.services.util.ConfiguracionUtil;
//import es.di.cac.ticketing.services.util.CorreoUtils;
//import es.di.cac.ticketing.services.util.SrvConfiguracionPublico;
import utiles.Constantes;
//import es.di.cac.ticketing.services.util.SrvConfiguracionPublico;

/**
 * @author JMH
 *
 */
public class Correo {
	private static final Log LOGGER = LogFactory.getLog(Correo.class);
	
	private String remitente;
	private String destinatario;
	private String asunto;
	private String contenido;
	//GGL 09.11.2015 Para mandar varios .pkpass adjuntos con la carta
	//private byte[] adjunto;
	private List<byte[]> adjuntos;
	//private String nombreAdjuntoConExtension;
	private List<String> nombreAdjuntosConExtension;
	private boolean contenidoHtml = false; 

	private JavaMailSenderImpl sender = null;

	
	public boolean isContenidoHtml() {
		return contenidoHtml;
	}
	public void setContenidoHtml(boolean contenidoHtml) {
		this.contenidoHtml = contenidoHtml;
	}
	
	public List<byte[]> getAdjuntos() {
		return adjuntos;
	}
	public void setAdjuntos(List<byte[]> adjuntos) {
		this.adjuntos = adjuntos;
	}
	/*
	public byte[] getAdjunto() {
		return adjunto;
	}
	public void setAdjunto(byte[] adjunto) {
		this.adjunto = adjunto;
	}
	*/
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getRemitente() {
		return remitente;
	}
	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}
	/*
	public String getNombreAdjuntoConExtension() {
		return nombreAdjuntoConExtension;
	}
	public void setNombreAdjuntoConExtension(String nombreAdjuntoConExtension) {
		this.nombreAdjuntoConExtension = nombreAdjuntoConExtension;
	}*/
	public List<String> getNombreAdjuntosConExtension() {
		return nombreAdjuntosConExtension;
	}
	public void setNombreAdjuntosConExtension(List<String> nombreAdjuntosConExtension) {
		this.nombreAdjuntosConExtension = nombreAdjuntosConExtension;
	}
	public String toString() {
		return "Remitente: "+remitente+", Destinatario: "+destinatario+", Asunto: "+asunto+", Contenido: "+contenido;//+", NombreAdjutno: "+nombreAdjuntoConExtension;
	}
	
	/**
	 * Settea los valores del servidor SMTP en el sender con los valores sacados de la tabla Configuracion de la BD
	 * 
	 * @param sender
	 * @author SBA
	 */
	private boolean createJavaMailSenderImpl() {	
		String host = Constantes.SERVIDOR_SMTP_HOST;
		boolean activado = !StringUtils.isBlank(host);
		if (activado) {
			sender = new JavaMailSenderImpl();
			sender.setDefaultEncoding( Constantes.ENCODING_UTF8 );
			Integer seguridad = Integer.valueOf(Constantes.SERVIDOR_SMTP_SEGURIDAD);
			sender.setHost(host);
			sender.setPort(Integer.valueOf(Constantes.SERVIDOR_SMTP_PORT));
			if (seguridad == 1) {
				Properties mailProps = new Properties();
				mailProps.setProperty("mail.smtp.auth", Constantes.CADENA_true);
				mailProps.setProperty("mail.smtp.starttls.enable", Constantes.CADENA_true);
				sender.setJavaMailProperties(mailProps);
				String usuario = Constantes.SERVIDOR_SMTP_USUARIO;
				sender.setUsername(usuario);
				String pass = Constantes.SERVIDOR_SMTP_PASSWORD;
				sender.setPassword(pass);
			}
		}
		return activado;
	}

	/**
	 * Envia correo, para enviar un archivo adjunto, se debe tambien poner si o si el valor
	 * 'nombreAdjuntoConExtension', sino se va a enviar solo el mensaje
	 * @param correo
	 * @throws Exception
	 * @author SBA
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public void enviarCorreo(boolean enviarconcopia) throws MessagingException, UnsupportedEncodingException 
	{
		Validate.notEmpty(this.getAsunto(), "el asunto es requerido");
		Validate.notEmpty(this.getDestinatario(), "el destinatario es requerido");
		Validate.notEmpty(this.getRemitente(), "el remitente es requerido");
		Validate.notEmpty(this.getContenido(), "el contenido es requerido");
		MimeMessageHelper helper = null;
		boolean activado = this.createJavaMailSenderImpl();
		if ( activado ) {
			try {
				/* API 13 Marzo 2012 Vamos a intentar tratar el contenido como llega sin convertir a UTF8 pues segun el logger ya llega en UTF-8 y no en ISO-8859-1
				// Creamos el mensaje
				MimeMessage message = sender.createMimeMessage();
				// Creamos el helper que nos ayuda a configurar el mensaje
				if( correo.getNombreAdjuntoConExtencion() != null ){
					helper = new MimeMessageHelper( message, true, Constantes.ENCODING_UTF8 );
					ByteArrayResource byteArrayResource = new ByteArrayResource( correo.getAdjunto() );
					helper.addAttachment(   new String(  correo.getNombreAdjuntoConExtencion().getBytes( Constantes.ENCODING_ISO_8859_1 ), Constantes.ENCODING_UTF8  ), byteArrayResource   );
				}else{
					helper = new MimeMessageHelper( message, Constantes.ENCODING_UTF8 );
				}
				helper.setSubject(   new String(  correo.getAsunto().getBytes( Constantes.ENCODING_ISO_8859_1 ), Constantes.ENCODING_UTF8  )   );
				helper.setFrom(   new String(  correo.getRemitente().getBytes( Constantes.ENCODING_ISO_8859_1 ), Constantes.ENCODING_UTF8  )   );
				helper.setTo(   new String(  correo.getDestinatario().getBytes( Constantes.ENCODING_ISO_8859_1 ), Constantes.ENCODING_UTF8  )   );
				if ( correo.isContenidoHtml() ) {
					helper.setText(   new String(  correo.getContenido().getBytes( Constantes.ENCODING_ISO_8859_1 ), Constantes.ENCODING_UTF8  ), true   );
				} else {
					helper.setText(   new String(  correo.getContenido().getBytes( Constantes.ENCODING_ISO_8859_1 ), Constantes.ENCODING_UTF8  )   );
				}*/
				MimeMessage message = sender.createMimeMessage();
				// Creamos el helper que nos ayuda a configurar el mensaje
				//if( correo.getNombreAdjuntoConExtension() != null ){
				if( /*!correo.getNombreAdjuntosConExtension().isEmpty() &&*/ this.getNombreAdjuntosConExtension()!=null){
					if (!this.getNombreAdjuntosConExtension().isEmpty()) {
						helper = new MimeMessageHelper( message, true, Constantes.ENCODING_UTF8 );
						//GGL 09.11.2015 Iterar por el array de adjuntos
						//ByteArrayResource byteArrayResource = new ByteArrayResource( correo.getAdjunto() );
						//helper.addAttachment( correo.getNombreAdjuntoConExtension(), byteArrayResource );
						/*GGL 10.11.2015 Cambio el for para llevar el mismo contador en ambos List. A la proxima revision cambiaremos a Map
						for (Iterator iter = correo.getAdjuntos().iterator();iter.hasNext();){
							byte[] adjunto = (byte[]) iter.next();
							ByteArrayResource byteArrayResource = new ByteArrayResource( adjunto );
							helper.addAttachment( correo.getNombreAdjuntoConExtension(), byteArrayResource );
						}*/
						for (int i=0;i<this.getAdjuntos().size();i++){
							byte[] adjunto = (byte[]) this.getAdjuntos().get(i);
							ByteArrayResource byteArrayResource = new ByteArrayResource( adjunto );
							helper.addAttachment( this.getNombreAdjuntosConExtension().get(i), byteArrayResource );
						}
					}
				} else{
					helper = new MimeMessageHelper( message, Constantes.ENCODING_UTF8 );
				}
				helper.setSubject( this.getAsunto() );
				helper.setFrom( this.getRemitente() );
				
				// JMH 14.10.2013: Inclusión de varios destinatarios CC para el email. Se toman las
				// direcciones de mail de la tabla CONFIGURACION
				String aDireccionemail[];
				String sDireccionemail = Constantes.INTEGRACION_COLOSSUS_EXPERTICKET_DESTINATARIO_EMAIL;
				
				int cont = 0;
				
				StringTokenizer strToken = new StringTokenizer(sDireccionemail, ",;:");
				
				aDireccionemail = new String[strToken.countTokens()];
				while (strToken.hasMoreTokens())
				{
					aDireccionemail[cont] = strToken.nextToken().trim();
					cont++;
				}
								
				helper.setCc(aDireccionemail);
				
				// JMH 14.10.2013: Inclusión de varios destinatarios CC para el email. Se toman las
				// direcciones de mail de la tabla CONFIGURACION, separadas por ",", ";" o ":".
				
				// JMH 17.10.2013: Inclusión de varios destinatarios To para el email. Se toman las
				// direcciones de mail del campo email que se haya introducido en los datos del cliente, separadas por ",", ";" o ":".				
//				helper.setTo( correo.getDestinatario() );
				cont = 0;
				sDireccionemail = this.getDestinatario();
				strToken = new StringTokenizer(sDireccionemail, ",;:");
				aDireccionemail = new String[strToken.countTokens()];
				while (strToken.hasMoreTokens())
				{
					aDireccionemail[cont] = strToken.nextToken().trim();
					cont++;
				}
								
				helper.setTo(aDireccionemail);				
				// JMH 17.10.2013: Inclusión de varios destinatarios To para el email. Se toman las
				// direcciones de mail del campo email que se haya introducido en los datos del cliente, separadas por ",", ";" o ":".				
				
				if ( this.isContenidoHtml() )
					helper.setText(this.getContenido(), true);
				else
					helper.setText( this.getContenido() );

				//message.addHeader("Content-Transfer-Encoding","8bit");
				if (LOGGER.isInfoEnabled() ){
					//API 13 Marzo 2012 Tengo dudas sobre si utilizar un Array de Bytes como base para obtener la codificación por defecto se válido para los Strings también. Por tanto meteremos otra instruccion que sea mas aclaratoria.
					String mensajeTexto = "Mensaje de prueba";
					String defaultEncodingStrings = new InputStreamReader(  new ByteArrayInputStream( mensajeTexto.getBytes() )  ).getEncoding();
					// JAN: estos logs ayudarán a analizar posibles problemas de encoding.
					String defaultEncoding = new InputStreamReader( new ByteArrayInputStream(new byte[0]) ).getEncoding();
					LOGGER.info("enviarCorreo, defaultEncodingStrings: " + defaultEncodingStrings );
					LOGGER.info("enviarCorreo, defaultEncoding: " + defaultEncoding );
					LOGGER.info("enviarCorreo, System.getProperty('file.encoding'): " + System.getProperty("file.encoding"));
					/*
					LOGGER.info("enviarCorreo, 'áéíóú'.");
					LOGGER.info("enviarCorreo, new String('áéíóú'.getBytes(),'UTF-8'): " + new String("áéíóú".getBytes(),Constantes.ENCODING_UTF8));
					LOGGER.info("enviarCorreo, new String('áéíóú'.getBytes(),'iso-8859-1'): " + new String("áéíóú".getBytes(),Constantes.ENCODING_ISO_8859_1));
					LOGGER.info("enviarCorreo, new String('áéíóú'.getBytes('ISO-8859-1'),'UTF-8'): " + new String("áéíóú".getBytes(Constantes.ENCODING_ISO_8859_1),Constantes.ENCODING_UTF8));
					LOGGER.info("enviarCorreo, correo.getAsunto(): " + correo.getAsunto());
					LOGGER.info("enviarCorreo, new String(correo.getAsunto().getBytes('ISO-8859-1'),'UTF-8'): " + new String(correo.getAsunto().getBytes(Constantes.ENCODING_ISO_8859_1),Constantes.ENCODING_UTF8));
					LOGGER.info("enviarCorreo, sender.getDefaultEncoding(): " + sender.getDefaultEncoding());
					LOGGER.info("enviarCorreo, helper.getEncoding(): " + helper.getEncoding());
					LOGGER.info("enviarCorreo, message.getEncoding(): " + message.getEncoding());
					*/
					LOGGER.info( "enviarCorreo, " + Constantes.CADENA_VOCALES );
					LOGGER.info(  "enviarCorreo, new String('" + Constantes.CADENA_VOCALES + "'.getBytes(),'UTF-8'): " + new String( Constantes.CADENA_VOCALES.getBytes(), Constantes.ENCODING_UTF8 )  );
					LOGGER.info(  "enviarCorreo, new String('" + Constantes.CADENA_VOCALES + ".getBytes(),'iso-8859-1'): " + new String( Constantes.CADENA_VOCALES.getBytes(), Constantes.ENCODING_ISO_8859_1 )  );
					LOGGER.info(  "enviarCorreo, new String('" + Constantes.CADENA_VOCALES + "'.getBytes('ISO-8859-1'),'UTF-8'): " + new String(  Constantes.CADENA_VOCALES.getBytes( Constantes.ENCODING_ISO_8859_1 ), Constantes.ENCODING_UTF8  )   );
					LOGGER.info(  "enviarCorreo, correo.getAsunto(): " + this.getAsunto());
					LOGGER.info(  "enviarCorreo, new String(correo.getAsunto().getBytes('ISO-8859-1'),'UTF-8'): " + new String(this.getAsunto().getBytes( Constantes.ENCODING_ISO_8859_1 ), Constantes.ENCODING_UTF8 )  );
					LOGGER.info(  "enviarCorreo, sender.getDefaultEncoding(): " + sender.getDefaultEncoding()  );
					LOGGER.info(  "enviarCorreo, helper.getEncoding(): " + helper.getEncoding()  );
					LOGGER.info(  "enviarCorreo, message.getEncoding(): " + message.getEncoding()  );
				}
				sender.send( message );
			} catch (MessagingException e) {
				LOGGER.error(" ## Error al enviar correo. Mensaje \n " + this.toString(), e);
				throw e;
			} catch (MailException me){
				LOGGER.error(" ## Error al enviar correo. Mensaje \n " + this.toString(), me);
				throw me;
			} catch (UnsupportedEncodingException uee){
				LOGGER.error(" ## Error de encoding al enviar correo. Mensaje \n " + this.toString(), uee);
				throw uee;
			}
		} else {
			if (LOGGER.isWarnEnabled())
				LOGGER.warn("El envio de correos no está activado. Rellene el parametro HOST de la configuracion");
		}
	}
}
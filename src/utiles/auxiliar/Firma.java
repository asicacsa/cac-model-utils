package utiles.auxiliar;

import java.security.cert.Certificate;
import java.security.Key;

/**
 * 
 * La clase {@link Firma} representa una clave privada y su correspondiente certificado, extraidos ambos de un fichero que contiene una firma digital, tanto clave pública, como privada.
 * @author API
 * @since 2 Abril 2012
 *
 */
public class Firma {

	private Key key;
	private Certificate certificado;


	public Firma() {}


	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}

	public Certificate getCertificado() {
		return certificado;
	}

	public void setCertificado(Certificate certificado) {
		this.certificado = certificado;
	}

}
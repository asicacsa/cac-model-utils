package utiles;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;

import com.google.common.base.Charsets;

/**
 * Esta clase gestiona operaciones frecuentemente utilizadas de entrada/salida<br>
 * Esta clase NO requiere instanciación, pues es un compendio de metodos estáticos.
 * @author API
 * @since 2 Abril 2012
 *
 */
public class IOOperations {

	/**
	 * @author API
	 * @since 20 Octubre 2011
	 * @param ruta La ruta debe ser una ruta UNC bien terminada con el caracter "\" en el caso de ser una maquina windows la de destino. Ejemplo "\\Servidor\Recurso_compartido\"<br>Si el host destino del fichero es una maquina UNIX/LINUX la ruta debe ser del tipo "/tmp/ICAA/".
	 * @param nomFichero Nombre del fichero que queremos cargar. 
	 * @return
	 */
	public static String cargaDeDiscoContenidoFichero( String ruta, String nomFichero ) throws IOException, FileNotFoundException, SecurityException {
		Validate.notNull( ruta, "La ubicación del fichero debe facilitarse para poderlo cargar.");
		Validate.notNull( nomFichero, "El nombre del fichero debe facilitarse necesariamente para poderlo cargar del disco." );
		String fichero = ruta + nomFichero;
		File fich = new File( fichero );
		if ( ! fich.canRead() )
			throw new IOException("No se puede cargar el fichero de la ruta " + fichero + " . Compruebe que el sistema de ficheros contiene un fichero en la ruta indicada y que Colossus tiene permisos de lectura sobre el mismo.\n");
		FileReader freader = new FileReader( fich );
		int tamFichero = new Long( fich.length() ).intValue();
		char[] buffer = new char[tamFichero];
		freader.read(buffer, Constantes.CERO, tamFichero);
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i<tamFichero; i++)
			sb.append( buffer[i] );
		return sb.toString();

		// JMARIN (15.06.2018): Error en la codificación del fichero
//		Validate.notNull( ruta, "La ubicación del fichero debe facilitarse para poderlo cargar.");
//		Validate.notNull( nomFichero, "El nombre del fichero debe facilitarse necesariamente para poderlo cargar del disco." );
//		String fichero = ruta + nomFichero;
//		File fich = new File( fichero );
//		if ( ! fich.canRead() )
//			throw new IOException("No se puede cargar el fichero de la ruta " + fichero + " . Compruebe que el sistema de ficheros contiene un fichero en la ruta indicada y que Colossus tiene permisos de lectura sobre el mismo.\n");
//		InputStream is = new FileInputStream(fich);
//		
//		byte[] buffer = IOUtils.toByteArray(is);
//		return buffer;
		// JMARIN (15.06.2018): Error en la codificación del fichero
	}



	/**
	 * @author API
	 * @since 1 Septiembre 2011
	 * @param ruta La ruta debe ser una ruta UNC bien terminada con el caracter "\" en el caso de ser una maquina windows la de destino. Ejemplo "\\Servidor\Recurso_compartido\"<br>Si el host destino del fichero es una maquina UNIX/LINUX la ruta debe ser del tipo "/tmp/ICAA/".
	 * @param nomFichero Nombre del fichero en el que se desea almacenar el contenido facilitado como texto.
	 * @param contenido Contenido facilitado como texto que se desea almacenar en disco, en el fichero especificado por los parametros ruta y nomFichero.
	 * @return
	 */
	public static void escribeEnDisco( String ruta, String nomFichero, String contenido ) throws Exception {
		Validate.notNull( ruta, "La ubicación del fichero debe facilitarse necesariamente para poder ser guardado en disco.");
		Validate.notNull( nomFichero, "El nombre del fichero debe facilitarse necesariamente para poder ser guardado en disco." );
		File directorio = new File( ruta );
		if ( !directorio.isDirectory() ) //Si no existe el directorio lo creamos.
			directorio.mkdir();
		String fichero = ruta + nomFichero;
		File fich = new File( fichero );
		if ( !fich.createNewFile() )
			throw new Exception("No se puede crear un fichero en la ruta " + fichero + " . Compruebe que el sistema de ficheros no contiene un fichero en la ruta indicada. Si existe el fichero, el usuario con que se lanza Colossus debe tener permisos de modificacion sobre el mismo.\n");
		FileWriter fwriter = new FileWriter( fich );
		fwriter.write( contenido );
		fwriter.flush();
		fwriter.close();
	}

	/**
	 * @author API
	 * @since 24 Enero 2012
	 * @param ruta La ruta debe ser una ruta UNC bien terminada con el caracter "\" en el caso de ser una maquina windows la de destino. Ejemplo "\\Servidor\Recurso_compartido\"<br>Si el host destino del fichero es una maquina UNIX/LINUX la ruta debe ser del tipo "/tmp/ICAA/".
	 * @param nomFichero Nombre del fichero que queremos cargar. 
	 * @return
	 */
	public static byte[] cargaDeDiscoContenidoFicheroBinario( String ruta, String nomFichero ) throws IOException {
		Validate.notNull( ruta, "La ubicaion del fichero debe facilitarse para poderlo cargar.");
		Validate.notNull( nomFichero, "El nombre del fichero debe facilitarse necesariamente para poderlo cargar del disco." );
		String fichero = ruta + nomFichero;
		File fich = new File( fichero );
		if ( ! fich.canRead() )
			throw new IOException( "No se puede cargar el fichero de la ruta |" + fichero + "| . Compruebe que el sistema de ficheros contiene un fichero en la ruta indicada y que Colossus tiene permisos de lectura sobre el mismo.\n" );
		FileInputStream fis = new FileInputStream( fich );
		DataInputStream dis = new DataInputStream( fis ); 
		int tamFichero = new Long( fich.length() ).intValue();
		byte[] buffer = new byte[tamFichero];
		for(int i = 0; i<tamFichero; i++)
			buffer[i] = dis.readByte();
		fis.close();
		dis.close();
		return buffer;
	}



	/**
	 * Lee el fichero especificado de disco, lo pasa a un array de bytes según la codificacion facilitada en la tabla de configuracion, lo decodifica pasandolo a un buffer de caracteres interpretados de acuerdo a la misma codificacion establecida. Termina generando una copia del fichero en la codificación solicitada, se mantiene el fichero original.
	 * @author API
	 * @since 22 Diciembre 2011
	 * @param ruta
	 * @param nombreFicheroOrigen
	 * @param epilogoFicheroRecodificado
	 * @param charsetDestino
	 * @return
	 * @throws Exception
	 */
	public static File recodificaFichero( String ruta, String nombreFicheroOrigen, String epilogoFicheroRecodificado, String charsetDestino ) throws CharacterCodingException, IOException {
		File directorio = new File( ruta );
		if ( !directorio.isDirectory() ) //Si no existe el directorio lo creamos.
			directorio.mkdir();
		String[] partes = StringUtils.split( nombreFicheroOrigen, Constantes.PUNTO );
		int numPartes = partes.length;
		StringBuilder sb = new StringBuilder( partes[0] );
		sb.append( epilogoFicheroRecodificado );
		for( int i=1; i < numPartes; i++ ) {
			sb.append( Constantes.PUNTO );
			sb.append( partes[i] );
		}
		String pathFicheroRecodificado = ruta + sb.toString();
		File ficheroRecodificado = new File( pathFicheroRecodificado );
		if ( !ficheroRecodificado.createNewFile() )
			throw new IOException("No se puede crear un fichero en la ruta |" + pathFicheroRecodificado + "| . Compruebe que el sistema de ficheros no contiene un fichero en la ruta indicada. Si existe el fichero, el usuario con que se lanza Colossus debe tener permisos de modificacion sobre el mismo.\n");
		
		Charset charsetdestino = Charset.forName( charsetDestino );

		/**
		 * JMH (04.12.2019)
		 * Al escribir el fichero recodificado a Windows Cp-1252 añade un espacio en blanco al fina de la última línea.
		 * Modifico el método para corregirlo:
		 * 1.- Leo el fichero original línea a línea y las meto en una List de String
		 * 2.- Recorro la lista generada excepto la última línea, y añado un CR para cada línea
		 * 3.- Añado la última línea sin añadir CR al final de línea 
		 */
		////OutputStream fos = new FileOutputStream( pathFicheroRecodificado );
		////OutputStream bos = new BufferedOutputStream( fos );
		////OutputStreamWriter osw = new OutputStreamWriter( bos, charsetdestino );
		
		// JMH (18.06.2018)
		//osw.write(  IOOperations.cargaDeDiscoContenidoFichero( ruta, nombreFicheroOrigen )  );
		////String cadena = new String(IOOperations.cargaDeDiscoContenidoFichero( ruta, nombreFicheroOrigen ));
		
		//cadena = cadena.replaceAll("\\n", "\r\n");
		////osw.write(cadena);
		// JMH (18.06.2018)
		
		List<String> listLineasFichero = new ArrayList<String>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(ruta+nombreFicheroOrigen), "UTF8"));
			String cadena  = reader. readLine();
			while (cadena != null) {
				listLineasFichero.add(cadena);
				cadena = reader.readLine();
			}
		} catch (FileNotFoundException fnfe) {
				throw new FileNotFoundException(fnfe.getLocalizedMessage() + ". No se puede cargar el fichero de la ruta " + ruta+nombreFicheroOrigen + " . Compruebe que el sistema de ficheros contiene un fichero en la ruta indicada y que Colossus tiene permisos de lectura sobre el mismo.\n");
		} catch (SecurityException se) {
				throw new SecurityException(se.getLocalizedMessage() + ". No se puede cargar el fichero de la ruta " + ruta+nombreFicheroOrigen + " . Compruebe que el sistema de ficheros contiene un fichero en la ruta indicada y que Colossus tiene permisos de lectura sobre el mismo.\n");
		} catch (UnsupportedEncodingException uee) {
				throw new SecurityException(uee.getLocalizedMessage() + ". No se puede cargar el fichero de la ruta " + ruta+nombreFicheroOrigen + " . Compruebe que el sistema de ficheros contiene un fichero en la ruta indicada y que Colossus tiene permisos de lectura sobre el mismo.\n");
		} finally {
			reader.close();			
		}

		BufferedWriter  writer = null;
		
		try {
			 writer   = new BufferedWriter(new OutputStreamWriter(new FileOutputStream( ficheroRecodificado), charsetdestino));

			int i ;
			for ( i = 0 ; i < listLineasFichero.size() - 1 ; i++) {
				writer.write(listLineasFichero.get(i));
				writer.newLine();
			}
			writer.write(listLineasFichero.get(i));
		} catch (FileNotFoundException fnfe) {
				throw new FileNotFoundException(fnfe.getLocalizedMessage() + ". No se puede cargar el fichero de la ruta " + ruta+nombreFicheroOrigen + " . Compruebe que el sistema de ficheros contiene un fichero en la ruta indicada y que Colossus tiene permisos de lectura sobre el mismo.\n");
		} catch (SecurityException se) {
				throw new SecurityException(se.getLocalizedMessage() + ". No se puede cargar el fichero de la ruta " + ruta+nombreFicheroOrigen + " . Compruebe que el sistema de ficheros contiene un fichero en la ruta indicada y que Colossus tiene permisos de lectura sobre el mismo.\n");
		} catch (IOException ioe) {
				throw new SecurityException(ioe.getLocalizedMessage() + ". No se puede cargar el fichero de la ruta " + ruta+nombreFicheroOrigen + " . Compruebe que el sistema de ficheros contiene un fichero en la ruta indicada y que Colossus tiene permisos de lectura sobre el mismo.\n");
		} finally {	
			writer.flush();
			writer.close();
		}		
		
		////osw.flush();
		////osw.close();
		/**
		 * JMH (04.12.2019)
		 */
		return ficheroRecodificado;
		
	}

}

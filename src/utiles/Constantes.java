package utiles;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import es.di.framework.acegisecurity.utils.UserSessionCache;

/**
 * @author Angel Perez Izquierdo
 * @since 7 Enero 2008
 */
public final class Constantes {
	
	/**Constante entera que representa el objeto {@link Integer} con valor -1.<br><b>-1</b>*/
	public static final Integer MENOSUNO = -1;
	/**Constante entera que representa el objeto {@link Integer} con valor 0.<br><b>0</b><br>*/
	public static final Integer CERO = 0;
	/**Constante entera que representa el objeto {@link Integer} con valor 1.<br><b>1</b><br>*/
	public static final Integer UNO = 1;
	/**Constante entera que representa el objeto {@link Integer} con valor 2.<br><b>2</b><br>*/
	public static final Integer DOS = 2;
	/**Constante entera que representa el objeto {@link Integer} con valor 3.<br><b>3</b><br>*/
	public static final Integer TRES = 3;
	/**Constante entera que representa el objeto {@link Integer} con valor 4.<br><b>4</b><br>*/
	public static final Integer CUATRO = 4;
	/**Constante entera que representa el objeto {@link Integer} con valor 5.<br><b>5</b><br>*/
	public static final Integer CINCO = 5;
	/**Constante entera que representa el objeto {@link Integer} con valor 6.<br><b>6</b><br>*/
	public static final Integer SEIS = 6;
	/**Constante entera que representa el objeto {@link Integer} con valor 7.<br><b>7</b><br>*/
	public static final Integer SIETE = 7;
	/**Constante entera que representa el objeto {@link Integer} con valor 8.<br><b>8</b><br>*/
	public static final Integer OCHO = 8;
	/**Constante entera que representa el objeto {@link Integer} con valor 9.<br><b>9</b><br>*/
	public static final Integer NUEVE = 9;
	/**Constante entera que representa el objeto {@link Integer} con valor 10.<br><b>10</b><br>*/
	public static final Integer DIEZ = 10;
	/**Constante entera que representa el objeto {@link Integer} con valor 11.<br><b>11</b><br>*/
	public static final Integer ONCE = 11;
	/**Constante entera que representa el objeto {@link Integer} con valor 12.<br><b>12</b><br>*/
	public static final Integer DOCE = 12;
	/**Constante entera que representa el objeto {@link Integer} con valor 13.<br><b>13</b><br>*/
	public static final Integer TRECE = 13;
	/**Constante entera que representa el objeto {@link Integer} con valor 14.<br><b>14</b><br>*/
	public static final Integer CATORCE = 14;
	/**Constante entera que representa el objeto {@link Integer} con valor 15.<br><b>15</b><br>*/
	public static final Integer QUINCE = 15;
	/**Constante entera que representa el objeto {@link Integer} con valor 16.<br><b>16</b><br>*/
	public static final Integer DIECISEIS = 16;
	/**Constante entera que representa el objeto {@link Integer} con valor 17.<br><b>17</b><br>*/
	public static final Integer DICECISIETE = 17;
	/**Constante entera que representa el objeto {@link Integer} con valor 18.<br><b>18</b><br>*/
	public static final Integer DICECIOCHO = 18;
	/**Constante entera que representa el objeto {@link Integer} con valor 19.<br><b>19</b><br>*/
	public static final Integer DICECINUEVE = 19;

	/**Constante entera que representa el objeto {@link Integer} con valor 21.<br><b>21</b><br>*/
	public static final Integer VEINTIUNO = 21;
	/**Constante entera que representa el objeto {@link Integer} con valor 22.<br><b>22</b><br>*/
	public static final Integer VEINTIDOS = 22;
	/**Constante entera que representa el objeto {@link Integer} con valor 23.<br><b>23</b><br>*/
	public static final Integer VEINTITRES = 23;
	/**Constante entera que representa el objeto {@link Integer} con valor 24.<br><b>24</b><br>*/
	public static final Integer VEINTICUATRO = 24;
	/**Constante entera que representa el objeto {@link Integer} con valor 25.<br><b>25</b><br>*/
	public static final Integer VEINTICINCO = 25;
	/**Constante entera que representa el objeto {@link Integer} con valor 26.<br><b>26</b><br>*/
	public static final Integer VEINTISEIS = 26;
	/**Constante entera que representa el objeto {@link Integer} con valor 27.<br><b>27</b><br>*/
	public static final Integer VEINTISIETE = 27;
	/**Constante entera que representa el objeto {@link Integer} con valor 28.<br><b>28</b><br>*/
	public static final Integer VEINTIOCHO = 28;
	/**Constante entera que representa el objeto {@link Integer} con valor 29.<br><b>29</b><br>*/
	public static final Integer VEINTINUEVE = 29;

	
	/**Constante entera que representa el objeto {@link Integer} con valor 31.<br><b>31</b><br>*/
	public static final Integer TREINTAYUNO = 31;
	/**Constante entera que representa el objeto {@link Integer} con valor 59.<br><b>59</b><br>*/
	public static final Integer CINCUENTAYNUEVE = 59;
	/**Dias naturales que comprenden la semana en el calendario Gregoriano<br><b>7</b>*/
	public static final Integer DIAS_POR_SEMANA = SIETE;
	/**Número máximo de semanas que pueden pertenecer a un año según el calendario definido por el ICAA<br><b>54</b>*/
	public static final Integer NUM_MAX_SEMANAS_ANUALES_ICAA = 54;
	/**<br><b>23</b>*/
	public static final Integer ULTIMA_HORA_DIA_24H = VEINTITRES;
	/**<br><b>59</b>*/
	public static final Integer ULTIMO_MINUTO_HORA = CINCUENTAYNUEVE;
	/**<br><b>59</b>*/
	public static final Integer ULTIMO_SEGUNDO_MINUTO = CINCUENTAYNUEVE;
	/**<br><b>0</b>*/
	public static final Integer PRIMERA_HORA_DIA_24H = CERO;
	/**<br><b>0</b>*/
	public static final Integer PRIMER_MINUTO_HORA = CERO;
	/**<br><b>0</b>*/
	public static final Integer PRIMER_SEGUNDO_MINUTO = CERO;
	/**Constante entera que representa el número de posiciones decimales a utilizar cuando se captura una ArithmeticException al utilizar el método divide entre 2 objetos {@link BigDecimal} y tengamos que truncar el resultado a un número arbitrario de posiciones decimales que va a ser 32.<br><b>32</b><br>*/
	public static final Integer PRECISION_REDONDEO_POR_TRUNCAMIENTO_EN_DIVISION_BigDecimal = 32;
	/**Puerto en el que se va a estar escuchando en el servidor<br>Inicialmente 1038 elegido por el Rafael Santiesteban.*/
	public static final Integer PUERTOSERVIDOR = 1038; 
	/**Identificador de actividad utilizada para los clientes del Club Ciutat de forma predeterminada.<br><b>new Integer(701)</b>*/
	public static final Integer ID_ACTIVIDAD_CLIENTES_CLUB = 701;
	/**Identificador de actividad utilizada para los clientes del Club Ciutat de forma predeterminada.<br><b>new Integer(607)</b>*/
	public static final Integer SEISCIENTOSSIETE = 607;
	/**Constante entera que representa el numero de posiciones decimales a utilizar en importes no finales a pagar por el cliente valor por defecto es 3.<br><b>3</b><br>*/
	public static final Integer POSICIONES_DECIMALES_INTERMEDIAS = TRES;
	/**Constante entera que representa el numero de posiciones decimales a utilizar internamente para redondear en un {@link MathContext} con una precisión de 0, premultiplicando por 10^digitos deseados a obtener en la representacion final, tomaremos el numero maximo de decimales representables en un float que son 7.<br><b>7</b><br>*/
	public static final Integer UMBRAL_POSICIONES_DECIMALES_FLOAT = SIETE;
	/**Campo que indica que la nomenclatura del fichero a enviar al ICAA acaba con el número de la semana a la que van referidos los datos del informe, dentro del año actual.<br><b>1</b>*/
	public static final Integer POR_SEMANA_ICAA = UNO;
	/**Campo que indica que la nomenclatura del fichero a enviar al ICAA acaba con el número Juliano del primer dia de la semana a la que van referidos los datos del informe, dentro del año actual.<br><b>1</b>*/
	public static final Integer POR_DIA_ICAA = CERO;
	/**Constante que representa si la salida formateada se ha de interpretar como si fuera numérica, a la hora de paddear o extender, independientemente del tipo de objeto que representa su valor.<br>Seleccionar esta constante implica que el campo se padeará con 0's por la izquierda hasta completar la longitud del campo.<br>Se usa en la generación de Informes Semanales para el ICAA.<br>Internamente se representa con el entero <b>0</b>*/
	public static final Integer SALIDA_NUMERICA_CAMPOSFICHERO_ICAA = CERO;
	/**Constante que representa si la salida formateada se ha de interpretar como si fuera texto, a la hora de paddear o extender, independientemente del tipo de objeto que representa su valor.<br>Seleccionar esta constante implica que el campo se padeará con ' ' por la derecha hasta completar la longitud del campo.<br>Se usa en la generación de Informes Semanales para el ICAA.<br>Internamente se representa con el entero <b>1</b>*/
	public static final Integer SALIDA_TEXTUAL_CAMPOSFICHERO_ICAA = UNO;

	/**Constante entera que representa el Numero Maximo de Usos de un Pase Club por recinto y periodo de vigencia.<br><b>10000</b><br>*/
	public static final Integer USOS_MAXIMOS_PASE_CLUB = 10000;
	/**Identificador por defecto para los Modelos de Pase Club.<br><b>1</b>*/
	public static final Integer ID_MODELO_PASE_CLUB_DEFECTO = UNO;
	/**Entero que representa al identificador de la base de datos de Producción correspondiente al Producto O+M+H<br><b>484</b>*/
	public static final Integer ID_PRODUCTO_OMH = 484;
	/**Entero que representa al identificador de la base de datos de Producción correspondiente al Producto Hemisfèric<br><b>444</b>*/
	public static final Integer ID_PRODUCTO_HEMISFERIC = 444;
	/**Entero que representa al identificador de la base de datos de Producción correspondiente al Producto Oceanogràfic<br><b>447</b>*/
	public static final Integer ID_PRODUCTO_OCEANOGRAFIC = 447;
	/**Entero que representa al identificador de la base de datos de Producción correspondiente al Producto Museo<br><b>441</b>*/
	public static final Integer ID_PRODUCTO_MUSEO = 441;
	/**Entero que representa al identificador de la base de datos de Producción correspondiente al Producto Museo<br><b>441</b>*/
	public static final Integer ID_PRODUCTO_MH = 450;
	
	/*GGL 26.10.2016 CLUB CIUTAT*/
	/**Entero que representa al identificador de la base de datos de Producción correspondiente al Producto O+M+H<br><b>484</b>*/
	public static final Integer ID_PRODUCTO_CLUB_OMH = 1914;
	/**Entero que representa al identificador de la base de datos de Producción correspondiente al Producto Hemisfèric<br><b>444</b>*/
	//public static final Integer ID_PRODUCTO_HEMISFERIC = 444;
	/**Entero que representa al identificador de la base de datos de Producción correspondiente al Producto Oceanogràfic<br><b>447</b>*/
	public static final Integer ID_PRODUCTO_CLUB_OCEANOGRAFIC = 1911;
	/**Entero que representa al identificador de la base de datos de Producción correspondiente al Producto Museo<br><b>441</b>*/
	//public static final Integer ID_PRODUCTO_MUSEO = 441;
	/**Entero que representa al identificador de la base de datos de Producción correspondiente al Producto Museo<br><b>441</b>*/
	public static final Integer ID_PRODUCTO_CLUB_MH = 4043936;
	

	/**cadena de texto que representa a la codificación de caracteres UTF-8.<br><b>"UTF-8"</b>*/
	public static final String ENCODING_UTF8 = "UTF-8";
	/**cadena de texto que representa a la codificación de caracteres ISO-8859-1.<br><b>"ISO-8859-1"</b>*/
	public static final String ENCODING_ISO_8859_1 = "ISO-8859-1";
	/**cadena de texto que representa el juego de caracteres a utilizar en el fichero de destino para el informe a enviar al ICAA.<br><b>CHARSET_DESTINO_INFORME_ICAA</b>*/
	public static final String CHARSET_DESTINO_INFORME_ICAA = "CHARSET_DESTINO_INFORME_ICAA";
	/**Tipo de contenido para las respuestas de Controllers y servlets en el que se indica que contiene la respuesta que generan.<br><b>"text/xml"</b>*/
	public static final String CONTENT_TYPE_TEXT_XML = "text/xml";

	/**Función de resumen basada en el algoritmo SHA-1 que es no invertible utilizada para calcular "hashes" y verificar autenticidad de mensajes o documentos.<hr><b>"SHA-1"</b>*/
	public static final String ALGORITMO_RESUMEN_SHA_1 = "SHA-1";
	/**Cadena de texto mediante la cual identificaremos el Algoritmo de cifrado simétrico AES_128 de entre los soportados por la implementacion de Open PGP de las librerías de The Legion of Bouncy Castle v1.47.<hr><b>"AES_128"</b>*/
	public static final String ALGORITMO_AES_128 = "AES_128";
	/**Cadena de texto mediante la cual identificaremos el Algoritmo de cifrado simétrico AES_192 de entre los soportados por la implementacion de Open PGP de las librerías de The Legion of Bouncy Castle v1.47.<hr><b>"AES_192"</b>*/
	public static final String ALGORITMO_AES_192 = "AES_192";
	/**Cadena de texto mediante la cual identificaremos el Algoritmo de cifrado simétrico AES_256 de entre los soportados por la implementacion de Open PGP de las librerías de The Legion of Bouncy Castle v1.47.<hr><b>"AES_256"</b>*/
	public static final String ALGORITMO_AES_256 = "AES_256";
	/**Cadena de texto mediante la cual identificaremos el Algoritmo de cifrado simétrico BLOWFISH de entre los soportados por la implementacion de Open PGP de las librerías de The Legion of Bouncy Castle v1.47.<hr><b>"BLOWFISH"</b>*/
	public static final String ALGORITMO_BLOWFISH = "BLOWFISH";
	/**Cadena de texto mediante la cual identificaremos el Algoritmo de cifrado simétrico CAST5 de entre los soportados por la implementacion de Open PGP de las librerías de The Legion of Bouncy Castle v1.47.<hr><b>"CAST5"</b>*/
	public static final String ALGORITMO_CAST5 = "CAST5";
	/**Cadena de texto mediante la cual identificaremos el Algoritmo de cifrado simétrico DES de entre los soportados por la implementacion de Open PGP de las librerías de The Legion of Bouncy Castle v1.47.<hr><b>"DES"</b>*/
	public static final String ALGORITMO_DES = "DES";
	/**Cadena de texto mediante la cual identificaremos el Algoritmo de cifrado simétrico IDEA de entre los soportados por la implementacion de Open PGP de las librerías de The Legion of Bouncy Castle v1.47.<hr><b>"IDEA"</b>*/
	public static final String ALGORITMO_IDEA = "IDEA";
	/**Cadena de texto mediante la cual identificaremos el Algoritmo de cifrado simétrico SAFER de entre los soportados por la implementacion de Open PGP de las librerías de The Legion of Bouncy Castle v1.47.<hr><b>"SAFER"</b>*/
	public static final String ALGORITMO_SAFER = "SAFER";
	/**Cadena de texto mediante la cual identificaremos el Algoritmo de cifrado simétrico TRIPLE_DES de entre los soportados por la implementacion de Open PGP de las librerías de The Legion of Bouncy Castle v1.47.<hr><b>"TRIPLE_DES"</b>*/
	public static final String ALGORITMO_TRIPLE_DES = "TRIPLE_DES";
	/**Cadena de texto mediante la cual identificaremos el Algoritmo de cifrado simétrico TWOFISH de entre los soportados por la implementacion de Open PGP de las librerías de The Legion of Bouncy Castle v1.47.<hr><b>"TWOFISH"</b>*/
	public static final String ALGORITMO_TWOFISH = "TWOFISH";

	/**Caracter que representa al subguión.<br><b>'_'</b>*/
	public static final char SUBGUION = '_';
	/**Caracter que representa al espacio en blanco.<br><b>' '</b>*/
	public static final char CARACTER_BLANCO = ' ';
	/**Caracter que representa al caracter punto.<br><b>'.'</b>*/
	public static final char CARACTER_PUNTO = '.';
	/**Caracter que representa al caracter coma.<br><b>','</b>*/
	public static final char CARACTER_COMA = ',';
	/**Caracter que representa al caracter abre corchete.<br><b>'['</b>*/
	public static final char CARACTER_APERTURA_CORCHETE = '[';
	/**Caracter que representa al caracter cerrar corchete.<br><b>']'</b>*/
	public static final char CARACTER_CIERRE_CORCHETE = ']';
	/**Caracter que representa al caracter abrir paréntesis.<br><b>'('</b>*/
	public static final char CARACTER_APERTURA_PARENTESIS = '(';
	/**Caracter que representa al caracter cierre paréntesis.<br><b>')'</b>*/
	public static final char CARACTER_CIERRE_PARENTESIS = ')';
	/**Caracter que representa al caracter signo menor.<br><b>'<'</b>*/
	public static final char CARACTER_MENOR = '<';
	/**Caracter que representa al caracter signo mayor.<br><b>'>'</b>*/
	public static final char CARACTER_MAYOR = '>';
	/**Caracter que representa al caracter signo menos.<br><b>'-'</b>*/
	public static final char CARACTER_MENOS = '-';
	/**Caracter que representa al caracter barra.<br><b>'/'</b>*/
	public static final char CARACTER_BARRA = '/';
	/**Caracter que representa al caracter tabulador.<br><b>'\t'</b>*/
	public static final char CARACTER_TABULADOR = '\t';

	/**Cadena de texto que representa el valor null<br><b>null</b>*/
	public static final String CADENA_null = "null";
	/**Cadena de texto que representa el valor NULL<br><b>NULL</b>*/
	public static final String CADENA_NULL = "NULL";
	/**Cadena de texto que representa el valor falso asociado al tipo básico boolean de java<br><b>false</b>*/
	public static final String CADENA_false = "false";
	/**Cadena de texto que representa el valor cierto asociado al tipo básico boolean de java<br><b>true</b>*/
	public static final String CADENA_true = "true";
	/**Cadena de texto que representa la letra A.<br><b>A</b>*/
	public static final String CADENA_A = "A";
	/**Cadena de texto que representa la letra B.<br><b>B</b>*/
	public static final String CADENA_B = "B";;
	/**Cadena de texto que representa la letra C.<br><b>C</b>*/
	public static final String CADENA_C = "C";
	/**Cadena de texto que representa la letra D.<br><b>D</b>*/
	public static final String CADENA_D = "D";
	/**Cadena de texto que representa la letra E.<br><b>E</b>*/
	public static final String CADENA_E = "E";
	/**Cadena de texto que representa la letra F.<br><b>F</b>*/
	public static final String CADENA_F = "F";
	/**Cadena de texto que representa la letra G.<br><b>G</b>*/
	public static final String CADENA_G = "G";
	/**Cadena de texto que representa la letra H.<br><b>H</b>*/
	public static final String CADENA_H = "H";
	/**Cadena de texto que representa la letra I.<br><b>I</b>*/
	public static final String CADENA_I = "I";
	/**Cadena de texto que representa la letra J.<br><b>J</b>*/
	public static final String CADENA_J = "J";
	/**Cadena de texto que representa el valor negativo general.<br><b>"N"</b>*/
	public static final String CADENA_N = "N";
	/**Cadena de texto que representa la letra P.<br><b>"P"</b>*/
	public static final String CADENA_P = "P";
	/**Cadena de texto que representa la letra R.<br><b>"R"</b>*/
	public static final String CADENA_R = "R";
	/**Cadena de texto que representa el valor afirmativo general.<br><b>"S"</b>*/
	public static final String CADENA_S = "S";
	/**Cadena de texto que representa la letra T.<br><b>"T"</b>*/
	public static final String CADENA_T = "T";
	/**Cadena de texto que representa la letra M.<br><b>"M"</b>*/
	public static final String CADENA_M = "M";
	/**Cadena de texto que representa la letra V.<br><b>"V"</b>*/
	public static final String CADENA_V = "V";
	/**Cadena de texto que representa la letra X.<br><b>"X"</b>*/
	public static final String CADENA_X = "X";

	/**Cadena de texto que representa la letra a.<br><b>a</b>*/
	public static final String CADENA_a = "a";
	/**Cadena de texto que representa la letra b.<br><b>b</b>*/
	public static final String CADENA_b = "b";
	/**Cadena de texto que representa la letra c.<br><b>c</b>*/
	public static final String CADENA_c = "c";
	/**Cadena de texto que representa la letra e.<br><b>e</b>*/
	public static final String CADENA_e = "e";
	/**Cadena de texto que representa la letra f.<br><b>f</b>*/
	public static final String CADENA_f = "f";
	/**Cadena de texto que representa la letra g.<br><b>g</b>*/
	public static final String CADENA_g = "g";
	/**Cadena de texto que representa la letra m.<br><b>"m"</b>*/
	public static final String CADENA_m = "m";
	/**Cadena de texto que representa la letra p.<br><b>p</b>*/
	public static final String CADENA_p = "p";
	/**Cadena de texto que representa la letra r.<br><b>r</b>*/
	public static final String CADENA_r = "r";
	/**Cadena de texto que representa la letra s.<br><b>s</b>*/
	public static final String CADENA_s = "s";
	/**Cadena de texto que representa la letra t.<br><b>t</b>*/
	public static final String CADENA_t = "t";
	/**Cadena de texto que representa la letra v.<br><b>v</b>*/
	public static final String CADENA_v = "v";
	/**Cadena de texto que representa la letra z.<br><b>z</b>*/
	public static final String CADENA_z = "z";

	/**Cadena de texto que representa las letras es.<br><b>es</b>*/
	public static final String CADENA_es = "es";
	/**Cadena de texto que representa las letras UF.<br><b>UF</b>*/
	public static final String CADENA_UF = "UF";
	/**Cadena de texto que representa las letras EV.<br><b>EV</b>*/
	public static final String CADENA_EV = "EV";
	/**Cadena de texto que representa las letras EV.<br><b>EV</b>*/
	public static final String CADENA_IN = "IN";
	/**Cadena de texto que representa las letras IN.<br><b>IN</b>*/
	public static final String CADENA_FI = "FI";
	/**Cadena de texto que representa las letras MN.<br><b>MN</b>*/
	public static final String CADENA_MN = "MN";
	/**Cadena de texto que representa las letras NN.<br><b>NN</b>*/
	public static final String CADENA_NN = "NN";
	/**Cadena de texto que representa las letras NUM.<br><b>NUM</b>*/
	public static final String CADENA_NUM = "NUM";
	/**Cadena de texto que representa los caracteres R2.<br><b>R2</b>*/
	public static final String CADENA_R2 = "R2";
	/**Cadena de texto que representa las letras PER.<br><b>PER</b>*/
	public static final String CADENA_PER = "PER";
	/**Cadena de texto que representa las letras CAN.<br><b>CAN</b>*/
	public static final String CADENA_CAN = "CAN";
	/**Cadena de texto que representa las letras CL2.<br><b>CL2</b>*/
	public static final String CADENA_CL2 = "CL2";
	/**Cadena de texto que representa las letras WEB.<br><b>WEB</b>*/
	public static final String CADENA_WEB = "WEB";
	/**Cadena de texto que representa las letras DB.<br><b>DB</b>*/
	public static final String CADENA_DB = "DB";
	/**Cadena de texto que represetna todas las vocales que pueden aparecer tanto en castellano como en valenciano o francés, acentuadas, sin acentuar, con diéresis, con circunflejo en mayúsculas y minusculas.<br><b>aeiouAEIOUáéíóúàèìòùÁÉÍÓÚÀÈÌÒÙâêîôûÂÊÎÔÛäëïöüÄËÏÖÜ</b>*/
	public static final String CADENA_VOCALES = "aeiouAEIOUáéíóúàèìòùÁÉÍÓÚÀÈÌÒÙâêîôûÂÊÎÔÛäëïöüÄËÏÖÜ";
	/**Cadena de texto que representa las letras OT.<br><b>OT</b>*/
	public static final String CADENA_OT = "OT";
	/**Expresión regular [^\\d] cuyo resultado es obtener los caracteres no numéricos.<br><b>"[^\\d]"</b>*/
	public static final String EXPRESION_REGULAR_ELIMINAR_CARACTERES_NO_NUMERICOS = "[^\\d]";

	/**Cadena de texto orden<br><b>orden</b>*/
	public static final String S_orden = "orden";
	/**Cadena de texto id<br><b>id</b>*/
	public static final String S_id = "id";
	/**Cadena de texto appDefecto<br><b>appDefecto</b>*/
	public static final String S_appDefecto = "appDefecto";
	/**Cadena de texto key<br><b>key</b>*/
	public static final String S_key = "key";
	/**Cadena de texto value<br><b>value</b>*/
	public static final String S_value = "value";
	/**Cadena de texto KeyValueObject<br><b>KeyValueObject</b>*/
	public static final String S_KeyValueObject = "KeyValueObject";
	/**Cadena de texto ArrayList<br><b>ArrayList</b>*/
	public static final String S_ArrayList = "ArrayList";
	/**Cadena de texto idusuario<br><b>idusuario</b>*/
	public static final String S_idusuario = "idusuario";
	/**Cadena de texto nombre<br><b>nombre</b>*/
	public static final String S_nombre = "nombre";
	/**Cadena de texto apellidos<br><b>apellidos</b>*/
	public static final String S_apellidos = "apellidos";
	/**Cadena de texto apellido1<br><b>apellido1</b>*/
	public static final String S_apellido1 = "apellido1";
	/**Cadena de texto apellido2<br><b>apellido2</b>*/
	public static final String S_apellido2 = "apellido2";
	/**Cadena de texto dni<br><b>dni</b>*/
	public static final String S_dni = "dni";
	/**Cadena de texto nif<br><b>nif</b>*/
	public static final String S_nif = "nif";
	/**Cadena de texto identificador<br><b>identificador</b>*/
	public static final String S_identificador = "identificador";
	/**Cadena de texto tipocliente<br><b>tipocliente</b>*/
	public static final String S_tipocliente = "tipocliente";
	/**Cadena de texto taquilla<br><b>taquilla</b>*/
	public static final String S_taquilla = "taquilla";
	/**Cadena de texto idtaquilla<br><b>idtaquilla</b>*/
	public static final String S_idtaquilla = "idtaquilla";
	/**Cadena de texto authority0<br><b>authority0</b>*/
	public static final String S_authority0 = "authority0";
	/**Cadena de texto authority1<br><b>authority1</b>*/
	public static final String S_authority1 = "authority1";
	/**Cadena de texto venta<br><b>venta</b>*/
	public static final String S_venta = "venta";
	/**Cadena de texto idventa<br><b>idventa</b>*/
	public static final String S_idventa = "idventa";
	/**Cadena de texto bloquear<br><b>bloquear</b>*/
	public static final String S_bloquear = "bloquear";
	/**Cadena de texto desbloquear<br><b>desbloquear</b>*/
	public static final String S_desbloquear = "desbloquear";
	/**Cadena de texto descripcion<br><b>descripcion</b>*/
	public static final String S_descripcion = "descripcion";
	/**Cadena de texto dadodebaja<br><b>dadodebaja</b>*/
	public static final String S_dadodebaja = "dadodebaja";
	/**Cadena de texto lineaventa<br><b>lineaventa</b>*/
	public static final String S_lineaventa = "lineaventa";
	/**Cadena de texto fecha<br><b>fecha</b>*/
	public static final String S_fecha = "fecha";
	/**Cadena de texto fechaNacimiento<br><b>fechaNacimiento</b>*/
	public static final String S_fechaNacimiento = "fechaNacimiento";
	/**Cadena de texto fechainiciovigencia<br><b>fechainiciovigencia</b>*/
	public static final String S_fechainiciovigencia = "fechainiciovigencia";
	/**Cadena de texto fechafinvigencia<br><b>fechafinvigencia</b>*/
	public static final String S_fechafinvigencia = "fechafinvigencia";
	/**Cadena de texto fechaAlta<br><b>fechaAlta</b>*/
	public static final String S_fechaAlta = "fechaAlta";
	/**Cadena de texto fechaCaducidad<br><b>fechaCaducidad</b>*/
	public static final String S_fechaCaducidad = "fechaCaducidad";
	/**Cadena de texto fechacaducidad<br><b>fechacaducidad</b>*/
	public static final String S_fechacaducidad = "fechacaducidad";
	/**Cadena de texto fechayhoraventa<br><b>fechayhoraventa</b>*/
	public static final String S_fechayhoraventa = "fechayhoraventa";
	/**Cadena de texto editarGrupo<br><b>editarGrupo</b>*/
	public static final String S_editarGrupo = "editarGrupo";
	/**Cadena de texto idTarjeta<br><b>idTarjeta</b>*/
	public static final String S_idTarjeta = "idTarjeta";
	/**Cadena de texto idGrupoTarjetas<br><b>idGrupoTarjetas</b>*/
	public static final String S_idGrupoTarjetas = "idGrupoTarjetas";
	/**Cadena de texto codCliente<br><b>codCliente</b>*/
	public static final String S_codCliente = "codCliente";
	/**Cadena de texto codSexo<br><b>codSexo</b>*/
	public static final String S_codSexo = "codSexo";
	/**Cadena de texto cacGruposTarjeta<br><b>cacGruposTarjeta</b>*/
	public static final String S_cacGruposTarjeta = "cacGruposTarjeta";
	/**Cadena de texto cacClientes<br><b>cacClientes</b>*/
	public static final String S_cacClientes = "cacClientes";
	/**Cadena de texto CacEstadoCl<br><b>CacEstadoCl</b>*/
	public static final String S_CacEstadoCl = "CacEstadoCl";
	/**Cadena de texto codEstadoCl<br><b>codEstadoCl</b>*/
	public static final String S_codEstadoCl = "codEstadoCl";
	/**Cadena de texto cacTarjetas<br><b>cacTarjetas</b>*/
	public static final String S_cacTarjetas = "cacTarjetas";
	/**Cadena de texto nombreArchivo<br><b>nombreArchivo</b>*/
	public static final String S_nombreArchivo = "nombreArchivo";
	/**Cadena de texto codigo<br><b>codigo</b>*/
	public static final String S_codigo = "codigo";
	/**Cadena de texto proceso<br><b>proceso</b>*/
	public static final String S_proceso = "proceso";
	/**Cadena de texto facturable<br><b>facturable</b>*/
	public static final String S_facturable = "facturable";
	/**Cadena de texto fechaultimamodificacion<br><b>fechaultimamodificacion</b>*/
	public static final String S_fechaultimamodificacion = "fechaultimamodificacion";
	/**Cadena de texto direccionfiscal<br><b>direccionfiscal</b>*/
	public static final String S_direccionfiscal = "direccionfiscal";
	/**Cadena de texto periodofacturacion<br><b>periodofacturacion</b>*/
	public static final String S_periodofacturacion = "periodofacturacion";
	/**Cadena de texto periodoenvio<br><b>periodoenvio</b>*/
	public static final String S_periodoenvio = "periodoenvio";
	/**Cadena de texto canal<br><b>canal</b>*/
	public static final String S_canal = "canal";
	/**Cadena de texto idcanal<br><b>idcanal</b>*/
	public static final String S_idcanal = "idcanal";
	/**Cadena de texto nombreCanal<br><b>nombreCanal</b>*/
	public static final String S_nombreCanal = "nombreCanal";
	/**Cadena de texto idTipoCanal<br><b>idTipoCanal</b>*/
	public static final String S_idTipoCanal = "idTipoCanal";
	/**Cadena de texto nombreTipoCanal<br><b>nombreTipoCanal</b>*/
	public static final String S_nombreTipoCanal = "nombreTipoCanal";
	/**Cadena de texto facturarsucursal<br><b>facturarsucursal</b>*/
	public static final String S_facturarsucursal = "facturarsucursal";
	/**Cadena de texto formapago<br><b>formapago</b>*/
	public static final String S_formapago = "formapago";
	/**Cadena de texto fechaalta<br><b>fechaalta</b>*/
	public static final String S_fechaalta = "fechaalta";
	/**Cadena de texto usuario<br><b>usuario</b>*/
	public static final String S_usuario = "usuario";
	/**Cadena de texto tipoclienteactivo<br><b>tipoclienteactivo</b>*/
	public static final String S_tipoclienteactivo = "tipoclienteactivo";
	/**Cadena de texto tiporappelactivo<br><b>tiporappelactivo</b>*/
	public static final String S_tiporappelactivo = "tiporappelactivo";
	/**Cadena de texto grupoempresasactivo<br><b>grupoempresasactivo</b>*/
	public static final String S_grupoempresasactivo = "grupoempresasactivo";
	/**Cadena de texto fechainiciofacturable<br><b>fechainiciofacturable</b>*/
	public static final String S_fechainiciofacturable = "fechainiciofacturable";
	/**Cadena de texto nombrecompleto<br><b>nombrecompleto</b>*/
	public static final String S_nombrecompleto = "nombrecompleto";
	/*Cadena de texto codTipoTarjeta<br><b>codTipoTarjeta</b>*
	public static final String S_codTipoTarjeta = "codTipoTarjeta";
	**Cadena de texto impTarjetaTodos<br><b>impTarjetaTodos</b>*
	public static final String S_impTarjetaTodos = "impTarjetaTodos";
	**Cadena de texto registroAltaTitular<br><b>registroAltaTitular</b>*
	public static final String S_registroAltaTitular = "registroAltaTitular";
	**Cadena de texto busquedaClientesOtro<br><b>busquedaClientesOtro</b>*
	public static final String S_busquedaClientesOtro = "busquedaClientesOtro";
	**Cadena de texto goto<br><b>goto</b>*
	public static final String S_goto = "goto";
	**Cadena de texto registroAltaOtroMiembro<br><b>registroAltaOtroMiembro</b>*
	public static final String S_registroAltaOtroMiembro = "registroAltaOtroMiembro";
	**Cadena de texto procesoVentaIndividual<br><b>procesoVentaIndividual</b>*
	public static final String S_procesoVentaIndividual = "procesoVentaIndividual";
	**Cadena de texto listaProcesoVenta<br><b>listaProcesoVenta</b>*
	public static final String S_listaProcesoVenta = "listaProcesoVenta";
	**Cadena de texto guardar<br><b>guardar</b>*
	public static final String S_guardar = "guardar";
	**Cadena de texto cancelar<br><b>cancelar</b>*
	public static final String S_cancelar = "cancelar";
	**Cadena de texto renovar<br><b>renovar</b>*
	public static final String S_renovar = "renovar";
	**Cadena de texto impTarjetas<br><b>impTarjetas</b>*
	public static final String S_impTarjetas = "impTarjetas";
	**Cadena de texto parm_IdNewTitular<br><b>parm_IdNewTitular</b>*
	public static final String S_parm_IdNewTitular = "parm_IdNewTitular";
	**Cadena de texto parm_IdNewClienteTitular<br><b>parm_IdNewClienteTitular</b>*
	public static final String S_parm_IdNewClienteTitular = "parm_IdNewClienteTitular";
	**Cadena de texto parm_IdModePaseTitular<br><b>parm_IdModePaseTitular</b>*
	public static final String S_parm_IdModePaseTitular = "parm_IdModePaseTitular";
	**Cadena de texto parm_IdTarjetaCanc<br><b>parm_IdTarjetaCanc</b>*
	public static final String S_parm_IdTarjetaCanc = "parm_IdTarjetaCanc";
	**Cadena de texto parm_IdEstTarjCanc<br><b>parm_IdEstTarjCanc</b>*
	public static final String S_parm_IdEstTarjCanc = "parm_IdEstTarjCanc";
	**Cadena de texto parm_IdTarjetaReno<br><b>parm_IdTarjetaReno</b>*
	public static final String S_parm_IdTarjetaReno = "parm_IdTarjetaReno";
	**Cadena de texto parm_IdEstTarjReno<br><b>parm_IdEstTarjReno</b>*
	public static final String S_parm_IdEstTarjReno = "parm_IdEstTarjReno";
	**Cadena de texto parm_IdCliTarjReno<br><b>parm_IdCliTarjReno</b>*
	public static final String S_parm_IdCliTarjReno = "parm_IdCliTarjReno";
	**Cadena de texto parm_IdTarjeta<br><b>parm_IdTarjeta</b>*
	public static final String S_parm_IdTarjeta = "parm_IdTarjeta";
	**Cadena de texto parm_IdEstTarj<br><b>parm_IdEstTarj</b>*
	public static final String S_parm_IdEstTarj = "parm_IdEstTarj";
	**Cadena de texto parm_IdCliTarj<br><b>parm_IdCliTarj</b>*
	public static final String S_parm_IdCliTarj = "parm_IdCliTarj";
	**Cadena de texto parm_IdTarjetaImp<br><b>parm_IdTarjetaImp</b>*
	public static final String S_parm_IdTarjetaImp = "parm_IdTarjetaImp";
	**Cadena de texto parm_IdTipoTarjeta<br><b>parm_IdTipoTarjeta</b>*
	public static final String S_parm_IdTipoTarjeta = "parm_IdTipoTarjeta";
	**Cadena de texto parm_IdCodCliente<br><b>parm_IdCodCliente</b>*
	public static final String S_parm_IdCodCliente = "parm_IdCodCliente";
	**Cadena de texto parm_IdGrupoTarjeta<br><b>parm_IdGrupoTarjeta</b>*
	public static final String S_parm_IdGrupoTarjeta = "parm_IdGrupoTarjeta";*/
	/**Constante de tipo cadena que representa el valor Error 105<br><b>"Error 105"</b>.*/
	public static final String S_Error_105 = "Error 105";
	/**Constante de tipo cadena que representa el valor Error 106<br><b>"Error 106"</b>.*/
	public static final String S_Error_106 = "Error 106";
	/**Constante de tipo cadena que representa el valor Error 107<br><b>"Error 107"</b>.*/
	public static final String S_Error_107 = "Error 107";
	/**Constante de tipo cadena que representa el valor Error 120<br><b>"Error 120"</b>.*/
	public static final String S_Error_120 = "Error 120";
	/**Constante de tipo cadena que representa el valor entradas<br><b>"entradas"</b>.*/
	public static final String S_entradas = "entradas";
	/**Constante de tipo cadena que representa el valor lineadetalle<br><b>"lineadetalle"</b>.*/
	public static final String S_lineadetalle = "lineadetalle";
	/**Constante de tipo cadena que representa el valor lineadetalles<br><b>"lineadetalles"</b>.*/
	public static final String S_lineadetalles = "lineadetalles";
	/**Constante de tipo cadena que representa el valor lineadetallezonasesions<br><b>"lineadetallezonasesions"</b>.*/
	public static final String S_lineadetallezonasesions = "lineadetallezonasesions";
	/**Constante de tipo cadena que representa el valor tipoproducto<br><b>"tipoproducto"</b>.*/
	public static final String S_tipoproducto = "tipoproducto";
	/**Constante de tipo cadena que representa el valor tarifaproducto<br><b>"tarifaproducto"</b>.*/
	public static final String S_tarifaproducto = "tarifaproducto";
	/**Constante de tipo cadena que representa el valor tarifa<br><b>"tarifa"</b>.*/
	public static final String S_tarifa = "tarifa";
	/**Constante de tipo cadena que representa el valor anulada<br><b>"anulada"</b>.*/
	public static final String S_anulada = "anulada";
	/**Constante de tipo cadena que representa el valor tpp<br><b>"tpp"</b>.*/
	public static final String S_tpp = "tpp";
	/**Constante de tipo cadena que representa el valor tp<br><b>"tp"</b>.*/
	public static final String S_tp = "tp";
	/**Constante de tipo cadena que representa el valor zs<br><b>"zs"</b>.*/
	public static final String S_zs = "zs";
	/**Constante de tipo cadena que representa el valor su<br><b>"su"</b>.*/
	public static final String S_su = "su";	
	/**Constante de tipo cadena que representa el valor lv<br><b>"lv"</b>.*/
	public static final String S_lv = "lv";
	/**Constante de tipo cadena que representa el valor ld<br><b>"ld"</b>.*/
	public static final String S_ld = "ld";
	/**Constante de tipo cadena que representa el valor ldzs<br><b>"ldzs"</b>.*/
	public static final String S_ldzs = "ldzs";
	/**Constante de tipo cadena que representa el valor NEXTVAL<br><b>"NEXTVAL"</b>.*/
	public static final String S_NEXTVAL = "NEXTVAL";
	/**Constante de tipo cadena que representa el valor CURRVAL<br><b>"CURRVAL"</b>.*/
	public static final String S_CURRVAL = "CURRVAL";

//	/**Palabra reservada en lenguaje SQL para la ordenacion ascendente.<br><b>asc</b>*/
//	public static final String PALABRA_RESERVADA_ORDERBY_ASCENDENTE_SQL = "asc";
//	/**Palabra reservada en lenguaje SQL para la ordenacion descendente.<br><b>desc</b>*/
//	public static final String PALABRA_RESERVADA_ORDERBY_DESCENDENTE_SQL = "desc";
//	/**Palabra reservada en lenguaje SQL para comparación Cadenas de texto.<br><b>like</b>*/
//	public static final String PALABRA_RESERVADA_COMPARACION_SQL = "like";
//
//	/**Etiqueta que marca el inicio de un nodo xml denominado HTML.<br><b><HTML></b>*/
//	public static final String NODO_XML_HTML_APERTURA = "<HTML>";
//	/**Etiqueta que marca el cierre de un nodo xml denominado HTML.<br><b></HTML></b>*/
//	public static final String NODO_XML_HTML_CIERRE = "</HTML>";
//	/**Etiqueta que marca el inicio de un nodo xml denominado CENTER.<br><b><CENTER></b>*/
//	public static final String NODO_XML_CENTER_APERTURA = "<CENTER>";
//	/**Etiqueta que marca el cierre de un nodo xml denominado CENTER.<br><b></CENTER></b>*/
//	public static final String NODO_XML_CENTER_CIERRE = "</CENTER>";
//	/**Etiqueta que marca el inicio de un nodo xml denominado LEFT.<br><b><LEFT></b>*/
//	public static final String NODO_XML_LEFT_APERTURA = "<LEFT>";
//	/**Etiqueta que marca el cierre de un nodo xml denominado LEFT.<br><b></LEFT></b>*/
//	public static final String NODO_XML_LEFT_CIERRE = "</LEFT>";
//	/**Etiqueta que marca el cierre de un nodo xml denominado BR.<br><b><BR></b>*/
//	public static final String NODO_XML_BR = "<BR>";
//	/**Etiqueta que marca el inicio de un nodo xml denominado STRONG.<br><b><STRONG></b>*/
//	public static final String NODO_XML_STRONG_APERTURA = "<STRONG>";
//	/**Etiqueta que marca el cierre de un nodo xml denominado STRONG.<br><b></STRONG></b>*/
//	public static final String NODO_XML_STRONG_CIERRE = "</STRONG>";
//	/**Etiqueta que marca el inicio de un nodo xml denominado BODY.<br><b><BODY></b>*/
//	public static final String NODO_XML_BODY_APERTURA = "<BODY>";
//	/**Etiqueta que marca el cierre de un nodo xml denominado BODY.<br><b></BODY></b>*/
//	public static final String NODO_XML_BODY_CIERRE = "</BODY>";
//	/**Etiqueta que marca el inicio de un nodo xml denominado FONT.<br><b><FONT></b>*/
//	public static final String NODO_XML_FONT_APERTURA = "<FONT>";
//	/**Etiqueta que marca el cierre de un nodo xml denominado FONT.<br><b></FONT></b>*/
//	public static final String NODO_XML_FONT_CIERRE = "</FONT>";

	/**Etiqueta que marca el inicio de un nodo xml denominado p.<br><b><p></b>*/
	public static final String NODO_XML_p_APERTURA = "<p>";
	/**Etiqueta que marca el cierre de un nodo xml denominado p.<br><b></p></b>*/
	public static final String NODO_XML_p_CIERRE = "</p>";
	/**Etiqueta que marca el inicio de un nodo xml de error.<br><b><error></b>*/
	public static final String NODO_XML_error_APERTURA = "<error>";
	/**Etiqueta que marca el cierre de un nodo xml de error.<br><b></error></b>*/
	public static final String NODO_XML_error_CIERRE = "</error>";
	/**Etiqueta que marca un nodo xml de error vacío.<br><b><error/></b>*/
	public static final String NODO_XML_error_VACIO = "<error/>";
	/**Etiqueta que marca el inicio de un nodo xml de mensaje.<br><b><mensaje></b>*/
	public static final String NODO_XML_mensaje_APERTURA = "<mensaje>";
	/**Etiqueta que marca el cierre de un nodo xml de mensaje.<br><b></mensaje></b>*/
	public static final String NODO_XML_mensaje_CIERRE = "</mensaje>";
	/**Etiqueta que marca el inicio de un nodo xml de codigo.<br><b><codigo></b>*/
	public static final String NODO_XML_codigo_APERTURA = "<codigo>";
	/**Etiqueta que marca el cierre de un nodo xml de codigo.<br><b></codigo></b>*/
	public static final String NODO_XML_codigo_CIERRE = "</codigo>";
	/**Etiqueta que marca el inicio de un nodo xml de string.<br><b><string></b>*/
	public static final String NODO_XML_string_APERTURA = "<string>";
	/**Etiqueta que marca el cierre de un nodo xml de string.<br><b></string></b>*/
	public static final String NODO_XML_string_CIERRE = "</string>";
	/**Etiqueta que marca el inicio de un nodo xml de value.<br><b><value></b>*/
	public static final String NODO_XML_value_APERTURA = "<value>";
	/**Etiqueta que marca el cierre de un nodo xml de value.<br><b></value></b>*/
	public static final String NODO_XML_value_CIERRE = "</value>";
	/**Etiquetas XML utilizadas para abrir el xml de las respuestas con error del {@link InvocadorServiciosHttp}<br><b><error><codigo></b>*/
	public static final String NODOS_XML_error_codigo_APERTURA = new StringBuffer( NODO_XML_error_APERTURA ).append( NODO_XML_codigo_APERTURA ).toString();
	/**Etiquetas XML utilizadas para cerrar el xml de las respuestas con error del {@link InvocadorServiciosHttp}<br><b></mensaje></error></b>*/
	public static final String NODOS_XML_mensaje_error_CIERRE = new StringBuffer( NODO_XML_mensaje_CIERRE ).append( NODO_XML_error_CIERRE ).toString();
	/**Etiquetas XML utilizadas para comenzar a describir el mensaje de error en las respuestas con error del {@link InvocadorServiciosHttp}<br><b></codigo></mensaje></b>*/
	public static final String NODOS_XML_codigocierre_mensajeapertura = new StringBuffer( NODO_XML_codigo_CIERRE ).append( NODO_XML_mensaje_APERTURA ).toString();

	/**Etiqueta que marca el inicio de un nodo xml que representa el atributo importe.<br><b><importe></b>*/
	public static final String NODO_XML_importe_APERTURA = "<importe>";
	/**Etiqueta que marca el cierre de un nodo xml que representa el atributo importe.<br><b></importe></b>*/
	public static final String NODO_XML_importe_CIERRE = "</importe>";

	/**Nombre de la tarea que permite caducar las tarjetas del Club Ciutat, envio de correos automáticos en función de los eventos definidos, ...<br><b>"tareasAutomaticasClub"</b>*/
	public static final String NOMBRE_TAREA_tareasAutomaticasClub = "tareasAutomaticasClub";

	/**Nombre de la NamedQuery que obtendrá las sesiones dadas de baja del tipo de producto Hemisfèric, para la hora y la fecha especificadas.<br><b>select-SesionesDadasDeBajaTipoproductoHemisferic</b>*/
	public static final String NOMBRE_CONSULTA_select_SesionesDadasDeBajaTipoproductoHemisferic = "select_SesionesDadasDeBajaTipoproductoHemisferic";

	/**Pais por defecto<br><b>España</b>*/
	public static final String NOMBRE_PAIS_PORDEFECTO = "España";
	/**Cadena de texto que representa el acrónimo del Pais por defecto<br><b>ES</b>*/
	public static final String ACRONIMO_PAIS_PORDEFECTO = "ES";
	/**Identificador del País por defecto<br><b>472</b>*/
	public static final int ID_PAIS_PORDEFECTO = 472;

	/**Nombre del Club de la Ciudad de las Artes y las Ciencias.<br><b>Club Ciudad de las Artes y las Ciencias</b>*/
	public static final String CLUB_CAC = "Club Ciudad de las Artes y las Ciencias";
	/**Asunto del mensaje de correo electrónico a enviar a los nuevos miembros del Club Ciutat.<br><b>Alta en el Club Ciudad de las Artes y las Ciencias</b>*/
	public static final String ASUNTO_CORREO_CONFIRMACION_ALTA_PASE_CLUB = "Alta en el " + CLUB_CAC;
	/**Asunto del mensaje de correo electrónico a enviar a los miembros del Club Ciutat que renuevan el pase.<br><b>Renovación de pase del Club Ciudad de las Artes y las Ciencias</b>*/
	public static final String ASUNTO_CORREO_CONFIRMACION_RENOVACION_PASE_CLUB = "Renovación de pase del " + CLUB_CAC;
	/**Asunto del mensaje de correo electrónico a enviar a los miembros del Club Ciutat cuando su pase se encuentre a un mes de la fecha de caducidad.<br><b>Su tarjeta pase club se encuentra a un mes de su fecha de caducidad !!!</b>*/
	public static final String ASUNTO_CORREO_CADUCIDAD_PROXIMA_PASE_CLUB = CLUB_CAC + ": Su tarjeta pase club se encuentra a un mes de su fecha de caducidad !!!";
	/**Asunto del mensaje de correo electrónico a enviar a los miembros del Club Ciutat cuando su pase ha alcanzado la fecha de caducidad.<br><b>Su tarjeta pase club ha caducado !!!</b>*/
	public static final String ASUNTO_CORREO_CADUCIDAD_PASE_CLUB = CLUB_CAC + ": Su tarjeta pase club ha caducado !!!";
	/**Asunto del mensaje de correo electrónico a enviar a los miembros del Club Ciutat cuando sea su cumpleaños.<br><b>"Felicitación Aniversario"</b>*/
	public static final String ASUNTO_CORREO_ANIVERSARIO_CLIENTE_PASE_CLUB = CLUB_CAC + ": Felicitación Aniversario";
	/**Asunto del mensaje de correo electrónico a enviar a los miembros del Club Ciutat cuando su pase club ha sido bloqueado.<br><b>Club Ciudad de las Artes y las Ciencias: Bloqueo tarjeta pase club.</b>*/
	public static final String ASUNTO_CORREO_BLOQUEO_PASE_CLUB = CLUB_CAC + ": Bloqueo tarjeta pase club.";
	/**Asunto del mensaje de correo electrónico a enviar a los miembros del Club Ciutat cuando su pase club ha sido desbloqueado.<br><b>Club Ciudad de las Artes y las Ciencias: Bloqueo tarjeta pase club.</b>*/
	public static final String ASUNTO_CORREO_DESBLOQUEO_PASE_CLUB = CLUB_CAC + ": Desbloqueo tarjeta pase club.";
	/**Asunto del mensaje de correo electrónico a enviar a los miembros del Club Ciutat cuando su pase club ha sido cancelado.<br><b>Club Ciudad de las Artes y las Ciencias: Cancelación tarjeta pase club.</b>*/
	public static final String ASUNTO_CORREO_CANCELACION_PASE_CLUB = CLUB_CAC + ": Cancelación tarjeta pase club.";
	/**Asunto del mensaje de correo electrónico a enviar a los miembros del Club Ciutat cuando su pase club ha sido activado.<br><b>Club Ciudad de las Artes y las Ciencias: Activación tarjeta pase club.</b>*/
	public static final String ASUNTO_CORREO_ACTIVACION_PASE_CLUB = CLUB_CAC + ": Activación tarjeta pase club.";
	
	/**Etiquetas pseudoXML utilizadas para cerrar cada uno de los identificadores de entrada que devuelve el servidor de impresion a la capa de servicios, en caso de producirse problemas en la impresión.<br><b>|idEntrada|</b>*/
	public static final String MARCA_idEntrada_RESPONSEBODY_APERTURA = "|idEntrada|";
	/**Etiquetas pseudoXML utilizadas para cerrar cada uno de los identificadores de entrada que devuelve el servidor de impresion a la capa de servicios, en caso de producirse problemas en la impresión.<br><b>|_idEntrada|</b>*/
	public static final String MARCA_idEntrada_RESPONSEBODY_CIERRE = "|_idEntrada|";
	/**Etiqueta pseudoXML utilizada para abrir cada uno de los mensajes de error que devuelve el servidor de impresion a la capa de servicios, en caso de producirse problemas en la impresión.<br><b>|mensaje_error|</b>*/
	public static final String MARCA_mensaje_error_RESPONSEBODY_APERTURA = "|mensaje_error|";
	/**Etiqueta pseudoXML utilizada para cerrar cada uno de los mensajes de error que devuelve el servidor de impresion a la capa de servicios, en caso de producirse problemas en la impresión.<br><b>|_mensaje_error|</b>*/
	public static final String MARCA_mensaje_error_RESPONSEBODY_CIERRE = "|_mensaje_error|";

	/**Etiquetas pseudoXML utilizadas para cerrar cada uno de los identificadores de entrada que devuelve el servidor de impresion a la capa de servicios, en caso de producirse problemas en la impresión.<br><b><idEntrada<</b>*/
	public static final String NODO_XML_idEntrada_APERTURA = "<idEntrada>";
	/**Etiquetas pseudoXML utilizadas para cerrar cada uno de los identificadores de entrada que devuelve el servidor de impresion a la capa de servicios, en caso de producirse problemas en la impresión.<br><b></idEntrada<</b>*/
	public static final String NODO_XML_idEntrada_CIERRE = "</idEntrada>";
	/**Etiquetas pseudoXML utilizadas para comenzar el identificador de la venta que devuelve el servidor de impresion a la capa de servicios.<br><b><idventa<</b>*/
	public static final String NODO_XML_idventa_APERTURA = "<idventa>";
	/**Etiquetas pseudoXML utilizadas para cerrar el identificador de la venta que devuelve el servidor de impresion a la capa de servicios.<br><b></idventa<</b>*/
	public static final String NODO_XML_idventa_CIERRE = "</idventa>";
	/**Etiquetas pseudoXML utilizadas para comenzar la marca de si es reimpresion que devuelve el servidor de impresion a la capa de servicios.<br><b><reimpresion<</b>*/
	public static final String NODO_XML_reimpresion_APERTURA = "<reimpresion>";
	/**Etiquetas pseudoXML utilizadas para cerrar la marca de si es reimpresion que devuelve el servidor de impresion a la capa de servicios.<br><b></reimpresion<</b>*/
	public static final String NODO_XML_reimpresion_CIERRE = "</reimpresion>";
	/**Etiquetas pseudoXML utilizadas para comenzar la marca del tipo del trabajo de impresión que devuelve el servidor de impresion a la capa de servicios.<br><b><tipo<</b>*/
	public static final String NODO_XML_tipo_APERTURA = "<tipo>";
	/**Etiquetas pseudoXML utilizadas para cerrar la marca de tipo del trabajo de impresión que devuelve el servidor de impresion a la capa de servicios.<br><b></tipo<</b>*/
	public static final String NODO_XML_tipo_CIERRE = "</tipo>";
	/**Etiquetas pseudoXML utilizadas para comenzar el identificador de la idusuario que devuelve el servidor de impresion a la capa de servicios.<br><b><idusuario<</b>*/
	public static final String NODO_XML_idusuario_APERTURA = "<idusuario>";
	/**Etiquetas pseudoXML utilizadas para cerrar el identificador de la idusuario que devuelve el servidor de impresion a la capa de servicios.<br><b></idusuario<</b>*/
	public static final String NODO_XML_idusuario_CIERRE = "</idusuario>";
	/**Etiquetas pseudoXML utilizadas para comenzar el identificador de la idsesionusuario que devuelve el servidor de impresion a la capa de servicios.<br><b><idsesionusuario<</b>*/
	public static final String NODO_XML_idsesionusuario_APERTURA = "<idsesionusuario>";
	/**Etiquetas pseudoXML utilizadas para cerrar el identificador de la idsesionusuario que devuelve el servidor de impresion a la capa de servicios.<br><b></idsesionusuario<</b>*/
	public static final String NODO_XML_idsesionusuario_CIERRE = "</idsesionusuario>";
	/**Etiquetas pseudoXML utilizadas para especificar que la lista que devuelve el servidor de impresion a la capa de servicios, en caso de producirse problemas en la impresión.<br><b><ListaIdEntradasNoImpresas/></b>*/
	public static final String NODO_XML_ListaIdEntradasNoImpresas_LISTAVACIA = "<ListaIdEntradasNoImpresas/>";
	/**Etiquetas pseudoXML utilizadas para especificar que la lista que devuelve el servidor de impresion a la capa de servicios, en caso de producirse problemas en la impresión.<br><b><ListaIdEntradasNoImpresas /></b>*/
	public static final String NODO_XML_ListaIdEntradasNoImpresas__LISTAVACIA = "<ListaIdEntradasNoImpresas />";
	/**Etiquetas pseudoXML utilizadas para especificar que la lista de entradas que devuelve el servidor de impresion al ControllerResultadosImpresion.<br><b><ListaIdEntradas<</b>*/
	public static final String NODO_XML_ListaIdEntradas_APERTURA = "<ListaIdEntradas>";
	/**Etiquetas pseudoXML utilizadas para especificar que la lista de entradas que devuelve el servidor de impresion al ControllerResultadosImpresion.<br><b><ListaIdEntradas<</b>*/
	public static final String NODO_XML_ListaIdEntradas_CIERRE = "</ListaIdEntradas>";
	/**Etiquetas pseudoXML utilizadas para especificar que la lista de entradas no impresas que devuelve el servidor de impresion al ControllerResultadosImpresion.<br><b><ListaIdEntradasNoImpresas<</b>*/
	public static final String NODO_XML_ListaIdEntradasNoImpresas_APERTURA = "<ListaIdEntradasNoImpresas>";
	/**Etiquetas pseudoXML utilizadas para especificar que la lista de entradas no impresas que devuelve el servidor de impresion al ControllerResultadosImpresion.<br><b></ListaIdEntradasNoImpresas<</b>*/
	public static final String NODO_XML_ListaIdEntradasNoImpresas_CIERRE = "</ListaIdEntradasNoImpresas>";
	/**Etiqueta pseudoXML utilizada para abrir cada uno de los mensajes de error que devuelve el servidor de impresion a la capa de servicios, en caso de producirse problemas en la impresión.<br><b><mensaje_error<</b>*/
	public static final String NODO_XML_mensaje_error_APERTURA = "<mensaje_error>";
	/**Etiqueta pseudoXML utilizada para cerrar cada uno de los mensajes de error que devuelve el servidor de impresion a la capa de servicios, en caso de producirse problemas en la impresión.<br><b></mensaje_error<</b>*/
	public static final String NODO_XML_mensaje_error_CIERRE = "</mensaje_error>";
	
	/**Nombre del parametro a agregar a la cabecera de las peticiones a la capa de servicios que contiene los argumenros de los servicios invocados.<br><b>xml</b>*/
	public static final String NOMBRE_PARAMETRO_xml = "xml";
	/**Nombre del parametro a agregar a la cabecera de las peticiones a la capa de servicios cuyo valor será siempre de 1.<br>Es necesario para que funcione la llamada.<br><b>exe</b>*/
	public static final String NOMBRE_PARAMETRO_exe = "exe";
	/**Nombre del Metodo POST por el cual se imprimen las entradas en taquillas y puestos de Colossus para la impresión de recibos, operados por personal de CAC<hr><b>"add"</b>*/
	public static final String NOMBRE_METODO_add = "add";
	/**Nombre del Metodo POST por el cual se imprimen las entradas desde los puntos de recogida de entradas de Colossus, sin intervención de personal de CAC<br><b>"direct_print"</b>*/
	public static final String NOMBRE_METODO_direct_print = "direct_print";
	/**Nombre del Metodo POST por el cual se imprimen las entradas en taquillas y puestos de Colossus, operados por personal de CAC<hr><b>"add_defered"</b>*/
	public static final String NOMBRE_METODO_add_defered = "add_defered";
	/**Nombre del Metodo POST por el cual se envían los reconocimientos a los Servidores de Impresión de las taquillas.<hr><b>"ack"</b>*/
	public static final String NOMBRE_METODO_ack = "ack";


	/**Cadena de texto que representa el nombre de la capa de servicios. Utilizar en la construccion de urls<br><b>serviciosCAC</b>*/
	public static final String NOMBRE_CAPA_SERVICIOS = "services";
	/**Nombre del servicio al que invocar para realizar login ante la capa de servicios<br><b>getUserDetails</b>*/
	public static final String NOMBRE_SERVICIO_LOGIN = "getUserDetails";
	/**Etiqueta a buscar en la respuesta a la llamada al servicio de login<br><b>idusuario</b>*/
	public static final String ETIQUETA_RESPUESTA_LOGIN_USUARIO = S_idusuario;
	/**Nombre de usuario con el que incializar la variable username para las credenciales de application.<hr><b>"NONE_PROVIDED"</b>*/
	public static final String NOMBRE_USUARIO_POR_DEFECTO = "NONE_PROVIDED";
	/**Etiqueta con la que se cierra la pagina html que devuelven las llamadas a los servicios a traves de peticiones http acabadas en .invoke<br><b>\<\/textarea\></b>*/
	public static final String ETIQUETA_CIERRE_RESPUESTA_INVOKES = "</textarea>";
	/**Marca que indica el inicio de los parametros a pasar en la llamada a un servicio.<br><b><servicio><parametro></b>*/
	public static final String MARCA_INICIO_PARAMETRO_XML = "<servicio><parametro>";
	/**Marca que indica el final de los parametros a pasar en la llamada a un servicio.<br><b></parametro></servicio></b>*/
	public static final String MARCA_CIERRE_PARAMETRO_XML = "</parametro></servicio>";

	/**Nombre de la cabecera a emplear en todas las llamadas de autenticaci�n ante la capa de servicios.<br><b>Authorization</b>*/
	public static final String NOMBRE_CABECERA_AUTENTICACION = "Authorization";
	/**Marca que representa el tipo de autenticacion basico, que es el que vamos a utilizar siempre en todas las cabeceras para autenticarnos ante la capa de servicios.<br><b>Basic </b>*/
	public static final String MARCA_AUTENTICACION_BASICA = "Basic";
	/**Marca de apertura del parametro xml que se le pasa al servicios de autenticacion devuelva o no el usuario.*/
	public static final String MARCA_INICIO_PARAMETRO_XML_AUTENTICACION = "<servicio><parametro><string>";
	/**Marca de cierre del parametro xml que se le pasa al servicios de autenticacion devuelva o no el usuario.*/
	public static final String MARCA_CIERRE_PARAMETRO_XML_AUTENTICACION = "</string></parametro></servicio>";
	/**Marca de apertura del segundo argumento del método process utilizado en las llamadas por SOA a servicios de internet desde internetCAC<br><b><servicio><parametro><int></b>*/
	public static final String MARCA_INICIO_LLAMADA_SERVICIO_INTERNET = "<servicio><parametro><int>";
	/**Marca de cierre del segundo argumento del método process utilizado en las llamadas por SOA a servicios de internet desde internetCAC<br><b></int><string>internet</string></parametro></servicio></b>*/
	public static final String MARCA_CIERRE_LLAMADA_SERVICIO_INTERNET = "</int><string>internet</string></parametro></servicio>";

	/**Etiqueta que representa un nodo servicio vacío.<br><b><servicio/></b>*/
	public static final String NODO_XML_SERVICIO_VACIO = "<servicio/>";

	/**Nombre del bean a traves del que podremos realizar las llamadas a la capa de servicios de internet desde el proyecto internetCAC.<br><b>inetServiceSOA</b>*/
	public static final String NOMBRE_BEAN_inetServiceSOA = "inetServiceSOA";
	/**Nombre de las instancias del Club definidas sobre los beans de la Clase GrupoCacMB.<br><b>"grupoCacMB"</b>*/
	public static final String NOMBRE_BEAN_grupoCacMB = "grupoCacMB";
	/**Nombre de las instancias del Club definidas sobre los beans de la Clase TarjetaPaseMB.<br><b>"tarjetaPaseMB"</b>*/
	public static final String NOMBRE_BEAN_tarjetaPaseMB = "tarjetaPaseMB";
	/**Nombre del bean que nos va a a permitir realizar la autenticación por SOA. Lo ideal sería obtenerlo del contexto de la aplicación a través de Spring, pues se configura en el fichero spring-security.xml como un atributo del bean CacSOAAuthenticationProvider.<hr><b>"serviceSOAProvider"</b>*/
	public static final String NOMBRE_BEAN_serviceSOA = "serviceSOAProvider";
	/**Nombre del servicio para realizar la autenticación por SOA. Lo ideal sería obtenerlo del contexto de la aplicación a través de Spring, pues se configura en el fichero spring-security.xml como un atributo del bean CacSOAAuthenticationProvider.<hr><b>"authenticationInvokerHttp"</b>*/
	public static final String NOMBRE_BEAN_authenticationInvokerHttp = "authenticationInvokerHttp";
	/**Nombre del servicio para realizar la autenticación por SOA. Lo ideal sería obtenerlo del contexto de la aplicación a través de Spring, pues se configura en el fichero spring-security.xml como un atributo del bean CacSOAAuthenticationProvider.<hr><b>"httpServiceSOA"</b>*/
	public static final String NOMBRE_BEAN_httpServiceSOA = "httpServiceSOA";

	/**Tipo de publicacion de los servicios. Con <b>.invoke<b> hemos verificado el correcto funcionamiento. Estamos validando con <b>SOAWrapper</b>*/
	public static final String TIPO_PUBLICACION_SERVICIOS_INVOKE = ".invoke";
	/**Sufijo que se debe añadir en las peticiones via SOA para invocar servicios.<br><b>SOAWrapper<b>*/
	public static final String SUFIJO_MAPEADOR_SERVICIO_SOA = "SOAWrapper";
	/**Nombre asociado a la cache de la sesion de usuario del objeto {@link UserSessionCache} utilizado por el {@link InvocadorServiciosHttp}<br><b>"userSessionCache"</b>*/
	public static final String NOMBRE_CACHE_SESION_USUARIO = "userSessionCache";
	/**URL en la que se va a localizar el ficheo de configuración de la cache de usuarios*/
	public static final String URL_FICHERO_CONFIGURACION_CACHE_USUARIO = "/resources/ehCache-failsafe.xml";

	/**Mensaje a utilizar cuando el servicio produce un error.<br><b>Se ha producido un error durante la llamada al servicio </b>*/
	public static final String MSG_ERROR_LLAMADA_SERVICIO = "Se ha producido un error durante la llamada al servicio ";
	/**Mensaje a utilizar cuando se produzca un error durante la petición de login.<br><b>Se ha producido un error durante la llamada del servicio getUserDetails.</b>*/
	public static final String MSG_ERROR_LLAMADA_AUTENTICACION = "Se ha producido un error durante la llamada del servicio getUserDetails.";
	/**Mensaje genérico de autenticación ante la capa de servicios.<br><b>"<error>Se ha producido un error al autenticarse ante la capa de servicios. Revise los logs.</error>"</b>;*/
	public static final String MSG_ERROR_AUTENTICACION = "<error>Se ha producido un error al autenticarse ante la capa de servicios. Revise los logs.</error>";
	/**Mensaje a utilizar cuando no se pueda resolver la direccion IP del cliente que realiza el login.<br><b>No se ha podido resolver el host local se asigna la IP </b>*/
	public static final String MSG_ERROR_OBTENIENDO_IP_CLIENTE_LOGIN = "No se ha podido resolver el host local se asigna la IP ";
	/**Mensaje de error a mostrar cuando haya un error en la sintaxis de la llamada a un servicio.<br><b>La llamada al servicio no esta bien formada, verifique la sintaxis del nombre de servicio y de los parametros: </b>*/
	public static final String MSG_ERROR_URL_MAL_FORMADA = "La llamada al servicio no esta bien formada, verifique la sintaxis del nombre de servicio y de los parametros: ";
	/**Mensaje de error a mostrar cuando se intente realizar una llamada a un servicio para el cual, el usuario no está autorizado.<br><b>No tiene permisos suficientes para utilizar el servicio </b>*/
	public static final String MSG_ERROR_SERVICIO_NO_AUTORIZADO = "No tiene permisos suficientes para utilizar el servicio ";
	/**Mensaje de error a mostrar cuando se intente realizar una llamada a un servicio no publicado o que no existe.<br><b>Servicio no publicado o inexistete: </b>*/
	public static final String MSG_ERROR_SERVICIO_NO_ENCONTRADO = "Servicio no publicado o inexistente: ";
	/**Mensaje de error a mostrar cuando salte una excepción no controlada al realizar la llamada Http a un servicio.<br><b>"Se ha producido un error no controlado en la llamada al servicio "</b>*/
	public static final String MSG_ERROR_NO_CONTROLADO_LLAMADA_SERVICIO = "Se ha producido un error no controlado en la llamada al servicio ";
	/**Nodo XML advertencia que notifica que el servicio devuelve una lista vacía.<br><b><warning>Atencion el servicio devuelve una lista vacia</warning></b>*/
	public static final String ADVERTENCIA_LISTA_VACIA = "<warning>Atencion el servicio devuelve una lista vacia</warning>";
	/**Marca que indica el fin o cierre de un nodo xml sin hijos, por ejemplo listas vacías.<br><b>/></b>*/
	public static final String MARCA_CIERRE_NODO_XML_VACIO = "/>";
	/**Marca que indica que lo que sigue es una etiqueta de XML, bien de apertura o de cierre de nodo.<br><b><</b>*/
	public static final String MARCA_INICIO_ETIQUETA_XML = "<";
	/**Marca que indica que el final de un nodo XML, bien de apertura o de cierre.<br><b>></b>*/
	public static final String MARCA_FIN_ETIQUETA_XML = ">";
	/**Marca que indica que lo que sigue es una etiqueta de XML, de cierre de nodo.<br><b></</b>*/
	public static final String MARCA_INICIO_CIERRE_ETIQUETA_XML = "</";
	/**Cadena de texto que indica que se ha de buscar en todos los subdirectorios de la ruta que encontremos justo delante.*/
	public static final String MARCA_BUSQUEDA_SUBDIRECTORIOS = "/**/";

	/**Cadena de texto que se usa como indicador de un mensaje de error que se devuelve a la vista en el proyecto internetCAC.<br><b>ERROR !</b>*/
	public static final String MARCA_MENSAJE_ERROR_JSPCONTROLLER = "ERROR !";
	/**Cadena de texto con la que comienzan la mayoría de las descripciones de los errores se devuelve a la vista en el proyecto internetCAC.<br><b>"Error en el sistema.<br/>"</b>*/
	public static final String COMIENZO_MENSAJE_ERROR_JSPCONTROLLER = "Error en el sistema.<br/>";

	/**Cadena de texto utilizada para indicar que un servicio devuelve un valor.<br><b> devuelve\n</b>*/
	public static final String CONECTOR_VALOR_DEVUELTO_POR_SERVICIO = " devuelve\n";

	/**Codigo que hay que introducir en la tabla Recinto para poder identificar inequivocamente el Hemisferic.*/
	public static final String CODIGO_RECINTO_HEMISFERIC = "HEMIS";

	/**Espacio en blanco <b>" "</b>*/
	public static final String ESPACIO_BLANCO = " ";
	/**2 espacios en blanco <b>"  "</b>*/
	public static final String DOBLE_ESPACIO_BLANCO = "  ";
	/**Caracter que representa al tabulador<br><b>	</b>*/
	public static final String TAB = "	";
	/**Caracter que representa a la almohadilla<br><b>#</b>*/
	public static final String ALMOHADILLA = "#";
	/**Caracter que representa una coma.<br><b>","</b>*/
	public static final String COMA = ",";
	/**Caracter que representa un punto.<br><b>"."</b>*/
	public static final String PUNTO = ".";
	/**Caracter que representa un punto y coma.<br><b>";"</b>*/
	public static final String PUNTOYCOMA = ";";
	/**Cadena de texto que representa el símbolo dos puntos.<b>":"</b>*/
	public static final String DOSPUNTOS = ":";
	/**Caracter que representa una comilla.<br><b>"'"</b>*/
	public static final String COMILLA = "'";
	/**Caracter que representa una apertura de paréntesis.<br><b>"("</b>*/
	public static final String APERTURA_PARENTESIS = "(";
	/**Caracter que representa un cierre de paréntesis.<br><b>")"</b>*/
	public static final String CIERRE_PARENTESIS = ")";
	/**Caracter que representa una apertura de corchete.<br><b>"["</b>*/
	public static final String APERTURA_CORCHETE = "[";
	/**Caracter que representa un cierre de corchete.<br><b>"]"</b>*/
	public static final String CIERRE_CORCHETE = "]";
	/**Caracter que representa la barra utilizada en direcciones url habitualmente.<br><b>"/"</b>*/
	public static final String BARRA = "/";
	/**Caracter que representa el caracter de porcentaje.<br><b>"%"</b>*/
	public static final String TANTO_POR_CIEN = "%";
	/**Caracter que indica el comienzo de la parte decimal en la clase {@link NumeroDecimal} a la hora de formatear para mostrar por pantalla o escribir en fichero.<br><b>","</b>*/
	public static final String MARCA_DECIMAL_COMA = COMA;
	/**Caracter que indica el comienzo de la parte decimal en la clase {@link NumeroDecimal} a la hora de formatear para mostrar por pantalla o escribir en fichero.<br><b>"."</b>*/
	public static final String MARCA_DECIMAL_PUNTO = PUNTO;
	/**String de caracteres que pueden indicar el comienzo de la parte decimal en la clase {@link NumeroDecimal} a la hora de formatear para mostrar por pantalla o escribir en fichero.<br><b>".,"</b>*/
	public static final String MARCAS_DECIMALES = PUNTO + COMA;
	/**Caracter que representa el signo menos o la operacion resta.<br><b>"-"</b>*/
	public static final String SIGNOMENOS = "-";
	/**Caracter que representa el signo mas o la operacion suma.<br><b>"+"</b>*/
	public static final String SIGNOMAS = "+";
	/**Caracter utilizado para separar los distintos valores que pueden tomar los Atributoscontenido para generar el informe para el ICAA.<br><b>SEPARADOR_VALORES_ATRIBUTOSCONTENIDO_ICAA</b>*/
	public static final String SEPARADOR_VALORES_ATRIBUTOSCONTENIDO_ICAA = "SEPARADOR_VALORES_ATRIBUTOSCONTENIDO_ICAA";
	/**Cadena de texto utilizada para ser intercalada entre la dirección y el número de la dirección en aquellas direcciones pertenecientes a Clientes del Pase Club.<br><b>", "</b>*/
	public static final String SEPARADOR_VIA_NUMVIA = COMA + ESPACIO_BLANCO;
	/**Cadena de texto utilizada para ser intercalada entre el número de la dirección y el complemento de la misma, en aquellas direcciones pertenecientes a Clientes del Pase Club.<br><b>". "</b>*/
	public static final String SEPARADOR_NUMVIA_COMPLEMENTOVIA = PUNTO + ESPACIO_BLANCO;

	/**Constante utilizada por defecto en la clase {@link NumeroDecimal}<br><b>2</b><br>*/
	public static final int POSICIONES_DECIMALES_POR_DEFECTO = DOS;
	/**Constante utilizada por defecto en la clase {@link NumeroDecimal}<br><b>13</b><br>*/
	public static final int POSICIONES_ENTERAS_POR_DEFECTO = 13;
	/**Numero máximo de conexiones permitidas para cada uno de los huéspedes asociados al cliente Http del {@link InvocadorServiciosHttp}<br><b>40</b><br>*/
	public static final int NUM_MAX_CONEXIONES_POR_HUESPED = 40;
	/**Numero máximo total de conexiones permitidas para el cliente Http del {@link InvocadorServiciosHttp}<br><b>60</b><br>*/
	public static final int NUM_MAX_TOTAL_CONEXIONES = 60;

	/**Constante para asignar en los contructores por defecto y que nos servira para identificar los Objetos creados por Hibernate y estan vacios.<br>Valor por defecto <b>0</b> {@link Constantes.CERO}*/
	public static final Integer ID_NO_ASIGNADO = CERO;
	/**Valor del importe de los Precios de los Productos Simples para la entidad {@link Precio} puesto que habran de obtener el Precio de los Partidos.<br><b>-1F</b>*/
	public static final Float IMPORTE_NO_ASIGNADO = -1F;

	/**Constante para las operaciones Globales.<br><b>G</b>*/
	public static final String GLOBAL = CADENA_G;
	/**Tipo de Registro para los Registros de tipo Cabecera.<br><b>I</b>*/
	public static final String CABECERA = CADENA_I;
	/**Tipo de Registro para los Registros de tipo AforosClub combinados.<br><b>D</b>*/
	public static final String AFOROCLUB_COMBINADO = CADENA_D;
	/**Tipo de Registro para los Registros de tipo {@link AforoClub}.<br><b>E</b>*/
	public static final String AFOROCLUB = CADENA_E;
	/**Tipo de Registro para los Registros de tipo Competicion.<br><b>C</b>*/
	public static final String COMPETICON = CADENA_C;
	/**Tipo de Registro para los Registros de tipo Grupo de Zonas.<br><b>G</b>*/
	public static final String GRUPOZONAS = CADENA_G;
	/**Tipo de Registro para los Registros de tipo Zona.<br><b>B</b>*/
	public static final String ZONA = CADENA_B;
	/**Tipo de Registro para los Registros de tipo Partido.<br><b>S</b>*/
	public static final String PARTIDO = CADENA_S;
	/**Tipo de Registro para los Registros de tipo Precios por Partido.<br><b>J</b>*/
	public static final String PRECIOPARTIDO = CADENA_J;
	/**Tipo de Registro para los Registros de tipo Precio<br><b>H</b>*/
	public static final String PRECIO = CADENA_H;

	/**Caracter separador en rutas para URL's<br><b>"/"</b>*/
	public static final String SEPARADOR_URLs = BARRA;
	/**Caractet utilizado para especificar el puerto dentro de una URL.<br><b>":"</b>*/
	public static final String INDICADOR_PUERTO_EN_URL = DOSPUNTOS;
	/**Cadena de caracteres que especifica el protocolo a utilizar en una direccion URL, en este caso el protocolo HTTP.<br><b>"http://"</b>*/
	public static final String MARCA_PROTOCOLO_HTTP_EN_URL = "http://";
	/**Caracter separador de campos en principio el caracter PIPE "|"<hr><b>"|"</b>*/
	public static final String PIPE = "|";
	/**Caracter de salto de linea en principio "\n"*/
	public static final String SALTO_LINEA = "\n";
	/**Caracter de salto de linea usado dentro del lenguaje HTML.<br><b>"<BR>"</b>*/
	public static final String SALTO_LINEA_HTML = "<BR>";
	/**Caracter de salto de linea usado dentro del lenguaje HTML seguido de un caracter de salto de linea.*/
	public static final String SALTO_LINEA_HTML_MAS_SALTO_LINEA = SALTO_LINEA_HTML + SALTO_LINEA;
	/**Cadena utilizada para los campos nulos o que no interesa pasarlos con valor.*/
	public static final String CADENA_VACIA = "";
	/**Cadena utilizada para representar la contrabarra escapada.<br><b>"\\"</b>*/
	public static final String DOBLE_CONTRABARRA = "\\";
	/**Cadena utilizada para representar la contrabarra escapada "escapadnola".<br><b>"\\\\"</b>*/
	public static final String DOBLE_CONTRABARRA_ESCAPADA = "\\\\";
	/**Caracteres que serán descartados tanto en el nif/dni que se pasa desde la pantalla de busqueda del punto de recogida como del valor obtenido de la venta que se obtiene de la BD.<br><b>" -_"</b>*/
	public static final String CARACTERES_DESCARTABLES_NIF = " -_";
	/**Constante de cadena de texto que representa el valor 0<br><b>"0"</b>.*/
	public static final String CADENA_0 = "0";
	/**Constante de cadena de texto que representa el valor 1<br><b>"1"</b>.*/
	public static final String CADENA_1 = "1";
	/**Constante de cadena de texto que representa el valor 2<br><b>"2"</b>.*/
	public static final String CADENA_2 = "2";
	/**Constante de cadena de texto que representa el valor 3<br><b>"3"</b>.*/
	public static final String CADENA_3 = "3";
	/**Constante de cadena de texto que representa el valor 4<br><b>"4"</b>.*/
	public static final String CADENA_4 = "4";
	/**Constante de cadena de texto que representa el valor 5<br><b>"5"</b>.*/
	public static final String CADENA_5 = "5";
	/**Constante de cadena de texto que representa el valor 50<br><b>"50"</b>.*/
	public static final String CADENA_50 = "50";
	/**Constante de cadena de texto que representa el valor 6<br><b>"6"</b>.*/
	public static final String CADENA_6 = "6";
	/**Constante de cadena de texto que representa el valor 7<br><b>"7"</b>.*/
	public static final String CADENA_7 = "7";
	/**Constante de cadena de texto que representa el valor 8<br><b>"8"</b>.*/
	public static final String CADENA_8 = "8";
	/**Constante de tipo cadena que representa el valor 9<br><b>"9"</b>.*/
	public static final String CADENA_9 = "9";
	/**Constante de tipo cadena que representa el valor 10<br><b>"10"</b>.*/
	public static final String CADENA_10 = "10";
	/**Constante de tipo cadena que representa el valor 11<br><b>"11"</b>.*/
	public static final String CADENA_11 = "11";
	/**Constante de tipo cadena que representa el valor 12<br><b>"12"</b>.*/
	public static final String CADENA_12 = "12";
	/**Constante de tipo cadena que representa el valor 100<br><b>"100"</b>.*/
	public static final String CADENA_100 = "100";
	/**Constante de tipo cadena que representa el valor 101<br><b>"101"</b>.*/
	public static final String CADENA_101 = "101";
	/**Constante de tipo cadena que representa el valor 105<br><b>"105"</b>.*/
	public static final String CADENA_105 = "105";
	/**Constante de tipo cadena que representa el valor 106<br><b>"106"</b>.*/
	public static final String CADENA_106 = "106";
	/**Constante de tipo cadena que representa el valor 107<br><b>"107"</b>.*/
	public static final String CADENA_107 = "107";
	/**Constante de tipo cadena que representa el valor 401<br><b>"401"</b>.*/
	public static final String CADENA_401 = "401";
	/**Constante de tipo cadena que representa el valor 403<br><b>"403"</b>.*/
	public static final String CADENA_403 = "403";
	/**Constante de tipo cadena que representa el valor 00<br><b>"00"</b>.*/
	public static final String CADENA_00 = "00";
	/**Constante de tipo cadena que representa el valor 000<br><b>"000"</b>.*/
	public static final String CADENA_000 = "000";
	/**Constante de tipo cadena que representa el valor 000<br><b>"001"</b>.*/
	public static final String CADENA_001 = "001";
	/**Constante de tipo cadena que representa el valor 0000<br><b>"0000"</b>.*/
	public static final String CADENA_0000 = "0000";
	/**Constante de tipo cadena que representa el valor 99<br><b>"99"</b>.*/
	public static final String CADENA_99 = "99";
	/**Constante de tipo cadena que representa el valor 999<br><b>"999"</b>.*/
	public static final String CADENA_999 = "999";
	/**Constante de tipo cadena que representa el valor 99999<br><b>"99999"</b>.*/
	public static final String CADENA_99999 = "99999";

	/**Cadena de texto que representa el incio de la secuencia de los clientes para el club ciutat 0000000001<br><b>"0000000001"</b>*/
	public static final String INICIO_SECUENCIA_CLIENTES_CLUB = "0000000001";

	/**Constante de tipo cadena que representa el valor decimal 100 con una precisión de 2 decimales.<br><b>"100.00"</b>*/
	public static final String CADENA_CIEN_2DECIMALES_PUNTO = "100.00";
	/**Constante de tipo cadena que representa el valor decimal 0.0 con una precisión de 1 decimal.<br><b>"0.0"</b>*/
	public static final String CADENA_0_1DECIMAL_PUNTO = "0.0";

	/**Cadena de texto que representa el acronimo utilizado en el JSPController de internetCAC para representar al idioma castellano.<br><b>CAS</b>*/
	public static final String ACRONIMO_IDIOMA_CASTELLANO = "CAS";
	/**Cadena de texto que representa el acronimo utilizado en el JSPController de internetCAC para representar al idioma valenciano.<br><b>VAL</b>*/
	public static final String ACRONIMO_IDIOMA_VALENCIANO = "VAL";
	/**Cadena de texto que representa el acronimo utilizado en el JSPController de internetCAC para representar al idioma inglés.<br><b>ING</b>*/
	public static final String ACRONIMO_IDIOMA_INGLES = "ING";

	/**Version de la aplicacion que va a reconoce nuestros ficheros de replica.<br><b>1.1.1</b>*/
	public static final String VERSION = "1.1.1";

	/**Constante que va a indicar el modo de funcionamiento de generacion de ficheros<br>Inicialmente lo voy a poner en T - Test<br>Una vez realizadas las pruebas pertinentes lo fijaremos a P - Publico<br>Esta constante no se va a persistir*/
	public static final String TEST = CADENA_T;
	/**Constante que va a indicar el modo de funcionamiento de generacion de ficheros en modo P - Publico<br>Esta constante no se va a persistir*/
	public static final String PUBLICO = CADENA_P;

	/**Codigo de cliente para con la Caixa.<br><b>35</b><br>Longitud 2*/
	public static final String CODCLU = "35";

	/**Longitud maxima reserva para representar un COMPET en el Fichero de Replica.<br>valor 3 (int).*/
	public static final int LONGITUDMAXCOMPET = 3;
	/**Longitud maxima reserva para representar un CODTEM en el Fichero de Replica.<br>valor 1 (int).*/
	public static final int LONGITUDMAXCODTEM = 1;
	/**Longitud maxima reserva para representar un CODAFO en el Fichero de Replica.<br>valor 2 (int).*/
	public static final int LONGITUDMAXCODAFO = 2; 
	/**Longitud maxima reserva para representar un CODBLO en el Fichero de Replica.<br>valor 2 (int).*/
	public static final int LONGITUDMAXCODBLO = 2; 
	/**Longitud maxima reserva para representar un CODPAR en el Fichero de Replica.<br>valor 3 (int).*/
	public static final int LONGITUDMAXCODPAR = 3; 
	/**Longitud maxima reserva para representar un TIPPRE en el Fichero de Replica.<br>valor 3 (int).*/
	public static final int LONGITUDMAXTIPPRE = 3; 
	/**Longitud maxima reserva para representar un CODAFO en el Fichero de Replica.<br>valor 4 (int).*/
	public static final int LONGITUDMAXCODZON = 4; 
	/**Longitud del campo NUMCOMB perteneciente a los registros de tipo D, que no estaba documentado en las especificaciones de la mensajer�a.<br><b>2</b>*/
	public static final int LONGITUD_CAMPO_NUMCOMB = DOS;

	/**Valor decimal maximo que se puede representar mediante el identificador CODPAR.*/
	public static final long MAXCODPAR = ElementoBaseN.obtenerMaximoValorDecimalRepresentable( LONGITUDMAXCODPAR );
	/**Valor decimal maximo que se puede representar mediante el identificador CODTEM.*/
	public static final long MAXCODTEM = ElementoBaseN.obtenerMaximoValorDecimalRepresentable( LONGITUDMAXCODTEM );
	/**Valor decimal maximo que se puede representar mediante el identificador CODAFO.*/
	public static final long MAXCODAFO = ElementoBaseN.obtenerMaximoValorDecimalRepresentable( LONGITUDMAXCODAFO );
	/**Valor decimal maximo que se puede representar mediante el identificador TIPPRE.*/
	public static final long MAXTIPPRE = ElementoBaseN.obtenerMaximoValorDecimalRepresentable( LONGITUDMAXTIPPRE );
	/**Valor decimal maximo que se puede representar mediante el identificador CODZON.*/
	public static final long MAXCODZON = ElementoBaseN.obtenerMaximoValorDecimalRepresentable( LONGITUDMAXCODZON );
	/**Valor decimal maximo que se puede representar mediante el identificador CODBLO.*/
	public static final long MAXCODBLO = ElementoBaseN.obtenerMaximoValorDecimalRepresentable( LONGITUDMAXCODBLO );
	/**Valor decimal maximo que se puede representar mediante el identificador COMPET.*/
	public static final long MAXCOMPET = ElementoBaseN.obtenerMaximoValorDecimalRepresentable( LONGITUDMAXCOMPET );
	
	/**Longitud maxima para las cadenas descriptivas como por ejemplo la descripcion del Aforo.<br>valor 50 (int)*/
	public static final int LONGITUDMAXDESCLU = 50;
	/**Longitud maxima para las cadenas descriptivas como por ejemplo la descripcion del Bloque de Zonas.<br>valor 50 (int)*/
	public static final int LONGITUDMAXDESBLO = 50;
	/**Longitud maxima para las cadenas descriptivas como por ejemplo la descripcion de la Zona.<br>valor 50 (int)*/
	public static final int LONGITUDMAXDESZON = 50;
	/**Longitud maxima para el nombre del partido.<br>valor 50 (int)*/
	public static final int LONGITUDMAXNOMPAR = 50;
	/**Longitud maxima para las cadenas que describen el Objeto {@link Precio}.<br>valor 30 (int)*/
	public static final int LONGITUDMAXDESPRE = 30;
	/**Longitud maxima para las cadenas que describen el numero de secuencia.<br>valor 3 (int)*/
	public static final int LONGITUDMAXSEQ = 3;
	
	/**Direccion IP de la maquina en la que alamcenaremos una copia del fichero de replica a enviar a La Caixa.*/
	public static final String MAQUINA_ALMACEN = "127.0.0.1";
	
	/**Direccion IP de la maquina de la Caixa a la que enviaremos el fichero de Replica por FTP.*/
	public static final String FTP_LACAIXA = "7.2.1.47";
	
	/**Direccian IP publica en la que vamos a estar esperando recibir los mensajes<br>Inicialmente se escogio la IP del equipo NE1363 (API) pero se va a dejar en la IP <b>192.168.130.3</b> que La Caixa tiene mapeada para llegar hasta EDISON(192.168.1.3)*/
	public static final String IPSERVIDOR = "192.168.130.3";
	
	/**Nombre del canal al que van a ir asociadas todas las operaciones y consultas.<br>Valor <b>"ServiCaixa"</b>*/
	public static final String NOMBRECANAL = "ServiCaixa OnLine";
	
	/**Descripcion de los Partidos que se asignara a los Productos combinados.*/
	public static final String DESC_PARTIDO_COMBINADO = "Partido para los Productos combinados.";
	/**Descripcion que asignaremos a los {@link Producto} que se den de baja en el sistema.*/
	public static final String DESC_AFOROCLUB_ANULADO = "Producto anulado.";
	/**Descripcion que asignaremos a los {@link Precio} que se den de baja en el sistema.*/
	public static final String DESC_PRECIO_ANULADO = "Precio anulado.";
	/**Descripcion que asignaremos a los registros Zona que hayan sido anulados en el sistema.*/
	public static final String DESC_ZONA_ANULADA = "Zona anulada.";
	/**Descripcion de los BLoques de Zona que se dan de baja.*/
	public static final String DESC_ZONA_DADODEBAJA = "Zona dada de baja.";
	/**Descripcion de los registros Zona que se asignara a los Productos combinados.*/
	public static final String DESC_ZONA_COMBINADA = "Zona para los Productos combinados.";
	/**Codblo formateado para los Grupos de Zona anulados.<br><b>00</b>*/
	public static final String CODBLO_ANULADO_FORMATEADO = "00";
	/**Descripcion de los BLoques de Zona que se dan de baja.*/
	public static final String DESC_GRUPOZONAS_DADODEBAJA = "Grupo de zonas dado de baja.";
	/**Descripcion de los Bloques de Zona que se asignara a los Productos combinados.*/
	public static final String DESC_GRUPOZONAS_COMBINADO = "Los Productos combinados no tienen Tramos horarios";
	/**Descripcion a utilizar cuando en el caso de encontrar un {@link AforoClub} sin {@link Producto} asociado lo que a priori no es posible.*/
	public static final String DESC_AFOROCLUB_SIN_PRODUCTO = "Producto no asignado.";

	/**Descripcion que incluiremos en el registro de tipo C, cuando este sea anulado.*/
	public static final String DESC_COMPETICION_COMUN = "Competicion ";
	/**Descripcion que incluiremos en el registro de tipo C, cuando este sea anulado.*/
	public static final String DESC_COMPETICION_ANULADA = new StringBuffer( DESC_COMPETICION_COMUN ).append( " Anulada." ).toString();
	/**Numero de posiciones que puede contener la descripcion de la competicion sin tener en cuenta la parte comun.*/
	public static final int LONGITUD_DISPONIBLE_DESC_COMPETICION_SIN_PARTE_COMUN = 38;

	/**Nombre con el que se va a escribir el fichero en disco*/
	public static final String nombreFichero = "Fichero sin nombrar";

	/**Hora máxima en la que se puede establecer el cierre de tornos de una sesión sin renunciar al WinPlus, de forma que se siga podiendo entrar en la sesión modiifcada.*/
	public static final String HORA_CIERRE_TORNOS_MAXIMA = "23:59";
	/**Hora formateada para la Caixa para el caso de que el Recinto asociado a un producto no tenga hora de apertura asignada. Se asignara la cadena vacia.<br>Valor <b>""</b>*/
	public static final String HORA_LA_CAIXA_NO_ASIGNADA = CADENA_VACIA;
	/**Se asignara a los {@link GrupoZonas} anulados y combinados.<br>Valor <b>"00:00"</b>*/
	public static final String HORA_LA_CAIXA_GRUPOZONAS_ANULADO_COMBINADO = "00:00";
	/**Descripcion a aplicar a los contenidos de un {@link GrupoZonas} anulado.<br><b>{@link String}</b><br><b>"Contenido anulado"</b>*/
	public static final String DESC_CONTENIDO_GRUPOZONAS_ANULADO = "Contenido anulado";
	/**Descripcion a aplicar a los contenidos de un {@link GrupoZonas} combinado.<br><b>{@link String}</b><br><b>"Contenido combinado"</b>*/
	public static final String DESC_CONTENIDO_GRUPOZONAS_COMBINADO = "Contenido combinado";

	/**Formato generico para las fechas expresadas como Anyo mes y dia AAAAMMDD.<br>yyyyMMdd*/
	public static final SimpleDateFormat formatoFechaLaCaixa = new SimpleDateFormat("yyyyMMdd");
	/**Formato generico para las Horas expresadas como horas minutos y segundos <br>HHmmss.*/
	public static final SimpleDateFormat formatoHoraLargoLaCaixa = new SimpleDateFormat("HHmmss");
	/**Formato generico para las Horas expresadas como horas minutos <br>HHmm.*/
	public static final SimpleDateFormat formatoHoraCortoLaCaixa = new SimpleDateFormat("HHmm");

	/**Formato generico para las Horas de Inicio/Fin obtenidas en los xml de los servicios.<br>HH:mm*/
	public static final SimpleDateFormat formatoHoraXMLCorto = new SimpleDateFormat("HH:mm");
	/**Formato generico para las Horas de Inicio/Fin obtenidas en los xml de los servicios.<br>HH:mm:ss*/
	public static final SimpleDateFormat formatoHoraXMLLargo = new SimpleDateFormat("HH:mm:ss");

	/**Formato para el nombre de fichero <br>yyyy-MMM-dd HH-mm-ss*/
	public static final SimpleDateFormat formatoFechaHoraNombreFichero = new SimpleDateFormat("yyyy-MMM-dd HH-mm-ss");

	/**Formato para leer fechas a partir de los servicios <br>dd/MM/yyyy.*/
	public static final SimpleDateFormat formatoFechaXMLCorto = new SimpleDateFormat("dd/MM/yyyy");
	/**Formato para leer fechas a partir de los servicios <br>dd/MM/yy.*/
	public static final SimpleDateFormat formatoFechaXMLSuperCorto = new SimpleDateFormat("dd/MM/yy");
	/**Formato para leer fechas a partir de los servicios <br>dd/MM/yyyy-HH:mm:ss.*/
	public static final SimpleDateFormat formatoFechaXMLLargo = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss");
	/**Formato para leer fechas a partir de los servicios <br>dd/MM/yyyy HH:mm:ss.*/
	public static final SimpleDateFormat formatoFechaXMLLargoSinGuion = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	/**Formato para leer fechas a partir de los servicios <br>yyyy-MM-dd.*/
	public static final SimpleDateFormat formatoFechaXMLCortoInverso = new SimpleDateFormat("yyyy-MM-dd");
	/**Formato para leer fechas a partir de los servicios <br>yyyy-MM-dd-HH:mm:ss.*/
	public static final SimpleDateFormat formatoFechaXMLLargoInverso = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");

	/**Formato utilizado por Indra en los POJOs Bono, Sesion y en la clase es.di.cac.ticketing.services.controllers.printer.util.UtilFormato<br><b>dd-MMM-yyyy</b>*/
	public static final SimpleDateFormat formatoIndra = new SimpleDateFormat("dd-MMM-yyyy");
	/**Formato utilizado por Indra en los POJOs Bono, Sesion y en la clase es.di.cac.ticketing.services.controllers.printer.util.UtilFormato<br><b>dd-MMM-yyyy HH:mm</b>*/
	public static final SimpleDateFormat formatoIndraLargo = new SimpleDateFormat("dd-MMM-yyyy HH:mm");

	/**Formato para mostrar en las descripciones de Productos, Sesiones, ... mas amigable para el Cliente que ha de ver la informacion. <br>dd/MM/yyyy*/
	public static final SimpleDateFormat formatoFechaDescripcionesLaCaixa = formatoFechaXMLCorto;

	/**Formato para utilizar en el módulo de gestión de Pases Club Ciutat.</b></b><br><b>"dd/MM/yyyy-hh:mm:ss"</b>*/
	public static final SimpleDateFormat formatoFechaClub = new SimpleDateFormat( "dd/MM/yyyy-hh:mm:ss" );

	/**Formateador de fechas textual que indica el dia de la semana, el mes y el año para ser leido por un Humano.<br><b>EEEE d MMMMM yyyy</b>*/
	public static final SimpleDateFormat formatoFechaDescriptivoCorto = new SimpleDateFormat( "EEEE d MMMMM yyyy" );

	/**Formato para recuperar las fechas de Impresion de las entradas que envian los servidores de impresión desde las Taquillas.</b></b><br><b>"yyyyMMdd HHmmss"</b>*/
	public static final SimpleDateFormat formatoFechaServidoresImpresion = new SimpleDateFormat( "yyyyMMdd HHmmss" );

	/**Numero de milisegundos que tiene un dia = 1000ms * 60s * 60m * 24h = 86400000 */
	public static final long MS_POR_DIA = 86400000;
	
	/**Numero maximo de entradas que se pueden comprar para una sesion por un mismo cliente.<br><b>4</b>*/
	public static final String NUMVENTPA = "04";
	/**Numero maximo de entradas que se pueden comprar para ayegados en una sesion.<br><b>4</b>*/
	public static final String NUMACOPA = "04";
	
	/**Cadena de texto para representar el valor "verdadero" en los registros de La Caixa.<br><b>"S"</b>*/
	public static final String LKX_STR_CIERTO = CADENA_S;
	/**Cadena de texto para representar el valor "falso" en los registros de La Caixa.<br><b>"N"</b>*/
	public static final String LKX_STR_FALSO = CADENA_N;
	/**Cadena de texto que representa el Codigo del Sector al que pertenece la Zona.<br><b>"A"</b>*/
	public static final String SECZON = "A";
	/**Marca que se va a utilizar cuando una Zona sea anulada o borrada y se quiere desvincular de los Precios a los que estaba asociado.<br><b>"0"</b></br>*/
	public static final String SIN_CODIGOS_PRECIO = CADENA_0;
	
	/**El codigo que vamos a emplear en todos los registros que necesiten el Campo Codigo.*/
	public static final String CODIGO_COMPETICION = "001";
	/**Despues de quitar las Competiciones de todo el proceso, ahora me las vuelven a pedir.*/
	public static final String REGISTRO_COMPETICION_CAC = new StringBuffer( COMPETICON ).append( PIPE ).append( CODCLU )
		.append( PIPE ).append( GLOBAL ).append( PIPE ).toString();
	
	/**Valor para el campo tipo en los registros RegistroICAA0<br>Integer con valor <b>0</b>*/
	public static final String TIPO_REGISTROICAA0 = "TIPO_REGISTROICAA0";
	/**Valor para el campo tipo en los registros RegistroICAA0<br>Integer con valor <b>1</b>*/
	public static final String TIPO_REGISTROICAA1 = "TIPO_REGISTROICAA1";
	/**Valor para el campo tipo en los registros RegistroICAA0<br>Integer con valor <b>2</b>*/
	public static final String TIPO_REGISTROICAA2 = "TIPO_REGISTROICAA2";
	/**Valor para el campo tipo en los registros RegistroICAA0<br>Integer con valor <b>3</b>*/
	public static final String TIPO_REGISTROICAA3 = "TIPO_REGISTROICAA3";
	/**Valor para el campo tipo en los registros RegistroICAA0<br>Integer con valor <b>4</b>*/
	public static final String TIPO_REGISTROICAA4 = "TIPO_REGISTROICAA4";
	/**Valor para el campo tipo en los registros RegistroICAA0<br>Integer con valor <b>5</b>*/
	public static final String TIPO_REGISTROICAA5 = "TIPO_REGISTROICAA5";
	
	/**Longitud en caracteres de los registros de tipo {@link RegistroICAA0}<br>Valor int=56.*/
	public static final String LONG_REGISTROSICAA_TIPO0 = "LONG_REGISTROSICAA_TIPO0";
	/**Longitud en caracteres de los registros de tipo {@link RegistroICAA1}<br>Valor int=43.*/
	public static final String LONG_REGISTROSICAA_TIPO1 = "LONG_REGISTROSICAA_TIPO1";
	/**Longitud en caracteres de los registros de tipo {@link RegistroICAA2}<br>Valor int=41.*/
	public static final String LONG_REGISTROSICAA_TIPO2 = "LONG_REGISTROSICAA_TIPO2";
	/**Longitud en caracteres de la combinacion de los registros de tipo {@link RegistroICAA3} en una sola cadena, pues en nuestro caso<br>Valor int=28+144.*/
	public static final String LONG_REGISTROSICAA_TIPO3 = "LONG_REGISTROSICAA_TIPO3";
	/**Longitud en caracteres de los registros de tipo {@link RegistroICAA4}<br>Valor int=144.*/
	public static final String LONG_REGISTROSICAA_TIPO4 = "LONG_REGISTROSICAA_TIPO4";
	/**Longitud en caracteres de los registros de tipo {@link RegistroICAA5}<br>Valor int=21.*/
	public static final String LONG_REGISTROSICAA_TIPO5 = "LONG_REGISTROSICAA_TIPO5";
	
	/**Longitud del campo Codigo de Sala de Sala del Registro Tipo1, que va a ser unico en nuestro caso, L'HEMISFÈRIC va a ser el unico recinto en el que se realizan proyecciones.<br>Valor <b>12</b>*/
	public static final String LONGITUD_CODIGOSALA_ICAA = "LONGITUD_CODIGOSALA_ICAA";
	/**Longitud del campo Codigo de Sala de Sala del Registro Tipo1, que va a ser unico en nuestro caso, L'HEMISFÈRIC va a ser el unico recinto en el que se realizan proyecciones.<br>Valor <b>30</b>*/
	public static final String LONGITUD_NOMBRESALA_ICAA = "LONGITUD_NOMBRESALA_ICAA";
	/**Longitud asignada a los campo de fecha en formato juliano para el Informe de la ICAA.<br>Valor <b>3</b>*/
	public static final String LONGITUD_CAMPOSFECHA_ICAA = "LONGITUD_CAMPOSFECHA_ICAA";
	/**Longitud asignada al campo Numero de Peliculas por Sesion del Informe para la ICAA.<br>Valor <b>2</b>*/
	public static final String LONGITUD_CAMPONUMPELICULASSESION_ICAA = "LONGITUD_CAMPONUMPELICULASSESION_ICAA";
	/**Longitud asignada al campo Numero de Linea Totales para el Informe de la ICAA.<br>Valor <b>11</b>*/
	public static final String LONGITUD_CAMPONUMLINEASTOTALES_ICAA = "LONGITUD_CAMPONUMLINEASTOTALES_ICAA";
	/**Longitud asignada al campo Numero Total de Sesiones para el Informe para la ICAA.<br>Valor <b>11</b>*/
	public static final String LONGITUD_CAMPONUMTOTALSESIONES_ICAA = "LONGITUD_CAMPONUMTOTALSESIONES_ICAA";
	/**Longitud asignada al campo Numero Total de Espectadores para el Informe para la ICAA.<br>Valor <b>11</b>*/
	public static final String LONGITUD_CAMPONUMTOTALESPECTADORES_ICAA = "LONGITUD_CAMPONUMTOTALESPECTADORES_ICAA";
	/**Longitud asignada al campo Recaudacion Total para el Informe para la ICAA.<br>Valor <b>11</b>*/
	public static final String LONGITUD_CAMPORECAUDACIONTOTAL_ICAA = "LONGITUD_CAMPORECAUDACIONTOTAL_ICAA";
	/**Longitud asignada al campo Numero Total de Espectadores de la Sesion, para el Informe para la ICAA.<br>Valor <b>5</b>*/
	public static final String LONGITUD_CAMPONUMTOTALESPECTADORESSESION_ICAA = "LONGITUD_CAMPONUMTOTALESPECTADORESSESION_ICAA";
	/**Longitud asignada al campo Recaudacion de la Sesion, para el Informe para la ICAA.<br>Valor <b>8</b>*/
	public static final String LONGITUD_CAMPORECAUDACIONSESION_ICAA = "LONGITUD_CAMPORECAUDACIONSESION_ICAA";
	/**Longitud asignada al campo "Codigo Interno Pelicula", para el Informe para la ICAA.<br>Valor <b>5</b>*/
	public static final String LONGITUD_CAMPOCODIGOINTERNOPELICULA_ICAA = "LONGITUD_CAMPOCODIGOINTERNOPELICULA_ICAA";
	/**Longitud asignada al campo "Codigo Expediente Pelicula", para el Informe para la ICAA.<br>Valor <b>12</b>*/
	public static final String LONGITUD_CAMPOCODIGOEXPEDIENTEPELICULA_ICAA = "LONGITUD_CAMPOCODIGOEXPEDIENTEPELICULA_ICAA";
	/**Longitud asignada al campo "Titulo Pelicula", para el Informe para la ICAA.<br>Valor <b>50</b>*/
	public static final String LONGITUD_CAMPOTITULOPELICULA_ICAA = "LONGITUD_CAMPOTITULOPELICULA_ICAA";
	/**Longitud asignada al campo "Codigo Distribuidora", para el Informe para la ICAA.<br>Valor <b>12</b>*/
	public static final String LONGITUD_CAMPOCODIGODISTRIBUIDORA_ICAA = "LONGITUD_CAMPOCODIGODISTRIBUIDORA_ICAA";
	/**Longitud asignada al campo "Nombre Distribuidora", para el Informe para la ICAA.<br>Valor <b>50</b>*/
	public static final String LONGITUD_CAMPONOMBREDISTRIBUIDORA_ICAA = "LONGITUD_CAMPONOMBREDISTRIBUIDORA_ICAA";
	/**Longitud asignada al campo "Codigo Idioma Pelicula", para el Informe para la ICAA.<br>Valor <b>1</b>*/
	public static final String LONGITUD_CAMPOCODIGOIDIOMAPELICULA_ICAA = "LONGITUD_CAMPOCODIGOIDIOMAPELICULA_ICAA";	
	/**Longitud asignada al campo "Codigo Subtitulos Pelicula", para el Informe para la ICAA.<br>Valor <b>1</b>*/
	public static final String LONGITUD_CAMPOCODIGOSUBTITULOSPELICULA_ICAA = "LONGITUD_CAMPOCODIGOSUBTITULOSPELICULA_ICAA";
	/**Nombre de la clave de la tabla de configuracion para obtener la Longitud asignada al campo "Codigo Sala", para el Informe para la ICAA.<br>Valor <b>LONGITUD_CAMPOCODIGOSALA_ICAA</b>*/
	public static final String LONGITUD_CAMPOCODIGOSALA_ICAA = "LONGITUD_CAMPOCODIGOSALA_ICAA";
	/**Nombre de la clave de la tabla de configuracion para obtener la Longitud asignada al campo "Nombre Sala", para el Informe para la ICAA.<br>Valor <b>LONGITUD_CAMPONOMBRESALA_ICAA</b>*/
	public static final String LONGITUD_CAMPONOMBRESALA_ICAA = "LONGITUD_CAMPONOMBRESALA_ICAA";
	/**Longitud asignada al campo "Numero de sesiones programadas en la fecha de sesion", para el Informe para la ICAA.<br>Valor <b>LONGITUD_CAMPONUMSESIONESPROGRAMADASENFECHASESION_ICAA</b>*/
	public static final String LONGITUD_CAMPONUMSESIONESPROGRAMADASENFECHASESION_ICAA = "LONGITUD_CAMPONUMSESIONESPROGRAMADASENFECHASESION_ICAA";
	/**<br><b>LONGITUD_CAMPOCODIGOVERSIONORIGINALPELICULA_ICAA</b>*/
	public static final String LONGITUD_CAMPOCODIGOVERSIONORIGINALPELICULA_ICAA = "LONGITUD_CAMPOCODIGOVERSIONORIGINALPELICULA_ICAA";
	/**<br><b>LONGITUD_CAMPOFORMATOPROYECCIONPELICULA_ICAA</b>*/
	public static final String LONGITUD_CAMPOFORMATOPROYECCIONPELICULA_ICAA = "LONGITUD_CAMPOFORMATOPROYECCIONPELICULA_ICAA";

	/**Numero de posiciones a descontar a la longitud del campo dentro de un registro del Informe para la ICAA para obtener a la longitud de la parte entera en un valor decimal, incluye la MARCA_DECIMAL_PUNTO.<br>Valor <b>3</b>*/
	public static final String POSICIONESNOENTERAS_REGISTROS_ICAA = "POSICIONESNOENTERAS_REGISTROS_ICAA";
	/**Caracteristica mediante la que obtenemos el numero de posiciones de valores decimales en los campos con valores decimales de los registros del Informe para la ICAA.<br>Valor <b>POSICIONESDECIMALES_REGISTROS_ICAA</b>*/
	public static final String POSICIONESDECIMALES_REGISTROS_ICAA = "POSICIONESDECIMALES_REGISTROS_ICAA";
	
	/**Codigo del Buzon que nos debe asignar la ICAA.<br>Estamos a la espera de que nos lo asignen.<br>Valor provisional <b>COD</b>*/
	public static final String CODIGO_BUZON_ICAA = "CODIGO_BUZON_ICAA";
	/**Codigo de la Sala que nos debe proporcionar la ICAA, de momento metemos este.<br><b>123456</b>*/
	public static final String CODIGO_SALA_HEMISFERIC = "CODIGO_SALA_HEMISFERIC";
	
	/**Valor para el campo tipoFicero del {@link RegistroICAA0} <b>FL</b>*/
	public static final String TIPO_FICHERO_ICAA_HABITUAL = "TIPO_FICHERO_ICAA_HABITUAL";
	/**Valor para el campo tipoFicero del {@link RegistroICAA0} <b>AT</b>*/
	public static final String TIPO_FICHERO_ICAA_ATRASADO = "TIPO_FICHERO_ICAA_ATRASADO";
	/**Nombre de la caracteristica de la tabla ICAA_Configuracionicaa que vamos a tomar para determinar el atributo que indica cuantos contenidos reales se representan con el contenido de Colossus.<br>Este valor es el que habra que dar de alta desde la aplicacion<br>Valor <b>NUMERO_CONTENIDOS_ICAA_POR_SESION</b>*/
	public static final String NUMERO_CONTENIDOS_ICAA_POR_SESION = "NUMERO_CONTENIDOS_ICAA_POR_SESION";
	/**Nombre de la caracteristica de la tabla ICAA_Configuracionicaa que vamos a tomar para determinar el atributo que indica los títulos de los expedientes asociados al Contenido de Colossus.<br>Este valor es el que habra que dar de alta desde la aplicacion<br>Valor <b>TITULO_PELICULA_ICAA</b>*/
	public static final String TITULO_PELICULA_ICAA = "TITULO_PELICULA_ICAA";
	/**Nombre del campo que vamos a tomar para comparar con el campo Codigo interno Pelicula.<br>Este valor es el que habra que dar de alta desde la aplicacion<br>Valor <b>Codigo Interno Pelicula</b>*/
	public static final String CODIGO_INTERNO_PELICULA_ICAA = "CODIGO_INTERNO_PELICULA_ICAA";
	/**Nombre del campo que vamos a tomar para comparar con el campo Codigo expediente Pelicula.<br>Este valor es el que habra que dar de alta desde la aplicacion<br>Valor <b>Codigo Expediente Pelicula</b>*/
	public static final String CODIGO_EXPEDIENTE_PELICULA_ICAA = "CODIGO_EXPEDIENTE_PELICULA_ICAA";
	/**Nombre del campo que vamos a tomar para comparar con el campo Codigo distribuidora.<br>Este valor es el que habra que dar de alta desde la aplicacion<br>Valor <b>Codigo Distribuidora</b>*/
	public static final String CODIGO_DISTRIBUIDORA_ICAA = "CODIGO_DISTRIBUIDORA_ICAA";
	/**Nombre del campo que vamos a tomar para comparar con el campo nombre distribuidora.<br>Este valor es el que habra que dar de alta desde la aplicacion<br>Valor <b>Nombre Distribuidora</b>*/
	public static final String NOMBRE_DISTRIBUIDORA_ICAA = "NOMBRE_DISTRIBUIDORA_ICAA";
	/**Nombre del campo que vamos a tomar para comparar con el campo Codigo idioma pelicula.<br>Este valor es el que habra que dar de alta desde la aplicacion<br>Valor <b>Codigo Idioma Pelicula</b>*/
	public static final String CODIGO_IDIOMA_PELICULA_ICAA = "CODIGO_IDIOMA_PELICULA_ICAA";
	/**Nombre del campo que vamos a tomar para comparar con el campo Codigo subtitulos pelicula.<br>Este valor es el que habra que dar de alta desde la aplicacion<br>Valor <b>Codigo Subtitulos Pelicula</b>*/
	public static final String CODIGO_SUBTITULOS_PELICULA_ICAA = "CODIGO_SUBTITULOS_PELICULA_ICAA";
	/**Nombre de la constante a traves de la cual obtendremos el nombre del Recinto sobre el que se va obtener los informes para el ICAA<br>NOMBRE_RECINTO_ICAA<b></b>*/
	public static final String NOMBRE_RECINTO_ICAA = "NOMBRE_RECINTO_ICAA";
	/**Nombre de la caracteristica de la tabla configuracion ICAA que permite obtener el valor del campo <b>descripcion_CAC</b> por el cual se va a buscar el código a generar en el fichero.<br><b>Sin Incidencias</b>*/
	public static final String DESCRIPCION_VENTA_SIN_INCIDENCIAS_ICAA = "DESCRIPCION_VENTA_SIN_INCIDENCIAS_ICAA";
	/**Nombre de la constante a traves de la cual obtendremos el valor del separador decimal a utilizar en el tratamiento de los ficheros para la ICAA.<br><b>MARCA_DECIMAL_ICAA</b>*/
	public static final String MARCA_DECIMAL_ICAA = "MARCA_DECIMAL_ICAA";
	/**Nombre de la constante o caracteristica a partir de la cual obtenemos el caracter con el que padear los valores numericos por la izquierda para generar los registros del para enviar el informe a la ICAA.<br><b>CARACTER_PADEO_VALORES_NUMERICOS_ICAA</b>*/
	public static final String CARACTER_PADEO_VALORES_NUMERICOS_ICAA = "CARACTER_PADEO_VALORES_NUMERICOS_ICAA";
	/**Nombre de la constante o caracteristica a partir de la cual obtenemos el caracter con el que padear los valores alfanumericos para generar los registros del para enviar el informe a la ICAA.<br><b>CARACTER_PADEO_VALORES_ALFANUMERICOS_ICAA</b>*/
	public static final String CARACTER_PADEO_VALORES_ALFANUMERICOS_ICAA = "CARACTER_PADEO_VALORES_ALFANUMERICOS_ICAA";
	/**<br><b>CODIGO_VERSION_ORIGINAL_PELICULA_ICAA</b>*/
	public static final String CODIGO_VERSION_ORIGINAL_PELICULA_ICAA = "CODIGO_VERSION_ORIGINAL_PELICULA_ICAA";
	/**<br><b>CODIGO_FORMATO_PROYECCION_PELICULA_ICAA</b>*/
	public static final String CODIGO_FORMATO_PROYECCION_PELICULA_ICAA = "CODIGO_FORMATO_PROYECCION_PELICULA_ICAA";
	/**<br><b>RUTA_ALMACENAMIENTO_INFORMES_ICAA</b>*/
	public static final String RUTA_ALMACENAMIENTO_INFORMES_ICAA = "RUTA_ALMACENAMIENTO_INFORMES_ICAA";
	/**Indica la ruta completa incluyendo el nombre del fichero donde localizar la clave publica con la que vamos a cifrar los informes ICAA.<br><b>UBICACION_CLAVE_PUBLICA_CIFRADO_ICAA</b>*/
	public static final String UBICACION_CLAVE_PUBLICA_CIFRADO_ICAA = "UBICACION_CLAVE_PUBLICA_CIFRADO_ICAA";
	/**Indica la ruta completa incluyendo el nombre del fichero donde localizar la firma con la que vamos a cifrar los informes ICAA que previamente ha de ser autorizada por el ICAA.<br><b>UBICACION_FIRMA_AUTORIZADA_ICAA</b>*/
	public static final String UBICACION_FIRMA_AUTORIZADA_ICAA = "UBICACION_FIRMA_AUTORIZADA_ICAA";
	/**<br><b>SEMANAS_MARGEN_GENERACION_ICAA</b>*/
	public static final String SEMANAS_MARGEN_GENERACION_ICAA = "SEMANAS_MARGEN_GENERACION_ICAA";
	/**<br><b>EXTENSION_FICHERO_CIFRADO_ICAA</b>*/
	public static final String EXTENSION_FICHERO_CIFRADO_ICAA = "EXTENSION_FICHERO_CIFRADO_ICAA";
	/**<br><b>EXTENSION_FICHERO_INCIDENCIAS_ICAA</b>*/
	public static final String EXTENSION_FICHERO_INCIDENCIAS_ICAA = "EXTENSION_FICHERO_INCIDENCIAS_ICAA";
	/**<br><b>PREAMBULO_NOMBRE_FICHERO_INCIDENCIAS_ICAA</b>*/
	public static final String PREAMBULO_NOMBRE_FICHERO_INCIDENCIAS_ICAA = "PREAMBULO_NOMBRE_FICHERO_INCIDENCIAS_ICAA";
	/**<br><b>SUBCARPETA_INFORMES_INCIDENCIAS_ICAA</b>*/
	public static final String SUBCARPETA_INFORMES_INCIDENCIAS_ICAA = "SUBCARPETA_INFORMES_INCIDENCIAS_ICAA";
	/*<br><b>EXTENSION_FICHERO_CIFRADOYFIRMADO_ICAA</b>
	public static final String EXTENSION_FICHERO_CIFRADOYFIRMADO_ICAA = "EXTENSION_FICHERO_CIFRADOYFIRMADO_ICAA";*/
	/*<br><b>EXTENSION_FICHERO_ICAA</b>
	public static final String EXTENSION_FICHERO_ICAA = "EXTENSION_FICHERO_ICAA";*/
	/**<br><b>EPILOGO_FICHERO_ICAA_RECODIFICADO</b>*/
	public static final String EPILOGO_FICHERO_ICAA_RECODIFICADO = "EPILOGO_FICHERO_ICAA_RECODIFICADO";
	/**<br><b>INICIO_NOMBRE_FICHERO_ICAA</b>*/
	public static final String INICIO_NOMBRE_FICHERO_ICAA = "INICIO_NOMBRE_FICHERO_ICAA";
	/**Esta característica de la tabla ICAA_CONFIGURACIONICAA puede llevar asociados los siguientes valores:<br><b>0</b> va a indicar que se generará el nombre del fichero acabado con el numero Juliano del dia dentro del año.<br><b>1</b> va a indicar que se generará el nombre del fichero acabado con el indice de la semana dentro del año.<br><b>FINAL_NOMBRE_FICHERO_DIAJULIANO_0_SEMANAL_1_ICAA</b>*/
	public static final String FINAL_NOMBRE_FICHERO_DIAJULIANO_0_SEMANAL_1_ICAA = "FINAL_NOMBRE_FICHERO_DIAJULIANO_0_SEMANAL_1_ICAA";
	/**Esta característica de la tabla ICAA_CONFIGURACIONICAA espceifica a que dirección de correo electrónico se han de enviar los informes para el ICAA, una vez cifrados y firmados con la firma autorizada.<br><b>EMAIL_ENVIO_INFORMES_ICAA</b>*/
	public static final String EMAIL_ENVIO_INFORMES_ICAA = "EMAIL_ENVIO_INFORMES_ICAA";
	/**Esta característica de la tabla ICAA_CONFIGURACIONICAA espceifica a que dirección de correo electrónico se han de enviar los informes con información de sesiones y recaudación que se envia al ICAA.<br><b>EMAIL_ENVIO_INFORMES_ICAA_HR</b>*/
	public static final String EMAIL_ENVIO_INFORMES_ICAA_HR = "EMAIL_ENVIO_INFORMES_ICAA_HR";
	/**Número mínimo de dias que van a componer la primera semana del año según las reglas del calendario Gregoriano<br><b>NUMERO_MINIMO_DIAS_PRIMERA_SEMANA_ICAA</b>*/
	public static final String NUMERO_MINIMO_DIAS_PRIMERA_SEMANA_ICAA = "NUMERO_MINIMO_DIAS_PRIMERA_SEMANA_ICAA";
	/**Nombre de la característica que determina el valor del índice del dia según las constantes definidas en la clase Calendar para los días de la semana.<br>Notese que el 1 se corresponde con el Domingo Calendar.SUNDAY y el 7 con el Sábado Calendar.SATURDAY.<br><b>PRIMER_DIA_SEMANA_NATURAL_ICAA</b>*/
	public static final String PRIMER_DIA_SEMANA_NATURAL_ICAA = "PRIMER_DIA_SEMANA_NATURAL_ICAA";
	/**ID de la frima cuyo nombre es ICAA, es lo unico que he visto que puedo comparar o detectar en la PGPPublicKey.<br><b>ID_CLAVE_PUBLICA_CIFRADO_ICAA</b>*/
	public static final String ID_CLAVE_PUBLICA_CIFRADO_ICAA = "ID_CLAVE_PUBLICA_CIFRADO_ICAA";
	/**Password utilizado para proteger el acceso al fichero pfx de pareja de claves Pública y Privada para firmar los informes de CAC para el ICAA.<br><b>PASSWORD_PAREJA_CLAVES_FIRMA_CAC_ICAA</b>*/
	public static final String PASSWORD_PAREJA_CLAVES_FIRMA_CAC_ICAA = "PASSWORD_PAREJA_CLAVES_FIRMA_CAC_ICAA";
	/*Nombre del atributo de la tabla de configuracionICAA para obtener el servidor de envio de correos smtp.<br><b>SMTP_SERVER_ICAA<b>
	public static final String SMTP_SERVER_ICAA = "SMTP_SERVER_ICAA";*/
	/**Nombre del atributo de la tabla de configuracionICAA para obtener la direccion de correo del remitente autorizado por el ICAA para enviar los informes.<br><b>EMAIL_REMITENTE_AUTORIZADO_INFORMES_ICAA</b>*/
	public static final String EMAIL_REMITENTE_AUTORIZADO_INFORMES_ICAA = "EMAIL_REMITENTE_AUTORIZADO_INFORMES_ICAA";
	/**Nombre de la característica que nos va a permitir obtener el valor del tipo de KeyStore utilizado para contener la firma digital autorizada para firmar los correos a enviar al ICAA<br><b>TIPO_KEYSTORE_FIRMA_AUTORIZADA_ICAA</b>*/
	public static final String TIPO_KEYSTORE_FIRMA_AUTORIZADA_ICAA = "TIPO_KEYSTORE_FIRMA_AUTORIZADA_ICAA";
	/**Nombre de la característica que nos va a servir para saber si hemos de utilizar el modo de compatibilidad con la versión de PGP 2.6.x o anteriores.<br><b>MODO_COMPATIBILIDAD_PGP26x_ICAA</b>*/
	public static final String MODO_COMPATIBILIDAD_PGP26x_ICAA = "MODO_COMPATIBILIDAD_PGP26x_ICAA";
	/**Nombre de la característica que nos va a servir para determinar el algoritmo a utilizar en el proceso de firma a utilizar en el envio de correos electronicos para el ICAA<br><b>ALGORITMO_FIRMA_ENVIO_CORREOS_ICAA</b>*/
	public static final String ALGORITMO_FIRMA_ENVIO_CORREOS_ICAA = "ALGORITMO_FIRMA_ENVIO_CORREOS_ICAA";
	/**Nombre de la característica que nos va a servir para determinar el agoritmo a utilizar para poder desencriptar el password que permite tener acceso al fichero de firma digital.<br><b>ALGORITMO_CIFRADO_DESCIFRADO_PASSWORD_FIRMA_ICAA</b>*/
	public static final String ALGORITMO_CIFRADO_DESCIFRADO_PASSWORD_FIRMA_ICAA = "ALGORITMO_CIFRADO_DESCIFRADO_PASSWORD_FIRMA_ICAA";
	/**Nombre de la característica que nos va a servir para determinar el agoritmo a utilizar en la encriptación con PGP.<br><b>ALGORITMO_SIMETRICO_PGP_ICAA</b>*/
	public static final String ALGORITMO_SIMETRICO_PGP_ICAA = "ALGORITMO_SIMETRICO_PGP_ICAA";

	/**Nombre de la constante a traves de la cual obtendremos el valor con el que indicaremos que la venta se ha realizado de forma manual, debido a que el sistema no esta disponible.<br><b>INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL</b>*/
	public static final String INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL = "INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL";
	/**Nombre de la constante a traves de la cual obtendremos el valor con el que indicaremos que la venta se ha realizado de forma manual, debido a que el sistema no esta disponible y además se había cambiado la programación de la sesión actual con anterioridad.<br><b>INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL_ANULACION_PROGRAMACION</b>*/
	public static final String INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL_ANULACION_PROGRAMACION = "INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL_ANULACION_PROGRAMACION";
	/**Nombre de la constante a traves de la cual obtendremos el valor con el que indicaremos que la venta se ha realizado de forma manual, debido a que el sistema no esta disponible, se había cambiado la programación de la sesión actual con anterioridad y además se anulan entradas.<br><b>INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL_ANULACION_PROGRAMACION_Y_VENTAS</b>*/
	public static final String INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL_ANULACION_PROGRAMACION_Y_VENTAS = "INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL_ANULACION_PROGRAMACION_Y_VENTAS";
	/**Nombre de la constante a traves de la cual obtendremos el valor con el que indicaremos que la venta se ha realizado de forma manual, debido a que el sistema no esta disponible y además se han anulado ventas.<br><b>INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL_ANULACION_VENTAS</b>*/
	public static final String INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL_ANULACION_VENTAS = "INCIDENCIA_ICAA_CODIGO_VENTA_MANUAL_ANULACION_VENTAS";
	/**Nombre de la constante a traves de la cual obtendremos el valor con el que indicaremos que la venta no tiene incidencias.<br><b>INCIDENCIA_ICAA_CODIGO_VENTA_SIN_INCIDENCIAS</b>*/
	public static final String INCIDENCIA_ICAA_CODIGO_VENTA_SIN_INCIDENCIAS = "INCIDENCIA_ICAA_CODIGO_VENTA_SIN_INCIDENCIAS";
	/**Nombre de la constante a traves de la cual obtendremos el valor con el que indicaremos que la venta no tiene incidencias de las que se pueden asignar manualmente.<br><b>INCIDENCIA_ICAA_CODIGO_VENTA_SIN_INCIDENCIAS_MANUALMENTE_ASIGNABLES</b>*/
	public static final String INCIDENCIA_ICAA_CODIGO_VENTA_SIN_INCIDENCIAS_MANUALMENTE_ASIGNABLES = "INCIDENCIA_ICAA_CODIGO_VENTA_SIN_INCIDENCIAS_MANUALMENTE_ASIGNABLES";
	/**Nombre de la constante a traves de la cual obtendremos el codigo de la IncidenciaICAA correspondiente a la anulación de la Programacion de una sesión y sustitución por otra.<br><b>INCIDENCIA_ICAA_CODIGO_ANULACION_PROGRAMACION</b>*/
	public static final String INCIDENCIA_ICAA_CODIGO_ANULACION_PROGRAMACION = "INCIDENCIA_ICAA_CODIGO_ANULACION_PROGRAMACION";
	/**Nombre de la constante a traves de la cual obtendremos el codigo de la IncidenciaICAA correspondiente a la anulación de ventas para una sesión.<br><b>INCIDENCIA_ICAA_CODIGO_ANULACION_VENTAS</b>*/
	public static final String INCIDENCIA_ICAA_CODIGO_ANULACION_VENTAS = "INCIDENCIA_ICAA_CODIGO_ANULACION_VENTAS";
	/**Nombre de la constante a traves de la cual obtendremos el codigo de la IncidenciaICAA correspondiente a la anulación de ventas para una sesión que además había sido previamente reprogramada.<br><b>INCIDENCIA_ICAA_CODIGO_ANULACION_VENTAS_Y_PROGRAMACION</b>*/
	public static final String INCIDENCIA_ICAA_CODIGO_ANULACION_VENTAS_Y_PROGRAMACION = "INCIDENCIA_ICAA_CODIGO_ANULACION_VENTAS_Y_PROGRAMACION";

	/**Cadena a incluir si se excede el maximo valor representable para la Recaudacion.*/
	public static final String MAXRECAUDACION_EXCEDIDA_ICAA ="MAXRECAUDACION_EXCEDIDA_ICAA";
	/**Cadena a incluir si se excede el maximo valor representable para la Recaudacion.*/
	public static final String MAXRECAUDACIONSESION_EXCEDIDA_ICAA ="MAXRECAUDACIONSESION_EXCEDIDA_ICAA";
	/**Cadena de texto que representa el caracter basico a repetir, en el caso de no encontrarse un Atributo con el nombre especificado asociado al Contenido Pelicula Hemisfèric.*/
	public static final String ATRIBUTO_POR_NOMBRE_NO_ENCONTRADO_ICAA = "ATRIBUTO_POR_NOMBRE_NO_ENCONTRADO_ICAA";

	/**Formato generico para las fechas expresadas como Anyo mes y dia DDMMAA.<br>ddMMyy*/
	public static final String PATRON_FORMATOFECHAREGISTRO_ICAA = "PATRON_FORMATOFECHAREGISTRO_ICAA";
	/**Formato generico para las horas expresadas como Hora (24horas) y Minuto HHMM.<br>HHmm*/
	public static final String PATRON_FORMATOHORAREGISTRO_ICAA  = "PATRON_FORMATOHORAREGISTRO_ICAA";
	/**Formato generico para los nombres de fichero que contendra el Informe generado para la ICAA.*/
	public static final String PATRON_FORMATOFECHANOMBRE_ICAA = "PATRON_FORMATOFECHANOMBRE_ICAA";

	/**Nombre de la tarea programada que identifica al proceso automatizado de generacion Semanal del Informe para enviar al ICAA, segun se dió de alta en la tabla SINCRONIZACION de Colossus.<br><b>informeSemanalICAA</b>*/
	public static final String NOMBRE_TAREA_GENERACION_SEMANAL_INFORME_ICAA = "informeSemanalICAA";
	
	/** Nombre de la tarea programada que indica el proceso automatizado de envio automático de facturas electrónicas */
	public static final String NOMBRE_TAREA_ENVIO_AUTOMATICO_FACTURAS_ELECTRONICAS = "enviofacturaselectronicas";

	/*Etiqueta del nodo que representa la raiz del arbol XML que constituye el fichero de configuracion para las fenix TK 51.<br><b><ListaEntradasPR></b>*
	public static final String ETIQUETA_RAIZ_XML_FENIX = "<ListaEntradasPR>";*/
	/**Etiqueta que marca el inicio del nodo TimeOutServicio, gracias al cual se hara llegar al servidor de impresion el timeout del servicio estipulado en la aplicación con el fin de no bloquear indefinidamente la impresora.*/
	public static final String APERTURA_ETIQUETA_TIEMOUT_SERVICIO = "<TimeOutServicio>";
	/**Etiqueta que marca el fin del nodo TimeOutServicio, gracias al cual se hara llegar al servidor de impresion el timeout del servicio estipulado en la aplicación con el fin de no bloquear indefinidamente la impresora.*/
	public static final String CIERRE_ETIQUETA_TIEMOUT_SERVICIO = "</TimeOutServicio>";
	/**Timeout en ms para los servicios de impresion directa, sin cola y que deben responder a la capa de servicios online.<br><b>600.000 ms</b>*/
	public static final int TIMEOUT_SERVICIOS_IMPRESION_DIRECTA = 600000;
	/**Timeout en ms para los servicios de impresion.<br><b>20.000 ms</b>*/
	public static final int TIMEOUT_SERVICIOS_IMPRESION = 20000;
	/**Timeout en ms para soltar un Timeout al intentar conectar con un servidor.<br><b>10.000 ms</b>*/
	public static final int CONNECTION_TIMEOUT = 10000;

	/**Ruta en la que se van a generar las imagenes del Club del modulo de gestión de Pases Club de TISSAT.<br><b>Ojo que se han creado una clase suya de Constantes y no se pueden importar las 2 en la misma Clase pues hay colision.</b>*/
	public static final String DIRECTORIO_IMAGENES = "c:\\tmp\\";

	/**Este es el estado que va a devolver el servidor de impresion cuando se produzca un error en la impresion y queden entradas pendientes de imprimir.*/
	public static final int ESTADO_SERVIDOR_IMPRESION_CON_ENTRADAS_PEMDIENTES = 510;

	/**Extension de los fichos que contienen las plantillas de velocity o velocimacors utilizadas por el sistema de impresión de Colossus.<br><b>.vm</b>*/
	public static final String EXTENSION_PLANTILLAS_IMPRESION = ".vm";

	/**número de socios a buscar por defecto para mostrar en el listado de socios.<br><b>20</b>*/
	public static final int NUM_RESULTADOS_BUSQUEDA_SOCIOS_PASECLUB_ULTIMOS = 20;

	/** Número de días para el vencimiento de las facturas a clientes de crédito<br><b>45</b> */
	public static final int DIAS_VENCIMIENTO_FACTURA_CREDITO = 45;

	/** Código asignado al cliente genérico prepago<br><b>H1</b>*/
	public static final String H1 = "H1";

	/** Código asignado al cliente genérico crédito<br><b>H2</b>*/
	public static final String H2 = "H2";

	// JMH (22.05.2017 10:08): SAP 2.0
	/**
	 * Código asignado al cliente genérico prepago<br>
	 * <b>H1</b>
	 */
	public static final String H4 = "H4";

	/**
	 * Código asignado al cliente genérico crédito<br>
	 * <b>H2</b>
	 */
	public static final String H5 = "H5";
	// JMH (22.05.2017 10:08): SAP 2.0

	/**
	 * {@link NumeroDecimal} construido a partir del constructor por Defecto, con
	 * el Valor {@link BigDecimal}.ZERO <br>
	 * <br>
	 * <b>new NumeroDecimal()</b><br>
	 * <br>
	 * <b>JAMAS UTILIZAR PARA INICIALIZAR VARIBLES PUES SE PUEDEN MODIFICAR LOS
	 * VALORES DE LAS SUPUESTAS CONSTANTES A TRAVES DE REFERENCIAS.</b><br>
	 * <br>
	 * <b>TENER SIEMPRE EN MENTE EL ORDEN DE CREACION DE LAS CONSTANTES.<br>
	 * El constructor por defecto del NumeroDecimal depende entre otros de la
	 * CADENA_00 que al ponerlo al principio de la clase, este aun no tenia
	 * valor y por tanto, se asignaba con el valor "" a la parteDecimal lo que
	 * hacia que fallaran todas las comparaciones con la Constante NDZERO.</b>
	 */
	public static final NumeroDecimal NDZERO = new NumeroDecimal();

	/** ID de la entidad gestora CAC **/
	public static final String IDENTIDADGESTORACAC = "1";

	/** ID de la entidad gestora AVANQUA **/
	public static final String IDENTIDADGESTORAAVANQUA = "2";

	/** Nombre de la entidad gestora CAC **/
	public static final String NOMBREENTIDADGESTORACAC = "CAC";

	/** Nombre de la entidad gestora AVANQUA **/
	public static final String NOMBREENTIDADGESTORAAVANQUA = "AVANQUA";

	/**Nombre en la tabla CONFIGURACION para las facturas normales y rectificativas de la Entidad Gestora CAC.*/
	public static final String FACTURANORMALYRECTIFICATIVACAC = "facturacion.facturanormalyrectificativaCAC";

	/**Nombre en la tabla CONFIGURACION para las facturas normales y rectificativas de la Entidad Gestora AVANQUA.*/
	public static final String FACTURANORMALYRECTIFICATIVAAVANQUA = "facturacion.facturanormalyrectificativaAVANQUA";
	
	/**Nombre en la tabla CONFIGURACION para las facturas de bono crédito y rectificativas de la Entidad Gestora CAC.*/
	public static final String FACTURABONOCREDITOYRECTIFICATIVACAC = "facturacion.facturaBonoCreditoYRectificativaCAC";

	/**Nombre en la tabla CONFIGURACION para las facturas de bono crédito y rectificativas de la Entidad Gestora AVANQUA.*/
	public static final String FACTURABONOCREDITOYRECTIFICATIVAAVANQUA = "facturacion.facturaBonoCreditoYRectificativaAVAN";

	/**Nombre en la tabla CONFIGURACION para las facturas de bono prepago y rectificativas de la Entidad Gestora CAC.*/
	public static final String FACTURABONOPREPAGOYRECTIFICATIVACAC = "facturacion.facturaBonoCreditoYRectificativaCAC";

	/**Nombre en la tabla CONFIGURACION para las facturas de bonos prepago y rectificativas de la Entidad Gestora AVANQUA.*/
	public static final String FACTURABONOPREPAGOYRECTIFICATIVAAVANQUA = "facturacion.facturaBonoCreditoYRectificativaAVAN";

	// JMH (07.04.2017): SAP 2.0
	/** Nombre en la tabla CUENTA para el cliente genérico prepago de la entidad CAC */
	public static final String CLIENTEGENERICOPREPAGOCAC = "Cliente Genérico Prepago CAC";

	/** Nombre en la tabla CUENTA para el cliente genérico crédito de la entidad CAC */
	public static final String CLIENTEGENERICOCREDITOCAC = "Cliente Genérico Crédito CAC";

	/** Nombre en la tabla CUENTA para la cuenta de IVA entidad CAC */
	public static final String CUENTAMAYORIVACAC = "Mayor IVA CAC";
	
	/** Nombre en la tabla CUENTA para la cuenta de Ingresos Anticipados de la entidad CAC */
	public static final String CUENTAMAYORANTICIPADACAC = "Mayor Venta Anticipada CAC";

	/** Nombre en la tabla CUENTA para la cuenta de Gastos Extraordinarios de la entidad CAC */
	public static final String CUENTAMAYORGASTOSEXTRAORDINARIOSCAC = "Mayor Gastos Extraordinarios CAC";
	
	/** Nombre en la tabla CUENTA para la cuenta de Mayor Ingresos CAC */
	public static final String CUENTAMAYORINGRESOSCAC = "Mayor Ingresos CAC";
	
	/** Nombre en la tabla CUENTA para la cuenta de Tarjeta Crédito CAC */
	public static final String CUENTAMAYORTARJETACREDITOCAC = "Tarjeta Crédito CAC";
	
	/** Nombre en la tabla CUENTA para la cuenta de Transferencia CAC */
	public static final String CUENTAMAYORTRANSFERENCIACAC = "Transferencia CAC";
	
	/** Nombre en la tabla CUENTA para la cuenta de Efectivo Hemis CAC */
	public static final String CUENTAMAYOREFECTIVOHEMISCAC = "Efectivo Hemis CAC";
	
	/** Nombre en la tabla CUENTA para la cuenta de Efectivo Museu CAC */
	public static final String CUENTAMAYOREFECTIVOMUSEOCAC = "Efectivo Museu CAC";
	
	/** Nombre en la tabla CUENTA para la Cuenta Ingresos Extraordinarios CAC */
	public static final String CUENTAMAYORINGRESOSEXTRAORDINARIOSCAC = "Cuenta Ingresos Extraordinarios CAC";
	
	/** Nombre en la tabla CUENTA para la cuenta de Rappels CAC */
	public static final String CUENTAMAYORRAPPELSCAC = "Rappels CAC";
	
	/** Nombre en la tabla CUENTA para la cuenta de Mayor IVA AVQ */
	public static final String CUENTAMAYORIVAAVQ = "Mayor IVA AVQ";
	
	/** Nombre en la tabla CUENTA para la cuenta de Mayor Venta Anticipada AVQ */
	public static final String CUENTAMAYORANTICIPADAAVQ = "Mayor Venta Anticipada AVQ";
	
	/** Nombre en la tabla CUENTA para la cuenta de Mayor Gastos Extraordinarios AVQ */
	public static final String CUENTAMAYORGASTOSEXTRAORDINARIOSAVQ = "Mayor Gastos Extraordinarios AVQ";
	
	/** Nombre en la tabla CUENTA para la cuenta de Mayor Ingresos AVQ */
	public static final String CUENTAMAYORINGRESOSAVQ = "Mayor Ingresos AVQ";
	
	/** Nombre en la tabla CUENTA para la cuenta de Transferencia AVQ */
	public static final String CUENTAMAYORTRANSFERENCIAAVQ = "Transferencia AVQ";
	
	/** Nombre en la tabla CUENTA para la cuenta de Efectivo AVQ */
	public static final String CUENTAMAYOREFECTIVOAVQ = "Efectivo AVQ";
	
	/** Nombre en la tabla CUENTA para la cuenta de Tarjeta Crédito AVQ */
	public static final String CUENTAMAYORTARJETACREDITOAVQ = "Tarjeta Crédito AVQ";
	
	/** Nombre en la tabla CUENTA para la cuenta de Cuenta Ingresos Extraordinarios AVQ */
	public static final String CUENTAMAYORINGRESOSEXTRAORDINARIOSAVQ = "Cuenta Ingresos Extraordinarios AVQ";
	
	/** Nombre en la tabla CUENTA para la cuenta de Rappels AVQ */
	public static final String CUENTAMAYORRAPPELSAVQ = "Rappels AVQ";
	
	/** Nombre en la tabla CUENTA para la cuenta de Tarjeta Crédito Ventas Online CAC */
	public static final String CUENTAMAYORTARJETACREDITOVENTASONLONECAC = "Tarjeta Crédito Ventas Online CAC";	
	
	/** Nombre en la tabla CUENTA para la cuenta de Cliente Genérico Prepago CAC */
	public static final String CUENTACLIENTEGENERICOPREPAGOCAC = "Cliente Genérico Prepago CAC";

	/** Nombre en la tabla CUENTA para la cuenta de Cliente Genérico Crédito CAC */
	public static final String CUENTACLIENTEGENERICOCREDITOCAC = "Cliente Genérico Crédito CAC";
	// JMH (07.04.2017): SAP 2.0

	
	
	// JMH (14.03.2018): Integración COLOSSUS - EXPERTICKET
	/** Host para el envio de correos */
	public static final String SERVIDOR_SMTP_HOST = "172.29.156.243";

	/** Puerto del servidor de correos para el envio de correos */
	public static final String SERVIDOR_SMTP_PORT = "25";

	/** Seguridad activada en el servidor de correos para el envio de correos */
	public static final String SERVIDOR_SMTP_SEGURIDAD = "0";

	/** Usuario del servidor de correos para el envio de correos */
	public static final String SERVIDOR_SMTP_USUARIO = "cac";

	/** Password del servidor de correos para el envio de correos */
	public static final String SERVIDOR_SMTP_PASSWORD = "12345678";

	/**
	 * Remitente de correos en caso de ERROR en Integración COLOSSUS -
	 * EXPERTICKET
	 */
	public static final String INTEGRACION_COLOSSUS_EXPERTICKET_REMITENTE_EMAIL = "cac@cac.es";

	/**
	 * Destinatario de correos en caso de ERROR en Integración COLOSSUS -
	 * EXPERTICKET
	 */
	public static final String INTEGRACION_COLOSSUS_EXPERTICKET_DESTINATARIO_EMAIL = "jmarin@cac.es";

	/** Asunto de correos en caso de ERROR en Integración COLOSSUS - EXPERTICKET */
	public static final String INTEGRACION_COLOSSUS_EXPERTICKET_ASUNTO_EMAIL = "ERROR en la venta online EXPERTICKET";

	/**
	 * Mensaje de correos en caso de ERROR en Integración COLOSSUS - EXPERTICKET
	 */
	public static final String INTEGRACION_COLOSSUS_EXPERTICKET_MENSAJE_EMAIL = "Se ha producido un ERROR en la venta online EXPERTICKET";

	/**************************************************************************************************************************************
	 * CONSTANTES PARA LOS MENSAJES DE ESTADO EN LAS LLAMADAS A LOS SERVICIOS
	 * DESDE INTERNET (INTEGRACIÓN COLOSSUS EXPERTICKET)
	 **************************************************************************************************************************************/
	/** Ejecución Correcta del servicio: 200 OK **/
	public static final Integer SERVICESTATUSCODE_OK = 200;
	public static final String  SERVICESTATUSMESSAGE_OK = "OK";
	public static final String  SERVICESTATUSDESCRIPTION_OK = "Ejecución correcta del servicio";

	/** Not FoundStatusLine: HTTP/1.1: 404 Not Found **/
	public static final Integer SERVICESTATUSCODE_NOTFOUND = 404;
	public static final String  SERVICESTATUSMESSAGE_NOTFOUND = "Servicio no publicado o inexistente";
	public static final String  SERVICESTATUSDESCRIPTION_NOTFOUND = "Servicio no publicado o inexistente";

	/** ERROR INTERNO DEL SERVIDOR (EXCEPCIÓN NO CONTROLADA): 500 **/
	public static final Integer SERVICESTATUSCODE_INTERNAL_SERVER_ERROR = 500;
	public static final String  SERVICESTATUSMESSAGE_INTERNAL_SERVER_ERROR = "ERROR INTERNO DEL SERVIDOR";
	public static final String  SERVICESTATUSDESCRIPTION_INTERNAL_SERVER_ERROR = "Error Interno del Servidor";

	/** HiberntateException: 501 **/
	public static final Integer SERVICESTATUSCODE_HIBERNATE_ERROR = 501;
	public static final String  SERVICESTATUSMESSAGE_HIBERNATE_ERROR = "Error en la capa de persistencia con la Base de Datos";
	public static final String  SERVICESTATUSDESCRIPTION_HIBERNATE_ERROR = "Error producido en la capa de persistencia con la Base de Datos";

	/** UnsupportedOperationException: 502 **/
	public static final Integer SERVICESTATUSCODE_UNSUPPORTEDOPERATION_ERROR = 502;
	public static final String  SERVICESTATUSMESSAGE_UNSUPPORTEDOPERATION_ERROR = "Error por Operación no Permitida";
	public static final String  SERVICESTATUSDESCRIPTION_UNSUPPORTEDOPERATION_ERROR = "Error producido por Operación no Permitida";

	/** ClassCastException: 503 **/
	public static final Integer SERVICESTATUSCODE_CLASSCAST_ERROR = 503;
	public static final String  SERVICESTATUSMESSAGE_CLASSCAST_ERROR = "Error en la conversión de clase";
	public static final String  SERVICESTATUSDESCRIPTION_CLASSCAST_ERROR = "Error producido en la conversión (CAST) de una clase";

	/** NullPointerException: 504 **/
	public static final Integer SERVICESTATUSCODE_NULLPOINTER_ERROR = 504;
	public static final String  SERVICESTATUSMESSAGE_NULLPOINTER_ERROR = "Error NullPointer en la llamada al servicio";
	public static final String  SERVICESTATUSDESCRIPTION_NULLPOINTER_ERROR = "Error producido por NullPointerException en la llamada al servicio";

	/** IllegalArgumentException: 505 **/
	public static final Integer SERVICESTATUSCODE_ILLEGALARGUMENT_ERROR = 505;
	public static final String  SERVICESTATUSMESSAGE_ILLEGALARGUMENT_ERROR = "Error IllegalArgument en la llamada al servicio";
	public static final String  SERVICESTATUSDESCRIPTION_ILLEGALARGUMENT_ERROR = "Error producido por IllegalArgument en la llamada al servicio";

	/** MessagingException: 506 **/
	public static final Integer SERVICESTATUSCODE_MESSAGING_ERROR = 506;
	public static final String  SERVICESTATUSMESSAGE_MESSAGING_ERROR = "Error en el envio del mensaje por email";
	public static final String  SERVICESTATUSDESCRIPTION_MESSAGING_ERROR = "Error producido en el envio del mensaje por email";

	/** UnsupportedEncodingException: 507 **/
	public static final Integer SERVICESTATUSCODE_UNSUPPORTEDENCODING_ERROR = 507;
	public static final String  SERVICESTATUSMESSAGE_UNSUPPORTEDENCODING_ERROR = "Error UnsupportedEncodingException en el envio del mensaje por email";
	public static final String  SERVICESTATUSDESCRIPTION_UNSUPPORTEDENCODING_ERROR = "Error UnsupportedEncodingException producido en el envio del mensaje por email";
	
	/** NumberFormatException: 508 **/
	public static final Integer SERVICESTATUSCODE_NUMBERFORMAT_ERROR = 508;
	public static final String  SERVICESTATUSMESSAGE_NUMBERFORMAT_ERROR = "Error NumberFormatException";
	public static final String  SERVICESTATUSDESCRIPTION_NUMBERFORMAT_ERROR = "Error NumberFormatException al intenta leer un valor numérico";

	/** ERROR: El servicio no ha devuelto resultados: 600 **/
	public static final Integer SERVICESTATUSCODE_EMPTYRESPONSE_ERROR = 600;
	public static final String  SERVICESTATUSMESSAGE_EMPTYRESPONSE_ERROR = "ERROR: El servicio no ha devuelto resultados";
	public static final String  SERVICESTATUSDESCRIPTION_EMPTYRESPONSE_ERROR = "ERROR: El servicio no ha devuelto resultados";

	/** NullCanalVentas: 601 **/
	public static final Integer SERVICESTATUSCODE_CANALVENTAS_ERROR = 601;
	public static final String  SERVICESTATUSMESSAGE_CANALVENTAS_ERROR = "Error: el Canal de Ventas no existe";
	public static final String  SERVICESTATUSDESCRIPTION_CANALVENTAS_ERROR = "Error: el Canal de Ventas no existe";
	
	/** Producto: 602 **/
	public static final Integer SERVICESTATUSCODE_PRODUCTO_ERROR = 602;
	public static final String  SERVICESTATUSMESSAGE_PRODUCTO_ERROR = "Error: el Producto no existe";
	public static final String  SERVICESTATUSDESCRIPTION_PRODUCTO_ERROR = "Error: el Producto no existe";

	/** Fecha Sesión: 603 **/
	public static final Integer SERVICESTATUSCODE_FECHASESION_ERROR = 603;
	public static final String  SERVICESTATUSMESSAGE_FECHASESION_ERROR = "Error en la fecha de sesión";
	public static final String  SERVICESTATUSDESCRIPTION_FECHASESION_ERROR = "Error en la fecha de sesión";

	/** LanguageId Error: 604 **/
	public static final Integer SERVICESTATUSCODE_LANGUAGEID_ERROR = 604;
	public static final String  SERVICESTATUSMESSAGE_LANGUAGEID_ERROR = "Error en el LanguageID";
	public static final String  SERVICESTATUSDESCRIPTION_LANGUAGEID_ERROR = "Error en el LanguageID";
	
	/** TipoProducto Error: 605 **/
	public static final Integer SERVICESTATUSCODE_TIPOPRODUCTO_ERROR = 605;
	public static final String  SERVICESTATUSMESSAGE_TIPOPRODUCTO_ERROR = "Error: el Tipo de Producto no existe";
	public static final String  SERVICESTATUSDESCRIPTION_TIPOPRODUCTO_ERROR = "Error: el Tipo de Producto no existe";

	/** Cliente Error: 606 **/
	public static final Integer SERVICESTATUSCODE_CLIENTE_ERROR = 606;
	public static final String  SERVICESTATUSMESSAGE_CLIENTE_ERROR = "Error: el Cliente no existe";
	public static final String  SERVICESTATUSDESCRIPTION_CLIENTE_ERROR = "Error: el Cliente no existe";

	/** FormaPago Error: 607 **/
	public static final Integer SERVICESTATUSCODE_FORMAPAGO_ERROR = 607;
	public static final String  SERVICESTATUSMESSAGE_FORMAPAGO_ERROR = "Error: La Forma de Pago no existe";
	public static final String  SERVICESTATUSDESCRIPTION_FORMAPAGO_ERROR = "Error: La Forma de Pago no existe";

	/** Pais Error: 608 **/
	public static final Integer SERVICESTATUSCODE_PAIS_ERROR = 608;
	public static final String  SERVICESTATUSMESSAGE_PAIS_ERROR = "Error: el Pais no existe";
	public static final String  SERVICESTATUSDESCRIPTION_PAIS_ERROR = "Error: el Pais no existe";

	/** DatosVentaInternet Error: 609 **/
	public static final Integer SERVICESTATUSCODE_DATOSVENTAINTERNET_ERROR = 609;
	public static final String  SERVICESTATUSMESSAGE_DATOSVENTAINTERNET_ERROR = "Error: no existen DatosVentaInternet para la venta";
	public static final String  SERVICESTATUSDESCRIPTION_DATOSVENTAINTERNET_ERROR = "Error: no existen DatosVentaInternet para la venta";

	/** Prerreserva Error: 610 **/
	public static final Integer SERVICESTATUSCODE_PRERRESERVA_ERROR = 610;
	public static final String  SERVICESTATUSMESSAGE_PRERRESERVA_ERROR = "Error: no existen Prerreservas asociadas a la sesión de usuario";
	public static final String  SERVICESTATUSDESCRIPTION_PRERRESERVA_ERROR = "Error: no existen Prerreservas asociadas a la sesión de usuario";

	/** SesionUsuario Error: 611 **/
	public static final Integer SERVICESTATUSCODE_SESIONUSUARIO_ERROR = 611;
	public static final String  SERVICESTATUSMESSAGE_SESIONUSUARIO_ERROR = "Error: no existe la sesión de usuario";
	public static final String  SERVICESTATUSDESCRIPTION_SESIONUSUARIO_ERROR = "Error: no existe la sesión de usuario";

	/** Contenido Error: 612 **/
	public static final Integer SERVICESTATUSCODE_CONTENIDO_ERROR = 612;
	public static final String  SERVICESTATUSMESSAGE_CONTENIDO_ERROR = "Error: no existe el contenido";
	public static final String  SERVICESTATUSDESCRIPTION_CONTENIDO_ERROR = "Error: no existe el contenido";

	/** Carrito Error: 613 **/
	public static final Integer SERVICESTATUSCODE_CARRITO_ERROR = 613;
	public static final String  SERVICESTATUSMESSAGE_CARRITO_ERROR = "Error: el carrito de la compra está vacio";
	public static final String  SERVICESTATUSDESCRIPTION_CARRITO_ERROR = "Error: el carrito de la compra está vacio";

	/** CodigoAutorizacion Error: 614 **/
	public static final Integer SERVICESTATUSCODE_CODIGOAUTORIZACION_ERROR = 614;
	public static final String  SERVICESTATUSMESSAGE_CODIGOAUTORIZACION_ERROR = "Error: el carrito de la compra está vacio";
	public static final String  SERVICESTATUSDESCRIPTION_CODIGOAUTORIZACION_ERROR = "Error: el carrito de la compra está vacio";

	/** Venta Error: 615 **/
	public static final Integer SERVICESTATUSCODE_VENTA_ERROR = 615;
	public static final String  SERVICESTATUSMESSAGE_VENTA_ERROR = "Error: no existe la venta";
	public static final String  SERVICESTATUSDESCRIPTION_VENTA_ERROR = "Error: no existe la venta";

	/** Modificacion Error: 616 **/
	public static final Integer SERVICESTATUSCODE_MODIFICACION_ERROR = 616;
	public static final String  SERVICESTATUSMESSAGE_MODIFICACION_ERROR = "Error: no existe la modificación";
	public static final String  SERVICESTATUSDESCRIPTION_MODIFICACION_ERROR = "Error: no existe la modificación";

	/** Usuario Error: 617 **/
	public static final Integer SERVICESTATUSCODE_USUARIO_ERROR = 617;
	public static final String  SERVICESTATUSMESSAGE_USUARIO_ERROR = "Error: no existe el usuario";
	public static final String  SERVICESTATUSDESCRIPTION_USUARIO_ERROR = "Error: no existe el usuario";

	/** Venta Facturada Error: 618 **/
	public static final Integer SERVICESTATUSCODE_VENTAFACTURADA_ERROR = 618;
	public static final String  SERVICESTATUSMESSAGE_VENTAFACTURADA_ERROR = "Error: la venta está facturada";
	public static final String  SERVICESTATUSDESCRIPTION_VENTAFACTURADA_ERROR = "Error: la venta está facturada";

	/** Venta Anulada Error: 619 **/
	public static final Integer SERVICESTATUSCODE_VENTAANULADA_ERROR = 619;
	public static final String  SERVICESTATUSMESSAGE_VENTAANULADA_ERROR = "Error: la venta está anulada";
	public static final String  SERVICESTATUSDESCRIPTION_VENTAANULADA_ERROR = "Error: la venta está anulada";

	/** Venta con Canjes de Bono Error: 620 **/
	public static final Integer SERVICESTATUSCODE_VENTACONCANJESBONO_ERROR = 620;
	public static final String  SERVICESTATUSMESSAGE_VENTACONCANJESBONO_ERROR = "Error: la venta tiene canjes de bonos";
	public static final String  SERVICESTATUSDESCRIPTION_VENTACONCANJESBONO_ERROR = "Error: la venta tiene canjes de bonos";

	/** No existe Zonasesion Error: 621 **/
	public static final Integer SERVICESTATUSCODE_NOEXISTEZONASESION_ERROR = 621;
	public static final String  SERVICESTATUSMESSAGE_NOEXISTEZONASESION_ERROR = "Error: no existe la ZONASESION";
	public static final String  SERVICESTATUSDESCRIPTION_NOEXISTEZONASESION_ERROR = "Error: no existe la ZONASESION";

	/** No existe Recinto Error: 622 **/
	public static final Integer SERVICESTATUSCODE_NOEXISTERECINTO_ERROR = 622;
	public static final String  SERVICESTATUSMESSAGE_NOEXISTERECINTO_ERROR = "Error: no existe el RECINTO";
	public static final String  SERVICESTATUSDESCRIPTION_NOEXISTERECINTO_ERROR = "Error: no existe el RECINTO";

	/** No existe Entrada Error: 623 **/
	public static final Integer SERVICESTATUSCODE_NOEXISTEENTRADA_ERROR = 623;
	public static final String  SERVICESTATUSMESSAGE_NOEXISTEENTRADA_ERROR = "Error: no existe la ENTRADA";
	public static final String  SERVICESTATUSDESCRIPTION_NOEXISTEENTRADA_ERROR = "Error: no existe la ENTRADA";

	/** No existe Sesion Error: 624 **/
	public static final Integer SERVICESTATUSCODE_NOEXISTESESION_ERROR = 624;
	public static final String  SERVICESTATUSMESSAGE_NOEXISTESESION_ERROR = "Error: no existe la SESION";
	public static final String  SERVICESTATUSDESCRIPTION_NOEXISTESESION_ERROR = "Error: no existe la SESION";

	/** El usuario no tiene privilegios para esta funcionalidad Error: 625 **/
	public static final Integer SERVICESTATUSCODE_USUARIOSINPERMISOS_ERROR = 625;
	public static final String  SERVICESTATUSMESSAGE_USUARIOSINPERMISOS_ERROR = "Error: El usuario no tiene privilegios para esta funcionalidad";
	public static final String  SERVICESTATUSDESCRIPTION_USUARIOSINPERMISOS_ERROR = "Error: El usuario no tiene privilegios para esta funcionalidad";

	/**************************************************************************************************************************************
	 * CONSTANTES PARA LOS MENSAJES DE ESTADO EN LAS LLAMADAS A LOS SERVICIOS
	 * DESDE INTERNET (INTEGRACIÓN COLOSSUS EXPERTICKET)
	 **************************************************************************************************************************************/
	
	/**************************************************************************
	 * CONSTANTES PARA LOS CÓDIGOS DE ESTADOACCESO PARA EL CONTROL DE ACCESOS *
	 * ************************************************************************/ 
	public static final Integer CONTROLACCESOS_ACCESO_CORRECTO         = 0;   // Correcto
	public static final Integer CONTROLACCESOS_TARJETA_NO_ENCONTRADA   = 201; // Tarjeta no encontrada
	public static final Integer CONTROLACCESOS_EXCESO_DE_UTILIZACION   = 202; // Exceso de utilización
	public static final Integer CONTROLACCESOS_ACCESO_ANTES_DE_FECHA   = 207; // Acceso antes de fecha
	public static final Integer CONTROLACCESOS_ACCESO_DESPUES_DE_FECHA = 208; // Acceso después de fecha
	public static final Integer CONTROLACCESOS_RECINTO_NO_AUTORIZADO   = 209; // Recinto no autorizado
	public static final Integer CONTROLACCESOS_ACCESO_ANTES_DE_HORA    = 210; // Acceso antes de hora
	public static final Integer CONTROLACCESOS_ACCESO_DESPUES_DE_HORA  = 211; // Acceso después de hora
	public static final Integer CONTROLACCESOS_SESION_NO_AUTORIZADA    = 213; // Sesión no autorizada
	public static final Integer CONTROLACCESOS_ENTRADA_ANULADA         = 214; // Entrada anulada
	public static final Integer CONTROLACCESOS_BONO_ANULADO            = 215; // Bono anulado
	public static final Integer CONTROLACCESOS_NO_CONTROLADO           = -1;  // INCIDENCIA NO CONTROLADA
	/**************************************************************************
	 * CONSTANTES PARA LOS CÓDIGOS DE ESTADOACCESO PARA EL CONTROL DE ACCESOS *
	 * ************************************************************************/ 
}
